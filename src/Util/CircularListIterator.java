package Util;

import java.util.Iterator;

class CircularListIterator<E> implements Iterator<E>{

	int currentIndex;
	int size;
	CircularList<E> myList;
	
	CircularListIterator(CircularList<E> cl){
		currentIndex=0;
		size=cl.size();
		myList = cl;
	}
	
	@Override
	public boolean hasNext() {
		return currentIndex==size;
	}

	@Override
	//having to handle this exception is so awkward! Bad design!!!
	public E next() {
		E o = myList.get(currentIndex);
		currentIndex++;
		return o;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
	
}
