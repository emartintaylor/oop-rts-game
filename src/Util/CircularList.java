package Util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * This class is a circular linked list. It provides the ability to parse the list backwards and forwards and stores the current 
 * selected element for later use. 
 * 
 * The way we are using it for almost everything is bad because something besides controller 
 * could call a method that controller is only supposed to use, and thus mess up the state of 
 * the currently selected unit. 
 * 
 * We should have had one interface to this class for controller (a Circularly linked collection), 
 * and one for model (more like a Collection)
 *
 */
public class CircularList<E>{
	private Node<E> current;
	private Node<E> head;
	private Node<E> tail;
	private int size;
	private String description;
	/**
	 * Creates an empty circular list.
	 */
	public CircularList(){
		size = 0;
	}
	/**
	 * Inserts an element into the list.
	 *
	 * @param element			The element to insert into the list
	 */
	public void add(E element){
		description = element.toString();
		if(head == null){
			head = new Node<E>(element);
			current = head;
			tail = head;
			head.setNext(tail);
			tail.setPrevious(head);
		}
		else{
			Node<E> n = new Node<E>(element, head, tail);
			tail.setNext(n);
			head.setPrevious(n);
			tail = n;
		}
		size++;
	}
	
	/**
	 * Returns the element from the desired index.
	 *
	 * @return			The element from the desired index
	 * @throws SOTSException	If the index is out of range
	 */
	public E get(int index){
		if(index < 0 || index >= size){throw new IndexOutOfBoundsException("Index out of bounds: " + index); }
		Node<E> n = head;
		for(int x = 0; x < index; x++){ n = n.getNext(); }
		return n.getElement();
	}
	/**
	 * Returns the currently selected element in the list
	 *
	 * @return			The current element
	 * @throws OilException	If the list is empty
	 */
	public E getCurrent() {
		if(size == 0){ throw new IndexOutOfBoundsException("Cannot parse the list: it is empty.");}		
		return current.getElement();
	}
	/**
	 * Selects the next element in the the list and returns it. This element can be accessed with
	 * the <code>getCurrent()</code> method until <code>getNext()</code> or <code>getPrevious()</code> is called.
	 *
	 * @return			The next element in the list
	 * @throws OilException	If the list is empty
	 */
	public E getNext() {
		if(size == 0){ throw new IndexOutOfBoundsException("Cannot parse the list: it is empty.");}		
		current = current.getNext();
		return current.getElement();
	}
	/**
	 * Selects the previous element in the the list and returns it. This element can be accessed with
	 * the <code>getCurrent()</code> method until <code>getNext()</code> or <code>getPrevious()</code> is called.
	 *
	 * @return			The previous element in the list
	 * @throws OilException	If the list is empty
	 */
	public E getPrevious() {
		if(size == 0){ throw new IndexOutOfBoundsException("Cannot parse the list: it is empty.");}		
		current = current.getPrevious();
		return current.getElement();
	}
	/**
	 * Returns true if the list is empty.
	 *
	 * @return				True if the list is empty
	 */
	public boolean isEmpty(){
		return size == 0;
	}
	/**
	 * Removes the first occurrence of the provided element from the list. If the element removed is the
	 * currently selected element, the currently selected element is set to the element following the one
	 * removed. 
	 *
	 * @return 			The element that is removed
	 * @throws OilException	If the list is empty or if the element is not found
	 */
	public E remove(E element) {
		if(size == 0){throw new IndexOutOfBoundsException("Cannot parse the list: it is empty");}
		//E out;		
		Node<E> n = head;
		for(int x = 0; x <= size; x++){
			if(n.getElement().equals(element)){
				n.getPrevious().setNext(n.getNext());
				n.getNext().setPrevious(n.getPrevious());
				if(n.equals(current)){current = n.getNext();}
				size--;
				return (E) n.getElement();
			}
			else{n = n.getNext();}
		}
		throw new NoSuchElementException("The requested element is not in the list");
	}	
	/**
	 * Removes an element from the list. The element will be removed from the specified location. If this
	 * element is the currently selected element, the selected element is set to the next element in the list.
	 *
	 * @return 			The element that is removed
	 * @throws OilException	If the index is out of range
	 */
	public E remove(int index){
		E out;
		if(index < 0 || index >= size){ throw new IndexOutOfBoundsException("Index out of bounds: " + index); }
		else if(size == 0){
			out = (E) head.getElement();
			head = null;
			current = null;
			tail = null;			
		}
		else{
			Node<E> n = head;
			for(int x = 0; x < index; x++){
				n = n.getNext();
			}
			n.getPrevious().setNext(n.getNext());
			n.getNext().setPrevious(n.getPrevious());
			if(current.equals(n)){current = n.getNext();}
			out = (E) n.getElement();
		}
		size--;
		return out;		
	}
	/**
	 * Sets the currently select element to the index given.
	 *
	 * @param index				The index to set current element to.
	 * @return 				The element selected
	 * @throws OilException			If the index is out of bounds
	 */
	public E setCurrent(int index){
		if(index < 0 || index >= size){ throw new IndexOutOfBoundsException("Index Out of Bounds: " + index);}
		Node<E> n = head;
		for(int x = 0; x < index; x++){n = n.getNext();} 
		current = n;
		return (E) current.getElement();
	}
	/**
	 * Returns the number of elements in the list.
 	 *
	 * @return				The number of elements in the list
	 */
	public int size(){
		return size;
	}
	/**
	 * Main method for test purposes only.
	 *
	 * @param args			Argument list - not needed
	 */
	/*
	public static void main(String args[]){
		CircularList<String> cl = new CircularList<String>();
		System.out.println(cl.isEmpty() ? "isEmpty() pass" : "isEmpty() fail");
		try{
			cl.getCurrent();
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.getNext();
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.getPrevious();
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.remove(3);
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.get(3);
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.remove("Cat");
		}
		catch(OilException SOTSE){System.out.println(SOTSE);}
		cl.add("One");
		cl.add("Two");
		cl.add("Three");
		cl.add("Four");
		cl.add("");
		try{
			cl.remove("Cat");
		}
		catch(OilException SOTSE){System.out.println(SOTSE);}
		try{
			System.out.println(cl.get(2).equals("Three") ? "get(int) passes" : "get(int) fails");
		}
		catch(OilException SOTSE){System.out.println("get(int) fails");}
		try{
			System.out.println(cl.get(0).equals("One") ? "get(int) passes" : "get(int) fails");
		}
		catch(OilException SOTSE){System.out.println("get(int) fails");}
		try{
			System.out.println(cl.get(4).equals("") ? "get(int) passes" : "get(int) fails");
		}
		catch(OilException SOTSE){System.out.println("get(int) fails");}
		try{
			cl.remove(5);
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.remove(-1);
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.get(5);
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		try{
			cl.get(-1);
			System.out.println("Size constraint fails");
		}
		catch(OilException SOTSE){System.out.println("Size constraint pass");}
		System.out.println(cl.size() == 5 ? "size() pass" : "size() fail");
		System.out.println(cl.isEmpty() ? "isEmpty() fail" : "isEmpty() pass");
		try{
			cl.setCurrent(-1);
			System.out.println("setCurrent(int) fails constraint");
		}
		catch(OilException SOTSE){System.out.println("setCurrent(int) passes constraint");}
		try{
			cl.setCurrent(5);
			System.out.println("setCurrent(int) fails constraint");
		}
		catch(OilException SOTSE){System.out.println("setCurrent(int) passes constraint");}
		try{
			System.out.println(cl.setCurrent(4).equals("") ? "set(int) passes" : "set(int) fails");
		}
		catch(OilException SOTSE){System.out.println("setCurrent(int) fails");}
		try{
			System.out.println("Print Forward:");
			System.out.println(cl.getCurrent());
			for(int x = 0; x < (2* cl.size()); x++){
				System.out.println(cl.getNext());
			}
			System.out.println("Now Backward:");
			for(int x = 0; x < 2 * cl.size(); x++){
				System.out.println(cl.getPrevious());
			}
			cl.getPrevious();
		}
		catch(OilException SOTSE){ System.out.println("Print fail");}
		try{
			System.out.println(cl.remove(3).equals("Four") ? "remove(int) passes" : "remove(int) fails");
			System.out.println(cl.getCurrent().equals("") ? "remove(int) adjustments passes" : "remove(int) adjustments fails");
		}
		catch(OilException SOTSE){ System.out.println("Remove(int) fails");}
		System.out.println(cl.size() == 4 ? "size() pass" : "size() fail");
		try{
			System.out.println("Print Forward:");
			for(int x = 0; x < (2* cl.size()); x++){
				System.out.println(cl.getNext());
			}
			System.out.println("Now Backward:");
			for(int x = 0; x < 2 * cl.size(); x++){
				System.out.println(cl.getPrevious());
			}
			cl.getPrevious();
		}
		catch(OilException SOTSE){ System.out.println("Print fail");}
		try{
			System.out.println(cl.remove("Three").equals("Three") ? "remove(E) passes" : "remove(E) fails");
			System.out.println(cl.getCurrent().equals("") ? "remove(E) adjustments passes" : "remove(E) adjustments fails");
		}
		catch(OilException SOTSE){ System.out.println("Remove(E) fails"); }
		System.out.println(cl.size() == 3 ? "size() pass" : "size() fail");
		try{
			System.out.println("Print Forward:");
			for(int x = 0; x < (2* cl.size()); x++){
				System.out.println(cl.getNext());
			}
			System.out.println("Now Backward:");
			for(int x = 0; x < 2 * cl.size(); x++){
				System.out.println(cl.getPrevious());
			}
			cl.getPrevious();
		}
		catch(OilException SOTSE){ System.out.println("Print fail");}
	}
	*/
	
	//really, this class should implement collection, but 
	//for sake of time I just added these two methods. 
	public Collection<E> getAll(){
		LinkedList<E> l = new LinkedList<E>();
		for(int index=0;index<size();index++)
				l.add(this.get(index));
		return l;
	}
	/**
	 * precondition: none of the elements in c are in the CircularList
	 * postcondition: all elements in c are in the CircularList
	 * @param c
	 */
	public void addAll(Collection<E> c){
		for(E elem : c)
			this.add(elem);
	}
		
}
