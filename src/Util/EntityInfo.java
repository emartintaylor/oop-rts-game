package Util;

import java.util.ArrayList;
import java.util.List;


public class EntityInfo {
	//jbhj
	private String name;
	private String type;
	private List<String> missions;
	private List<Pair<String>> info;
	private List<EntityInfo> children;
	
	public EntityInfo(String name) {
		this.name = name;
		missions = new ArrayList<String>();
		info = new ArrayList<Pair<String>>();
		children = new ArrayList<EntityInfo>();
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void addMission(String s) {
		missions.add(s);
	}
	
	public void addInfo(Pair<String> p) {
		info.add(p);
	}
	
	public void addChildren(EntityInfo c) {
		children.add(c);
	}
	
	public String getName() {
		return name;
	}
	
	public List getMissions() {
		return missions;
	}
	
	public List<Pair<String>> getInfo() {
		return info;
	}
	
	public List<EntityInfo> getChildren() {
		return children;
	}
}