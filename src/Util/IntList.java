package Util;

public class IntList implements IntRecycler{

	boolean[] list= null;
	
	public IntList(int capacity){
		list = new boolean[capacity];
	}
	
	public int newInt() {
		for(int i = 0; i< list.length; i++)
			if(list[i] == false){
				list[i] = true;
				return i;
			}
		return -1;
	}

	public void recycleInt(int garbage) {
		list[garbage] = false;
	}
	
	

}
