package Util;
//sdsd
/**
 * General exception class to help distinguish between Java exceptions
 * and our defined exceptions that are caused by constraints in the
 * game.  This class can be instantiated as is, but it's probably better
 * to subclass it for more common specific exceptions.
 *
 */
public class OilException extends Exception {

    private String info = "Default Oil Exception";

    public OilException (String s) {
       super();
       this.info = s;

    }

    public OilException() {
     super();
    }

    /**
   * Return informational message about the game exception
   */
    public String getInfo() {
    return this.info;
    }
    /**
   *
   */
    public String toString(){
    return this.info;
    }
}










