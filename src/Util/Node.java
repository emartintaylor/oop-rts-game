package Util;

public class Node<F>{
	private F element;
	private Node<F> next;
	private Node<F> previous;
	public Node(F element){
		setElement(element);
	}
	public Node(F element, Node<F> previous){
		this(element);
		setPrevious(previous);
	}
	public Node(F element, Node<F> next, Node<F> previous){
		this(element, previous);
		setNext(next);
	}
	public F getElement(){
		return element;
	}
	public Node<F> getNext(){
		return next;
	}
	public Node<F> getPrevious(){
		return previous;
	}
	public void setElement(F element){
		this.element = element;
	}
	public void setNext(Node<F> node){
		next = node;
	}
	public void setPrevious(Node<F> node){
		previous = node;
	}
}