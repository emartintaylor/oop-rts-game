package controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

/**
 * The EventDispatcher is concerned with receiving notifications about GameEvents and signaling them to
 * interested parties.  To this effect it provides to modes, one time listeners and default listeners.  One
 * time listeners are intended to be used a by an EventNotifier who wishes to momentarily override any default
 * listeners.
 * @author JR
 * @version 1.0
 *
 */
public class EventDispatcher implements EventNotifier, GameEventDispatcher
{
	/* This list should be checked first, if an entry for the GameEventType is found, it should be notified
	 * and popped from the stack.
	 */
	HashMap< GameEventType, Stack<EventNotifier>> oneTimeListeners;
	
	/* If no one-time listeners were found, then the list of default listeners for the GameEventType should
	 * be retrieved and every member notified.
	 */
	HashMap< GameEventType, HashSet<EventNotifier>> defaultListeners;
	
	private static final boolean REPORTS_ON = true; //set to false to turn reporting off.
	
	/**
	 * Instantiates a new EventDispatcher with empty default and one time listener lists.
	 */
	EventDispatcher()
	{
		if(REPORTS_ON)
			System.out.println("New EventDispatcher created: " + this);
		oneTimeListeners = new HashMap< GameEventType, Stack<EventNotifier>>();
		defaultListeners = new HashMap< GameEventType, HashSet<EventNotifier>>();
	}
	
	/**
	 * Called (by the InputActionDispatcher) to signal the occurrence of some GameEventType.  The EventDispatcher
	 * then checks its list of notifiers and determines which ones should be informed of the GameEventType.
	 * @param g -- The GameEventType that has occurred.
	 */
	@Override
	public void notify(GameEventType g)
	{
		if(REPORTS_ON)
		{
			System.out.println("EventDispatcher\n\tGameEventType signaled: " + g);
			System.out.println("Notifying one time listeners...");
		}
		if( !notifyOneTimeListeners(g)) //returns false if no OneTimeListeners were found
		{
			if(REPORTS_ON)
				System.out.println("No one time listeners found\nNotifying default listeners...");
			if(!notifyDefaultListeners(g) && REPORTS_ON)
				System.out.println("No default listeners found");
		}
	}
	
	/**
	 * Registers the EventNotifier as a default listener for the provided GameEventType.  When that event occurs,
	 * if there exist no one-Time listeners, then all default listeners will be signaled.
	 * @param event -- the GameEventType of interest.
	 * @param notifier -- the EventNotifier which should be informed of the event.
	 */
	@Override
	public void registerDefaultListener(GameEventType event, EventNotifier notifier)
	{
		if(REPORTS_ON)
			System.out.println("Registering " + notifier + "as a default listener for " + event);
		retrieveDefaultListeners(event).add(notifier);
	}
	
	/**
	 * Registers the EventNotifier as a one-time listener for the provided GameEventType and adds it to the top
	 * of the stack of one time listeners.  When that event occurs, the first member of the list of one-time listeners
	 * for that event will be notified.  
	 * @param event -- the GameEventType of interest.
	 * @param notifier -- the EventNotifier which should be informed of the event.
	 */
	@Override
	public void registerOneTimeListener(GameEventType event, EventNotifier notifier)
	{
		if(REPORTS_ON)
			System.out.println("Registering " + notifier + "as a one time listener for " + event);
		retrieveOneTimeListeners(event).add(notifier);

	}
	
	/**
	 * Removes the notifier from its registration for all GameEvents for the one time listener list.
	 * @param notifier -- the EventNotifier that should be unsubscribed.
	 */
	@Override
	public void removeOneTimeListener(EventNotifier notifier)
	{
		if(REPORTS_ON)
			System.out.println("Removing " + notifier + "from all oneTime listener events");
		for( Stack<EventNotifier> s : oneTimeListeners.values() )
		{
			if( s.contains(notifier) )
				removeFromStack( notifier, s );
		}
	}
	
	/**
	 * Removes the notifier from its registration for the included GameEventType from the one-time listener list.
	 * @param event -- the GameEventType the notifier should unsubscribe from
	 * @param notifier -- the EventNotifer being unsubscribed.
	 */
	@Override
	public void removeOneTimeListener(GameEventType event, EventNotifier notifier)
	{
		if(REPORTS_ON)
			System.out.println("Removing the EventNotifier " + notifier + "from the oneTime listener event " + event);
		removeFromStack(notifier, retrieveOneTimeListeners(event));
	}
	
	/**
	 * Removes the notifier from its registration for all GameEvents for teh default listener list.
	 * @param notifier -- the EventNotifier that should be unsubscribed.
	 */
	@Override
	public void removeDefaultListener(EventNotifier notifier)
	{
		if(REPORTS_ON)
			System.out.println("Removing the EventNotifier " + notifier + "from all default listener events");
		for( HashSet<EventNotifier> s : defaultListeners.values() )
		{
			s.remove(notifier);
		}
	}
	
	/**
	 * Removes the notifier from its registration for the included GameEventType from
	 * the default listener list.
	 * @param event -- the GameEventType the notifier should unsubscribe from
	 * @param notifier -- the EventNotifer being unsubscribed.
	 */
	@Override
	public void removeDefaultListener(GameEventType event, EventNotifier notifier)
	{
		if(REPORTS_ON)
			System.out.println("Removing the EventNotifier " + notifier + "from the default listener event " + event);
		retrieveDefaultListeners(event).remove(notifier);

	}
	
	/**
	 * An internal utility method, it notifies the first member of the one time listeners list, and pops it.
	 * @param g -- The GameEventType to be signaled.
	 * @return -- returns true if a one time listener was found and successfully notified.
	 */
	private boolean notifyOneTimeListeners(GameEventType g)
	{
		Stack<EventNotifier> otList = retrieveOneTimeListeners(g);
		if( !otList.isEmpty() )
		{
			EventNotifier en = otList.pop();
			en.notify(g);
			return true;
		}
		return false;
	}
	
	/**
	 * An internal utility method which returns the stack of one time listeners associated with the given
	 * GameEventType.  If no stack is associated with that GameEventType, then an empty stack associated
	 * with that GameEventType and the empty stack is returned.
	 * @param g -- The GameEventType whose one time listeners will be returned.
	 * @return The list of one time listeners associated with the GameEventType(empty if no set is found).
	 */
	private Stack<EventNotifier> retrieveOneTimeListeners(GameEventType g)
	{
		Stack<EventNotifier> theStack = oneTimeListeners.get(g);
		if( theStack == null )
		{
			theStack = new Stack<EventNotifier>();
			oneTimeListeners.put(g, theStack);
		}
		return theStack;
	}
	
	/**
	 * An internal utility method which retrieves the set of DefaultListeners for this event and notifies
	 * each one.
	 * @param g -- the GameEventType whose default listeners should be notified.
	 * @return -- true if the set of default listeners was not empty.
	 */
	private boolean notifyDefaultListeners(GameEventType g)
	{
		HashSet<EventNotifier> dlSet = retrieveDefaultListeners(g);
		if( dlSet.isEmpty() )
			return false;
		for( EventNotifier en : dlSet )
			en.notify(g);
		return true;
		
	}
	
	/**
	 * An internal utility method which returns the set of default listeners associated with the given
	 * GameEventType.  If no set is associated with that GameEventType, then an empty set associated
	 * with that GameEventType and the empty set is returned.
	 * @param g -- The GameEventType whose default listeners will be returned.
	 * @return The set of default listeners associated with the GameEventType(empty if no set is found).
	 */
	private HashSet<EventNotifier> retrieveDefaultListeners(GameEventType g)
	{
		HashSet<EventNotifier> theSet = defaultListeners.get(g);
		if( theSet == null )
		{
			theSet = new HashSet<EventNotifier>();
			defaultListeners.put(g, theSet);
		}
		return theSet;
	}
	
	/**
	 * An internal utility method for removing a specific notifier from the stack of EventNotifiers.
	 * Removes all entries from the stack that .equals() the provided EventNotifier.
	 * @param en -- the EventNotifier to be removed.
	 * @param s -- the Stack<EventNotifier> it should be removed from.
	 */
	private void removeFromStack( EventNotifier en, Stack<EventNotifier> s )
	{
		Stack<EventNotifier> tempStack = new Stack<EventNotifier>();
		while( !s.isEmpty())
		{
			EventNotifier tempEN = s.pop();
			if(!tempEN.equals(en))
				tempStack.add(tempEN);
		}
		while(!tempStack.isEmpty())
			s.add(tempStack.pop());
	}
	
	//a driver for testing
	public static void main(String[] args)
	{
		EventDispatcher e = new EventDispatcher();
		PrintNotifier p1 = new PrintNotifier(1);
		PrintNotifier p2 = new PrintNotifier(2);
		PrintNotifier p3 = new PrintNotifier(3);
		PrintNotifier p4 = new PrintNotifier(4);
		
		e.registerDefaultListener(GameEventType.HOTKEY_0, p1);
		e.registerDefaultListener(GameEventType.HOTKEY_0, p2);
		e.registerDefaultListener(GameEventType.HOTKEY_1, p1);
		e.registerOneTimeListener(GameEventType.HOTKEY_0, p3);
		e.registerOneTimeListener(GameEventType.HOTKEY_0, p3);
		e.removeOneTimeListener(GameEventType.HOTKEY_0, p3);
		e.notify(GameEventType.HOTKEY_0);
		e.registerOneTimeListener(GameEventType.HOTKEY_0, p3);
		e.registerOneTimeListener(GameEventType.HOTKEY_0, p3);
		e.removeDefaultListener(p3);
		e.removeDefaultListener(GameEventType.HOTKEY_0, p3);
		e.notify(GameEventType.HOTKEY_0);
		e.notify(GameEventType.HOTKEY_0);
		e.notify(GameEventType.HOTKEY_0);
		e.notify(GameEventType.HOTKEY_0);
		e.registerOneTimeListener(GameEventType.HOTKEY_1, p4);
		e.notify(GameEventType.HOTKEY_1);
		e.notify(GameEventType.HOTKEY_1);
		e.notify(GameEventType.HOTKEY_1);
		e.notify(GameEventType.EXIT_GAME);
		
	}
}

class PrintNotifier implements EventNotifier//inner class for testing
{
	private int identifier;
	
	PrintNotifier(int id)
	{
		this.identifier = id;
	}
	
	@Override
	public void notify(GameEventType g)
	{
		System.out.println("EventNotifier " + identifier + " has been signaled with the event " + g);
	}
}
