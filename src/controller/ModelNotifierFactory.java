package controller;

import controller.GameEventDispatcher;
import controller.ModelNotifier;
import Model.ControllableModel;;

/**
 * A class meant to hold definitions for all the various ModelNotifiers necessary to signal the
 * ControllableModel it contains only one static method which instantiates instances of the anonymous
 * inner classes and registers them with the GameEventDispatcher.
 * @author JR
 * @version 1.0
 *
 */
public class ModelNotifierFactory
{
	static void initializeModelNotifiers( final ControllableModel cm, GameEventDispatcher ged)
	{
		
		/* This first section defines several ViewNotifier as anonymous inner classes.
		 * Each is intended to call one function in the view when notified by a GameEventDispatcher.
		 */
		ModelNotifier mainOVNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleToGameOverviewDecorator();
			}
			
		};
		
		ModelNotifier unitOVNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleToUnitOverviewDecorator();
			}
			
		};
		
		ModelNotifier structOVNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleToStructureOverviewDecorator();
			}
			
		};
		
		ModelNotifier techOVNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleToTechTreeOverviewDecorator();
			}
			
		};

		ModelNotifier modeFwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleModeForward();
			}
			
		};
		
		ModelNotifier modeBwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleModeBackward();
			}
			
		};
		
		ModelNotifier typeFwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleTypeForward();
			}
			
		};
		
		ModelNotifier typeBwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleTypeBackward();
			}
			
		};
		
		ModelNotifier instFwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleInstanceForward();
			}
			
		};
		
		ModelNotifier instBwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleInstanceBackward();
			}
			
		};
		
		ModelNotifier commandFwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleCommandForward();
			}
			
		};
		
		ModelNotifier commandBwdNotifier = new ModelNotifier(cm)
		{
			@Override
			public void notify(GameEventType g)
			{
				cm.cycleCommandBackward();
			}
			
		};
		
		//now each needs to be registered as a default listener with the event dispatcher
		ged.registerDefaultListener(GameEventType.SHOW_MAIN, mainOVNotifier);
		ged.registerDefaultListener(GameEventType.SHOW_OV_UNIT, unitOVNotifier);
		ged.registerDefaultListener(GameEventType.SHOW_OV_STRUCT, structOVNotifier);
		ged.registerDefaultListener(GameEventType.SHOW_OV_TECH, techOVNotifier);
		ged.registerDefaultListener(GameEventType.MODE_NEXT, modeFwdNotifier);
		ged.registerDefaultListener(GameEventType.MODE_PREV, modeBwdNotifier);
		ged.registerDefaultListener(GameEventType.SUBMODE_NEXT, typeFwdNotifier);
		ged.registerDefaultListener(GameEventType.SUBMODE_PREV, typeBwdNotifier);
		ged.registerDefaultListener(GameEventType.INSTANCE_NEXT, instFwdNotifier);
		ged.registerDefaultListener(GameEventType.INSTANCE_PREV, instBwdNotifier);
		ged.registerDefaultListener(GameEventType.COMMAND_NEXT, commandFwdNotifier);
		ged.registerDefaultListener(GameEventType.COMMAND_PREV, commandBwdNotifier);
	}
}
