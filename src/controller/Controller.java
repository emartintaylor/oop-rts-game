package controller;

import java.io.IOException;

import Model.ControllableModel;
import Model.Displayable.interfaces.TickListener;
import View.ViewUpdater;

/**
 * Controller is the top level member of the Controller package, it can be used
 * by the other packages to retrieve the EventDispatcher, and is used to initialize the Controller.
 * @author JR
 *
 */
public class Controller implements TickListener
{
	EventDispatcher dispatcher;
	
	public Controller()
	{
		this.dispatcher = new EventDispatcher();
	}
	
	/**
	 * After instantiation the ControllerManager still needs to link with
	 * top level elements of the Model and View, these should each be passed
	 * into the initialize method.
	 */
	public void initialize(ViewUpdater v, ControllableModel m)
	{
		InputActionDispatcher i = new InputActionDispatcher(dispatcher);
		
		
		try
		{
			ControlSchemeParser.parseScheme("./src/controls.txt", i);
		} catch (IOException e1)
		{
			System.out.println("Control file is broken.");
			e1.printStackTrace();
		}
		
		ViewNotifierFactory.initializeViewNotifiers(v, dispatcher);
		ModelNotifierFactory.initializeModelNotifiers(m, dispatcher);
		GameKeyListener gameKey = new GameKeyListener(i);
		v.attachKeyListener(gameKey);
	}
	
	/**
	 * Used to retrieve the EventDispatcher from the Controller.
	 * @return The Controller's GameEventDispatcher.
	 */
	public GameEventDispatcher getEventManager()
	{
		return dispatcher;
	}

	@Override
	public void onTick()
	{
		dispatcher.notify(GameEventType.TICK);
	}
}