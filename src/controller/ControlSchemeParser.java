package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.KeyStroke;

/**
 * ControlSchemeParser supports several static methods used to parse the control scheme from a text file.
 * It does no error resolution, if the text file is not configured properly the parser should throw an
 * exception, but if the exception is caught and the program doesn't crash the game controls will not work
 * properly.
 * 
 * The text file for the controls should hold one control association per line with each line taking the
 * form [GameEventType]=[modifiers] [KeyName].
 * @author JR
 *
 */
public class ControlSchemeParser
{
	private final static boolean REPORTS_ON = true; // set to false to turn reports off
	
	/**
	 * ParseScheme will attempt to load the file identified by fName, and parse its controls
	 * into the InputActionDispatcher.
	 * 
	 * @param fName -- an absolute or relative location of the control scheme text file.
	 * @param i -- the InputActionDispatcher
	 * @throws IOException -- thrown if the file is not formatted properly, or if the File cannot be found.
	 */
	public static void parseScheme( String fName, InputActionDispatcher i ) throws IOException
	{
		BufferedReader file = new BufferedReader(new FileReader(fName));
		if(REPORTS_ON)
			System.out.println("Now parsing file: fName");
		
		while( file.ready())
		{
			String nextLine = file.readLine();
			if(REPORTS_ON)
				System.out.println("Parsing controls: " + nextLine);
			String[] vals = nextLine.split("=");
			if(vals.length != 2)
				throw new IOException( "File line: " + nextLine + " was not parsed into 2 Strings");
			setControls( vals, i );
		}
	}
	
	/**
	 * A private utility method which creates GameEventTypes and KeyStrokes from the input, and associates them
	 * with the InputActionDispatcher.  The control array must contain 2 strings with the first representing the
	 * String equivalent of one of the GameEventType enumerations, the second must properly convert to a KeyStrok
	 * via the KeyStroke.getKeyStroke(String keyName) method.
	 * @param control -- A 2 member array of String representing a GameEventType and a KeyStroke.
	 * @param i -- The InputActionDispatcher they will be associated with.
	 * @throws IOException -- If the array is not of size 2 or if either of the Strings fail to convert to a
	 * GameEventType and KeyStroke.
	 */
	private static void setControls(String[] control, InputActionDispatcher i) throws IOException
	{
		GameEventType g;
		KeyStroke k;
		try
		{
			g = GameEventType.valueOf( control[0]);
		}
		catch (Exception e)
		{
			throw new IOException("The GameEventType \"" + control[0] + "\" is invalid");
		}
		
		k = KeyStroke.getKeyStroke(control[1]);
		if( k == null )
			throw new IOException("The KeyStroke string \"" + control[1] + "\" is invalid");
			
		i.registerControl( k, g);	
	}
}
