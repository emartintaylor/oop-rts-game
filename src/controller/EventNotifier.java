package controller;

/**
 * EventNotifiers are concerned with game events, they can be registered with both the InputActionDispatcher and the GameEventDispatcher.
 * The only method they contain notify() is called to notify the notifier of any event it is listening to.
 * @author JR
 *
 */
public interface EventNotifier
{
	/**
	 * When registered with certain GameEventTypes, the notify method will be called when the event occurs.
	 * @param g -- the specific GameEventType that caused this method to be called.
	 */
	void notify( GameEventType g );
}
