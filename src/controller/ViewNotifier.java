package controller;

import View.ViewUpdater;

/**
 * An abstract class for all notifiers which call functions within the ViewNotifier.  This class
 * is to facilitate the use of Anonymous inner classes.
 * @author JR
 * @version 1.0
 */
public abstract class ViewNotifier implements EventNotifier
{
	private ViewUpdater view;
	
	/**
	 * Instantiates a new ViewNotifier, associating it with the provided ViewUpdater.
	 * @param v
	 */
	ViewNotifier( ViewUpdater v )
	{
		this.view = v;
	}
	
	/**
	 * Returns the ViewUpdater this ViewNotifier is associated with.
	 * @return The ViewUpdater for this notifier.
	 */
	private ViewUpdater getView()
	{
		return this.view;
	}
	
	/**
	 * Will be called by the GameEventNotifier when the registered GameEvent happens.
	 * @param g -- The GameEventType which caused this to be notified.
	 */
	public abstract void notify(GameEventType g);
	

}
