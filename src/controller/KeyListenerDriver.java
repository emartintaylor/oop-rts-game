package controller;

import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.KeyStroke;

import decorators.DecoratorCommander;

import Model.Map;
import Model.Player;
import Model.TableBasedRecycler;
import View.ViewUpdater;

public class KeyListenerDriver
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		EventDispatcher e = new EventDispatcher();
		InputActionDispatcher i = new InputActionDispatcher(e);
		
		try{
		ControlSchemeParser.parseScheme("./src/controls.txt", i);
		}
		catch( Exception ex)
		{
			System.out.println("Somethin done broke");
			System.out.println(ex);
		}
		
		/*
		ViewUpdater v = new ViewUpdater(){

			@Override
			public void show(String s)
			{
				System.out.println("ViewUpdater: SHOW SCREEN: " + s);
			}

			@Override
			public void updateAll()
			{
				System.out.println("ViewUpdater: UPDATE ALL");
			}

			@Override
			public void updateCurrent()
			{
				System.out.println("ViewUpdater: UPDATE CURRENT");
			}
			
		};
		
		ViewNotifierFactory.initializeViewNotifiers(v, e);*/
		
		String [] mapArray = {
				"999999",
				"900009",
				"900009",
				"900009",
				"999999"
		};
		Map map = new Map(mapArray,1);
		
		//Player player = new Player(map, 1);
		//DecoratorCommander dc = player.getDecoratorCommander();
		//ModelNotifierFactory.initializeModelNotifiers(dc, e);
		createAndShowGUI( new GameKeyListener( i ));

	}
	
	private static void createAndShowGUI( KeyListener k) {
        //Create and set up the window.
		JFrame frame = new JFrame("KeyListener demo frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
        frame.addKeyListener(k);
    }


}
