package controller;

import Model.ControllableModel;

/**
 * An abstract class for all notifiers which call functions within the ControllableModel.  This class
 * is to facilitate the use of Anonymous inner classes.
 * @author JR
 * @version 1.0
 */
public abstract class ModelNotifier implements EventNotifier
{
	private ControllableModel model;
	
	/**
	 * Instantiates a new ModelNotifier, associating it with the provided ControllableModel.
	 * @param v
	 */
	ModelNotifier( ControllableModel cm )
	{
		this.model = cm;
	}
	
	/**
	 * Returns the ControllableModel this ModelNotifier is associated with.
	 * @return The ControllableModel for this notifier.
	 */
	private ControllableModel getModel()
	{
		return this.model;
	}
	
	/**
	 * Will be called by the GameEventNotifier when the registered GameEvent happens.
	 * @param g -- The GameEventType which caused this to be notified.
	 */
	public abstract void notify(GameEventType g);
	

}
