package controller;

import View.ViewUpdater;

/**
 * A class meant to hold definitions for all the various ViewNotifiers necessary to signal the
 * ViewUpdater it contains only one static method which instantiates instances of the anonymous
 * inner classes and registers them with the GameEventDispatcher.
 * @author JR
 * @version 1.2
 *
 */
public class ViewNotifierFactory
{
	static void initializeViewNotifiers( final ViewUpdater v, GameEventDispatcher ged)
	{
		
		/* This first section defines several ViewNotifier as anonymous inner classes.
		 * Each is intended to call one function in the view when notified by a GameEventDispatcher.
		 */
		ViewNotifier mainScreenNotifier = new ViewNotifier(v)
		{
			@Override
			public void notify(GameEventType g)
			{
				v.showMainScreen();
			}
			
		};
		
		ViewNotifier unitOviewNotifier = new ViewNotifier(v)
		{
			@Override
			public void notify(GameEventType g)
			{
				v.showUnitOverView();
			}
			
		};
		
		ViewNotifier structOviewNotifier = new ViewNotifier(v)
		{
			@Override
			public void notify(GameEventType g)
			{
				v.showStructOverView();
			}
			
		};
		
		ViewNotifier techOviewNotifier = new ViewNotifier(v)
		{
			@Override
			public void notify(GameEventType g)
			{
				v.showTechTreeOverView();
			}
			
		};
		
		ViewNotifier updateNotifier = new ViewNotifier(v)
		{
			@Override
			public void notify(GameEventType g)
			{
				v.updateCurrent();
			}
			
		};
		
		ViewNotifier updateAllNotifier = new ViewNotifier(v)
		{
			@Override
			public void notify(GameEventType g)
			{
				v.updateAll();
			}
			
		};
		
		//now they need to be registered with the GameEventDispatcher
		ged.registerDefaultListener(GameEventType.SHOW_MAIN, mainScreenNotifier);
		ged.registerDefaultListener(GameEventType.SHOW_OV_UNIT, unitOviewNotifier);
		ged.registerDefaultListener(GameEventType.SHOW_OV_STRUCT, structOviewNotifier);
		ged.registerDefaultListener(GameEventType.SHOW_OV_TECH, techOviewNotifier);
		
		ged.registerDefaultListener(GameEventType.GAME_STATE_CHANGED, updateNotifier);
		ged.registerDefaultListener(GameEventType.TICK, updateAllNotifier);
	}
}
