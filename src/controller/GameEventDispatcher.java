package controller;

/**
 * The GameEventDispatcher interface is the visible interface to any elements
 * of the Model package which wish to register interest in game events.
 * @author JR
 * @version 1.2
 *
 */
public interface GameEventDispatcher
{
	/**
	 * Registers the EventNotifier as a default listener for the provided GameEventType.  When that event occurs,
	 * if there exist no one-Time listeners, then all default listeners will be signaled.
	 * @param event -- the GameEventType of interest.
	 * @param notifier -- the EventNotifier which should be informed of the event.
	 */
	public void registerDefaultListener( GameEventType event, EventNotifier notifier);
	
	/**
	 * Registers the EventNotifier as a one-time listener for the provided GameEventType.  When that event occurs,
	 * the first member of the list of one-time listeners for that event will be notified.  
	 * @param event -- the GameEventType of interest.
	 * @param notifier -- the EventNotifier which should be informed of the event.
	 */
	public void registerOneTimeListener( GameEventType event, EventNotifier notifier);
	
	/**
	 * Removes the notifier from its registration for all GameEvents for the one time listener list.
	 * @param notifier -- the EventNotifier that should be unsubscribed.
	 */
	public void removeOneTimeListener( EventNotifier notifier );
	
	/**
	 * Removes the notifier from its registration for the included GameEventType from the one-time listener list.
	 * @param event -- the GameEventType the notifier should unsubscribe from
	 * @param notifier -- the EventNotifer being unsubscribed.
	 */
	public void removeOneTimeListener( GameEventType event, EventNotifier notifier );
	
	/**
	 * Removes the notifier from its registration for all GameEvents for teh default listener list.
	 * @param notifier -- the EventNotifier that should be unsubscribed.
	 */
	public void removeDefaultListener( EventNotifier notifier );
	
	/**
	 * Removes the notifier from its registration for the included GameEventType from
	 * the default listener list.
	 * @param event -- the GameEventType the notifier should unsubscribe from
	 * @param notifier -- the EventNotifer being unsubscribed.
	 */
	public void removeDefaultListener( GameEventType event, EventNotifier notifier );
	
}
