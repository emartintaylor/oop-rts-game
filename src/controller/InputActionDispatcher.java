package controller;

import java.util.HashMap;

import javax.swing.KeyStroke;

/**
 * The InputActionDispatcher maintains the association of all KeyStrokes with the GameEvent they should signal.
 * In this way the user can specify any control scheme they desire while the game is only aware of the GameEvents
 * triggered by those keys.  The job of the dispatcher is to determine which GameEvents if any should be fired by
 * a keystroke it receives from some form of KeyListener. If the signaled KeyStroke is associated with any GameEvents
 * it will notify the GameEventDispatcher of their occurrence.
 * @author JR
 * @version 1.0
 */
public class InputActionDispatcher
{
	private HashMap< KeyStroke, GameEventType > controlMappings;
	private EventNotifier eventDispatcher;
	private final boolean REPORTS_ON = false; // Set this to false to turn of System.out reports
	
	/**
	 * Instantiates a new InputActionDispatcher with an empty Control map and associates it with the provided
	 * EventNotifier.
	 * @param e -- The EventNotifier this InputActionDispatcher should signal events to.
	 */
	InputActionDispatcher( EventNotifier e)
	{
		controlMappings = new HashMap< KeyStroke, GameEventType >();
		setEventNotifier( e );
	}
	
	/**
	 * A private setter for the member variable eventDispatcher.
	 * @param e -- the EventNotifier that all GameEvents should be sent to.
	 */
	private void setEventNotifier( EventNotifier e )
	{
		this.eventDispatcher = e;
	}
	
	/**
	 * Signals that a KeyStroke has been received, and forwards it's associated GameEvent to the GameEventDispatcher.
	 * If no GameEvents are associated with the KeyStroke, then it does nothing.
	 * @param ks -- the KeyStroke received from input.
	 */
	void signalKey( KeyStroke ks )
	{
		GameEventType g = retrieveEvent( ks );
		if( REPORTS_ON )
		{
			System.out.println( "InputActionDispatcher:" );
			System.out.println( "       recieved KeyStroke = " + ks);
			System.out.println( "       associated GameEvent = " + g);
		}
		if( g != null )
		{
			eventDispatcher.notify( g );
			System.out.println("1");
			eventDispatcher.notify( GameEventType.GAME_STATE_CHANGED);
			System.out.println("2");
		}
	}
	
	/**
	 * Private utility method to retrieve the GameEventType associated with the provided KeyStroke.  If no
	 * GameEventType is associated with the KeyStroke returns null.
	 * @param ks -- the key for the desired GameEventType
	 * @return
	 */
	private GameEventType retrieveEvent( KeyStroke ks )
	{
		if( getMap().containsKey( ks ))
			return getMap().get( ks );
		return null;
	}
	
	/**
	 * A private getter for the HashMap of control mappings.
	 * @return the HashMap containing all <KeyStroke, GameEventType> entries.
	 */
	private HashMap<KeyStroke, GameEventType> getMap()
	{
		return this.controlMappings;
	}
	
	/**
	 * Adds a new <KeyStroke, GameEventType> association to the control map.  If this KeyStroke is already
	 * associated with a GameEventType then the GameEventType is replaced with the new one.
	 * @param ks
	 * @param g
	 */
	void registerControl( KeyStroke ks, GameEventType g )
	{
		controlMappings.put(ks, g);
	}
	
	int getNumControls()
	{
		//TODO should return total number of keys in map
		return 0;
	}
	
	KeyStroke getControlKey( int index )
	{
		//TODO should return KeyStroke for control of this index
		return null;
	}
	
	GameEventType getEvent( int index )
	{
		//TODO should return GameEventType
		return null;
	}
	
	
}
