import Model.Model;
import View.View;
import View.ViewUpdater;
import controller.Controller;


public class RunGame
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		AClock clock = new AClock();
		Controller c = new Controller();
		Model m = new Model( c.getEventManager() );
		clock.register(m);
		clock.register(c);

		View v = new View( m.getMapToView(), m.getSelectablesToView(), m.getControllablesToView(), m.getCursor());
		v.start();
		c.initiaize((ViewUpdater)v,m.getControllableModel());
	}

}
