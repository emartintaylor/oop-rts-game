package View;

import java.awt.Graphics2D;
import java.awt.List;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.media.opengl.GL;

import Model.ControllablesToView;
import Model.DisplayablePlayer;
import Model.MapToView;
import Model.ViewItem;
import Model.Displayable.interfaces.Displayable;

class UnitVPLayer extends Layer {
	
	DisplayablePlayer dp;
	BufferedImage tiles[][];
	Graphics2D drawer;

	
	public UnitVPLayer(DisplayablePlayer dp, BufferedImage tiles[][], int pri) {
		super(pri);
		this.tiles = tiles;
		this.dp = dp;
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		ArrayList<Displayable> displayables = dp.getUnits();
		
		for(Displayable current : displayables) {
			System.out.println("placing unit");
			int tileX = current.getLocation().getX();
			int tileY = current.getLocation().getY();
			
			Graphics2D drawer = tiles[tileX][tileY].createGraphics();
		       drawer.drawImage(graphicsMap.getGraphic(current.getName().toUpperCase()), 80, 110, null);

		}
	}

}