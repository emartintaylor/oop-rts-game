package View;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

import Model.Displayable.interfaces.Renderable;

import com.sun.opengl.util.texture.TextureIO;

import decorators.SelectablesToView;

class UnitOverview extends BaseImage implements ViewElement {
	
	Graphics2D drawer;
	private Font font;
	SelectablesToView stv;
	
	public UnitOverview(SelectablesToView stv) {
		needRefresh = true;
		this.stv = stv;
		update();
		baseImage = new BufferedImage(1024, 228, BufferedImage.TYPE_INT_ARGB);
		font = new Font( "Times Roman", Font.PLAIN, 18 );
	}
	
	public void render(GL gl) {
				
		if(needRefresh) {
			needRefresh = false;
	        try {
	            texture = TextureIO.newTexture(baseImage,true);
	        }
	        catch (GLException e) {
	            e.printStackTrace();
	        }
		}
		
		gl.glPushMatrix();
		

		texture.bind();
		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0,1);
			gl.glVertex2d(0.3,0.6);
			
			gl.glTexCoord2d(0,0);
			gl.glVertex2d(0.3,0.1);
			
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(0.7, 0.1);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(0.7, 0.6);
			
			
		gl.glEnd();
		
		gl.glPopMatrix();
		

	}
	
	public void update() {
		
		baseImage = new BufferedImage(450, 450, BufferedImage.TYPE_INT_ARGB);
		needRefresh = true;
		drawer = baseImage.createGraphics();
		drawer.setFont(font);
		drawer.drawImage(graphicsMap.getGraphic("overview"),0,0,null);
		drawer.drawString("Unit Overview",166,50);
		
		 List<Renderable> renderables = stv.getRenderableList();
		 
		 if(renderables != null) {
			 for(Renderable r : renderables) {
				 drawer.drawString(r.getRenderInfo().getName(),20,20);
			 }
		 }
		 
		 
		
	}



}