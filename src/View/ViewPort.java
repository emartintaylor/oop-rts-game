package View;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.media.opengl.GL;
import javax.media.opengl.GLException;

import com.sun.opengl.util.texture.Texture;
import com.sun.opengl.util.texture.TextureIO;

import decorators.SelectablesToView;

import LocationStuff.Location;
import Model.ControllablesToView;
import Model.DisplayablePlayer;
import Model.MapToView;
import Model.SelectedInfoToView;
import Model.Visibility;


class ViewPort extends LayeredViewElement{
	
	BufferedImage tiles[][];
	int tilesX, tilesY;
	Texture tileTextures[][];
	MapToView mapToView;
	SelectablesToView stv;
	SelectedInfoToView cursorToView;
	boolean rbg = true;
	
	public ViewPort(MapToView mapToView, SelectablesToView stv, DisplayablePlayer dp, SelectedInfoToView cursorToView) {
		
		this.mapToView = mapToView;
		this.stv = stv;
		this.cursorToView = cursorToView;
		baseImage = graphicsMap.getGraphic("bg");
		
		layers = new ArrayList<Layer>();
		needRefresh = true;
	
		tilesX = mapToView.getWidth();
		tilesY = mapToView.getHeight();
		
		tiles = new BufferedImage[tilesX][tilesY];
		tileTextures = new Texture[tilesX][tilesY];

		for(int x=0; x<tilesX; x++) {
			for(int y=0; y<tilesY; y++) {
		        tiles[x][y] = new BufferedImage(200, 173, BufferedImage.TYPE_INT_ARGB);
			}
		}

		
		layers.add(new ItemVPLayer(mapToView, tiles, 1));
		layers.add(new TerrainVPLayer(mapToView, tiles, 0));
		layers.add(new ObstacleVPLayer(mapToView, tiles, 2));
		layers.add(new ResourceVPLayer(mapToView, tiles, 3));
		layers.add(new AOEVPLayer(mapToView, tiles, 10));
		layers.add(new UnitVPLayer(dp, tiles, 4));
		
		
	}
	
	public void render(GL gl) {

		refreshBG(gl);
		refreshTex(gl);
		renderBG(gl);
		
		gl.glPushMatrix();
			gl.glTranslated(getCamX(), getCamY(), 0);
			gl.glScaled(1.5,1.5,1);
			
			Location sel = cursorToView.getCurrentLocation();
			Visibility v[][] = mapToView.getVisibility(0);
	
			for(int x=0; x<tilesX; x++) {
				for(int y=0; y<tilesY; y++) {
					if(tileTextures[x][y]!=null) {
	
						tileTextures[x][y].bind();
						if(v[y][x] == Visibility.SHROUDED)
							gl.glColor3d(0.2,0.2,0.2);
						else if( v[y][x] == Visibility.NOTVISIBLE)
							gl.glColor3d(0,0,0);
						
						if(sel.getX()==x && sel.getY()==y)
							gl.glColor3d(1,0.5,0.5);
						
						if(x%2 == 0)
							DrawHelper.drawHex(gl, 0.079*x, -0.09*y, 0.05);
						else
							DrawHelper.drawHex(gl, 0.079*x, -0.09*y-0.044, 0.05);

						gl.glColor3d(1,1,1);
						
					}
				}
			}

		gl.glPopMatrix();
	}
	

	private double getCamY() {
		return 0.6;
	}

	private double getCamX() {
		return 0.35;
	}

	private void renderBG(GL gl) {
		
		texture.bind();
		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0,1);
			gl.glVertex2d(-0.2,1.2);
			
			gl.glTexCoord2d(0,0);
			gl.glVertex2d(-0.2,-0.2);
			
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(1.2, -0.2);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(1.2, 1.2);
			
			
		gl.glEnd();
		
	}
	
	private void refreshBG(GL gl) {
		
		if(rbg) {
	        try {
	            texture = TextureIO.newTexture(baseImage,true);
	        }
	        catch (GLException e) {
	            e.printStackTrace();
	        }
	        rbg = false;
		}
	}

	private void refreshTex(GL gl) {
		
		
		if(needRefresh) {
				
			for(int x=0; x<tilesX; x++) {
				for(int y=0; y<tilesY; y++) {
					if(mapToView.getChanged(x, y) == true ){
						System.out.println("tile was changed");
		                try {
		                    tileTextures[x][y] = TextureIO.newTexture(tiles[x][y],true);
		                }
		                catch (GLException e) {
		                    e.printStackTrace();
		                }
		                mapToView.clrChanged(x, y);
					}
				}
			}
			needRefresh = false;
		}
		
	}

	public void update() {
    	
    	Collections.sort(layers);
		clearTiles();
		for(ViewElement ve : layers) {
			ve.update();
		}
		needRefresh = true;

	}

	private void clearTiles() {
		for(int x=0; x<tilesX; x++) {
			for(int y=0; y<tilesY; y++) {
		        tiles[x][y] = new BufferedImage(200, 173, BufferedImage.TYPE_INT_ARGB);
			}
		}
	}
	
	public void centerOnSelected() {

	}

}