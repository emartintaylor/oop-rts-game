package View;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import javax.media.opengl.GL;

import Model.MapToView;
import Model.ViewItem;
import Model.ViewObstacle;

class ObstacleVPLayer extends Layer {
	
	MapToView mapToView;
	BufferedImage tiles[][];
	
	
  	public ObstacleVPLayer(MapToView mapToView, BufferedImage tiles[][], int pri) {
  		super(pri);
		this.tiles = tiles;
		this.mapToView = mapToView;
		update();
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		LinkedList<ViewObstacle> obstacles = mapToView.getObstacles();
		
		for(ViewObstacle current : obstacles) {
			int tileX = current.getX();
			int tileY = current.getY();
			
			Graphics2D drawer = tiles[tileX][tileY].createGraphics();
		       drawer.drawImage(graphicsMap.getGraphic(current.getRenderObstacle()), 80, 30, null);

		}
	}

}