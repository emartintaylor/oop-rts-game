package View;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

import com.sun.opengl.util.texture.TextureIO;

import decorators.SelectablesToView;

class SelectionHierarchy extends BaseImage implements ViewElement {
	
	Graphics2D drawer;
	private Font font;
	SelectablesToView selectablesToView;
	
	public SelectionHierarchy(SelectablesToView stv) {
		selectablesToView = stv;
		update();
		font = new Font( "Times Roman", Font.PLAIN, 20 );
		baseImage = new BufferedImage(169, 330, BufferedImage.TYPE_INT_ARGB);
	}
	
	public void render(GL gl) {
				
		if(needRefresh) {
			needRefresh = false;
	        try {
	            texture = TextureIO.newTexture(baseImage,true);
	        }
	        catch (GLException e) {
	            e.printStackTrace();
	        }
		}
		
		gl.glPushMatrix();
		
		texture.bind();
		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0,1);
			gl.glVertex2d(0,0.38);
			
			gl.glTexCoord2d(0,0);
			gl.glVertex2d(0,0);
			
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(0.17, 0);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(0.17, 0.38);
			
			
		gl.glEnd();
		
		gl.glPopMatrix();
		

	}
	
	public void update() {
		
		baseImage = new BufferedImage(169, 330, BufferedImage.TYPE_INT_ARGB);
		drawer = baseImage.createGraphics();
		drawer.drawImage(graphicsMap.getGraphic("selection"),0,0,null);
		
		//testing
		List<String> testList = selectablesToView.getSelectionHierarchy();		 
		 drawer.setFont(font);
		 
		 if(testList != null ) {
			 
			 int x = 0;
			 for(String s : testList) {
				if(s != null) {
					drawer.drawImage(graphicsMap.getGraphic("selectbox"),10,55 + 50*x,null);
					drawer.drawString(s, 40, 82 + 50*x);
					x++;
				}
			 }
		 }
		 
		 needRefresh = true;
	}



}