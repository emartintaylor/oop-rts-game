package View;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

import Model.DisplayablePlayer;

import com.sun.opengl.util.texture.TextureIO;

class ResourceDisplay extends BaseImage implements ViewElement {
	
	Graphics2D drawer;
	private Font font;
	DisplayablePlayer dp;
	
	public ResourceDisplay(DisplayablePlayer dp) {
		this.dp = dp;
		update();
		font = new Font( "Times Roman", Font.PLAIN, 20 );
		baseImage = new BufferedImage(330, 79, BufferedImage.TYPE_INT_ARGB);

	}
	
	public void render(GL gl) {
				
		if(needRefresh) {
			needRefresh = false;
	        try {
	            texture = TextureIO.newTexture(baseImage,true);
	        }
	        catch (GLException e) {
	            e.printStackTrace();
	        }
		}
		
		gl.glPushMatrix();
		

		texture.bind();
		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0,0);
			gl.glVertex2d(0.7,0);
			
			gl.glTexCoord2d(1,0);
			gl.glVertex2d(0.98,0);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(0.98, 0.13);
			
			gl.glTexCoord2d(0, 1);
			gl.glVertex2d(0.7, 0.13);
			
			
		gl.glEnd();
		
		gl.glPopMatrix();
		

	}
	
	public void update() {
		
		 baseImage = new BufferedImage(330, 79, BufferedImage.TYPE_INT_ARGB);
		 needRefresh = true;
		 drawer = baseImage.createGraphics();
		 
		 drawer.drawImage(graphicsMap.getGraphic("resourcedisplay"),0,0,null);
		 
		 drawer.drawImage(graphicsMap.getGraphic("food"),30,46,null);
		 drawer.drawString(""+dp.getFoodCount(),63,59);
		 drawer.drawImage(graphicsMap.getGraphic("ore"),120,45,null);
		 drawer.drawString(""+dp.getOreCount(),149,59);
		 drawer.drawImage(graphicsMap.getGraphic("energy"),214,44,null);
		 drawer.drawString(""+dp.getEnergyCount(),240,59);


	}



}