package View;

import java.awt.image.BufferedImage;

import com.sun.opengl.util.texture.Texture;

abstract class BaseImage {
	
	int width;
	int height;
	BufferedImage baseImage;
	Texture texture;
	boolean needRefresh;
	GraphicsMap graphicsMap = GraphicsMap.getGraphicsMap();
	

	public BufferedImage getBaseImage() {
		return baseImage;
	}
	
	public void refresh() {
		needRefresh = true;
	}
	
	
}