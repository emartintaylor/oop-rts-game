package View;

import javax.media.opengl.GL;

class DrawHelper {
	
	public static void drawHex(GL gl, double x, double y, double sideLength) {
		
		double r = sideLength*Math.cos(0.523);  
		double h = sideLength*Math.sin(0.523);

		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0.0, 0.5);
			gl.glVertex2d(x-sideLength*0.5-h,y);

			gl.glTexCoord2d(0.3, 0.0);
			gl.glVertex2d(x-sideLength*0.5, y-r);
			
			gl.glTexCoord2d(0.7, 0.0);
			gl.glVertex2d(x+sideLength*0.5, y-r);
	
			gl.glTexCoord2d(1.0, 0.5);
			gl.glVertex2d(x+sideLength*0.5+h, y);
	
			gl.glTexCoord2d(0.7, 1.0);
			gl.glVertex2d(x+sideLength*0.5, y+r);
	
			gl.glTexCoord2d(0.3, 1.0);
			gl.glVertex2d(x-sideLength*0.5, y+r);

		gl.glEnd();
	}
	
	public static void drawQuad(GL gl, double x, double y, double width, double height) {
		

		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0.0, 0.0);
			gl.glVertex2d(x,y);

			gl.glTexCoord2d(1, 0.0);
			gl.glVertex2d(x+width,y);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(x+width,y+height);
	
			gl.glTexCoord2d(0, 1);
			gl.glVertex2d(x , y+height);
	

		gl.glEnd();
	}
}