package View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.swing.JFrame;

import Model.ControllablesToView;
import Model.DisplayablePlayer;
import Model.MapToView;
import Model.SelectedInfoToView;


import com.sun.opengl.util.FPSAnimator;

import decorators.SelectablesToView;

public class View extends JFrame implements ViewUpdater{


	private static final long serialVersionUID = 1L;
	
	private static final int windowWidth = 1080;
	private static final int windowHeight = 800;
	private GLCanvas canvas;
	private FPSAnimator animator;
	
	List<ViewElement> permElements;
	ViewElement altElement;
	
	//alt elements
	ViewPort viewPort;
	SelectionHierarchy selectionHierarchy;
	StructureOverview structureOverview;
	TechTreeOverview techTreeOverview;
	UnitOverview unitOverview;
	
	
	public View(MapToView mapToView, SelectablesToView stv, DisplayablePlayer dp, SelectedInfoToView cursorToView) {
		
		permElements = new ArrayList<ViewElement>();
		
		//View Elements
		viewPort = new ViewPort(mapToView, stv, dp, cursorToView);
		permElements.add(new HUD(stv));
		permElements.add(new SelectionHierarchy(stv));
		permElements.add(new ResourceDisplay(dp));

		
		altElement = null;
		selectionHierarchy = new SelectionHierarchy(stv);
		structureOverview = new StructureOverview();
		techTreeOverview = new TechTreeOverview();
		unitOverview = new UnitOverview(stv);
		
		//JFrame stuff
		setSize(windowWidth, windowHeight);
		setTitle("iter3");
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		GraphicListener listener = new GraphicListener();
		canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
	    animator = new FPSAnimator(canvas, 100);

	    this.validate();
	}
	
	public void start() {
		animator.start();
	}


	public void updateAll() {
		for(ViewElement ve : permElements) {
			ve.update();
		}
		viewPort.update();
		updateCurrent();
	}


	public void updateCurrent() {
		for(ViewElement ve : permElements) 
			ve.update();
		
		if(altElement!=null) 
			altElement.update();
	}
	
	class GraphicListener implements GLEventListener {



		public void display(GLAutoDrawable drawable) {
			
			GL gl = drawable.getGL();
			gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
			
			
			viewPort.render(gl);
			for(ViewElement ve : permElements) {
				ve.render(gl);
			}
			
			
			if(altElement != null)
				altElement.render(gl);
			
		}

		public void displayChanged(GLAutoDrawable drawable, boolean arg1, boolean arg2) {

		}

		public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h) {	
		
			GL gl = drawable.getGL();
			gl.glViewport(0, 0, w, h);
			gl.glMatrixMode(GL.GL_PROJECTION);
			gl.glLoadIdentity();
			gl.glOrtho(0.0, 1.0, 1.0, 0.0, -1.0, 1.0);	
			gl.glMatrixMode(GL.GL_MODELVIEW);
			gl.glLoadIdentity();
		}

		public void init(GLAutoDrawable drawable) {
		
			try {
			
				GL gl = drawable.getGL();									//get GL pipeline
				gl.glEnable(GL.GL_TEXTURE_2D);								//enable 2D textures
				gl.glEnable(GL.GL_BLEND);
				gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
				update();


			} 

			catch (GLException e) {
				e.printStackTrace();
			} 
		}
		
		public void update() {
			for(ViewElement ve : permElements) {
				ve.update();
			}
			viewPort.update();
			unitOverview.update();
		}

	}

	@Override
	public void showMainScreen()
	{
		altElement = null;
		
	}

	@Override
	public void showStructOverView()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showTechTreeOverView()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showUnitOverView()
	{
		System.out.println("show unit over");
		altElement = unitOverview;
		
	}

	@Override
	public void attachKeyListener(KeyListener k)
	{
		canvas.addKeyListener(k);
		canvas.setFocusable(true);
	
	}
}