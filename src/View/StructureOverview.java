package View;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

import com.sun.opengl.util.texture.TextureIO;

class StructureOverview extends BaseImage implements ViewElement {
	
	Graphics2D drawer;
	private Font font;
	
	public StructureOverview() {
		update();
		font = new Font( "Times Roman", Font.PLAIN, 20 );
		baseImage = new BufferedImage(450, 450, BufferedImage.TYPE_INT_ARGB);
	}
	
	public void render(GL gl) {
				
		if(needRefresh) {
			needRefresh = false;
	        try {
	            texture = TextureIO.newTexture(baseImage,true);
	        }
	        catch (GLException e) {
	            e.printStackTrace();
	        }
		}
		
		gl.glPushMatrix();
		

		texture.bind();
		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0,1);
			gl.glVertex2d(0.3,0.6);
			
			gl.glTexCoord2d(0,0);
			gl.glVertex2d(0.3,0.1);
			
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(0.7, 0.1);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(0.7, 0.6);
			
			
		gl.glEnd();
		
		gl.glPopMatrix();
		

	}
	
	public void update() {
		
		baseImage = new BufferedImage(450, 450, BufferedImage.TYPE_INT_ARGB);
		needRefresh = true;
		drawer = baseImage.createGraphics();
		drawer.drawImage(graphicsMap.getGraphic("overview"),0,0,null);
		
	}



}