package View;

import java.awt.event.KeyListener;

public interface ViewUpdater {
	
	public void showMainScreen();
	public void showUnitOverView();
	public void showStructOverView();
	public void showTechTreeOverView();
	public void updateCurrent();
	public void updateAll();
	public void attachKeyListener(KeyListener k);
}