package View;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.media.opengl.GL;
import Model.MapToView;

class TerrainVPLayer extends Layer {
	
	MapToView mapToView;
	BufferedImage tiles[][];
	Graphics2D drawer;

	
	public TerrainVPLayer(MapToView mapToView, BufferedImage tiles[][], int pri) {
		super(pri);
		this.tiles = tiles;
		this.mapToView = mapToView;
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		String terrains[][] = mapToView.getTerrain();
		
		for(int x=0; x<mapToView.getWidth(); x++) {
			for(int y=0; y<mapToView.getHeight(); y++) {
		       drawer = tiles[x][y].createGraphics();
		       drawer.drawImage(graphicsMap.getGraphic(terrains[y][x]), 0, 0, null);
			}
		}


	}

}