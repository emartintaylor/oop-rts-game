package View;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import javax.media.opengl.GL;
import Model.MapToView;
import Model.ViewAOE;
import Model.ViewObstacle;
import Model.Visibility;

class AOEVPLayer extends Layer {
	
	
	MapToView mapToView;
	BufferedImage tiles[][];
	Graphics2D drawer;
	
	
  	public AOEVPLayer(MapToView mapToView, BufferedImage tiles[][], int pri) {
  		super(pri);
		this.tiles = tiles;
		this.mapToView = mapToView;
		update();
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		LinkedList<ViewAOE> obstacles = mapToView.getAreaEffects();
		
		for(ViewAOE current : obstacles) {
			int tileX = current.getX();
			int tileY = current.getY();
			
			drawer = tiles[tileX][tileY].createGraphics();
		       drawer.drawImage(graphicsMap.getGraphic(current.getRenderAOE()), 120, 70, null);

		}
		
	}

}