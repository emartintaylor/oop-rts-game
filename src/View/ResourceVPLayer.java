package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import javax.media.opengl.GL;

import Model.DisplayableResource;
import Model.MapToView;
import Model.ViewObstacle;

class ResourceVPLayer extends Layer {
	
	MapToView mapToView;
	BufferedImage tiles[][];
	private Font font;
	
  	public ResourceVPLayer(MapToView mapToView, BufferedImage tiles[][], int pri) {

  		super(pri);
		this.tiles = tiles;
		this.mapToView = mapToView;
		
		font = new Font( "Times Roman", Font.BOLD, 20 );
		update();
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		LinkedList<DisplayableResource> resources = mapToView.getResources();
		
		for(DisplayableResource current : resources) {
			int tileX = current.getX();
			int tileY = current.getY();
			
			Graphics2D drawer = tiles[tileX][tileY].createGraphics();
			drawer.setFont(font);
			drawer.setColor(Color.RED);
		    drawer.drawString("" + current.getEnergy(),55,140);
		    drawer.setColor(Color.CYAN);
		    drawer.drawString("" + current.getOre(),87,140);
		    drawer.setColor(Color.ORANGE);
		    drawer.drawString("" + current.getFood(),115,140);

		}
	}
}