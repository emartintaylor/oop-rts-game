package View;

import java.util.List;


abstract class LayeredViewElement extends BaseImage implements ViewElement {
	
	List<Layer> layers;
	
	
	public void render() {
		
	}
	
	public void update() {
		
	}
}