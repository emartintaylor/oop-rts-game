package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

import Model.Displayable.Range;
import Model.Displayable.interfaces.Renderable;
import Util.EntityInfo;
import Util.Pair;

import com.sun.opengl.util.texture.TextureIO;

import decorators.SelectablesToView;

class HUD extends BaseImage implements ViewElement {
	
	Graphics2D drawer;
	private Font font;
	int test;
	SelectablesToView stv;
	
	public HUD(SelectablesToView stv) {
		needRefresh = true;
		this.stv = stv;
		update();
		baseImage = new BufferedImage(1024, 228, BufferedImage.TYPE_INT_ARGB);
		font = new Font( "Times Roman", Font.PLAIN, 18 );
		test = 0;
	}
	
	public void render(GL gl) {
						
		if(needRefresh) {
			needRefresh = false;
	        try {
	            texture = TextureIO.newTexture(baseImage,true);
	        }
	        catch (GLException e) {
	            e.printStackTrace();
	        }
		}
		
		gl.glPushMatrix();
		
		texture.bind();
		gl.glBegin(GL.GL_POLYGON);
		
			gl.glTexCoord2d(0,1);
			gl.glVertex2d(0,1.03);
			
			gl.glTexCoord2d(0,0);
			gl.glVertex2d(0,0.7);
			
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(1, 0.7);
			
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(1, 1.03);
			
			
		gl.glEnd();
		
		gl.glPopMatrix();
		

	}
	
	public void update() {
		
		 baseImage = new BufferedImage(1024, 228, BufferedImage.TYPE_INT_ARGB);
		 drawer = baseImage.createGraphics();
		 
		 drawer.drawImage(graphicsMap.getGraphic("hud"),0,0,null);
		 
		 
		 List<Renderable> renderables = new ArrayList<Renderable>();
		 
		 Renderable r = stv.getCurrentRenderable();
		 
		 if(r!=null) {
			 EntityInfo ei = r.getRenderInfo();
			 int x = 0;
			 for(Pair<String> p : ei.getInfo()) {
				 drawer.drawString(p.getA(), 820, 60 + 15*x);
				 drawer.drawString(p.getB(), 910, 60 + 15*x);
				 x++;
			 }
			 
			 x = 0;
			 drawer.setColor(Color.GREEN);
			 System.out.println(ei.getType());
			 drawer.drawImage(graphicsMap.getGraphic(ei.getType().toUpperCase()),290,80,null);
			 drawer.fillRect(285,105,(int)(r.hpPercent()*30),7);
			 
			 drawer.drawImage(graphicsMap.getGraphic((ei.getType()+"port").toUpperCase()),655,83,null);

		 }
		 

		 
		 
		 needRefresh = true;
	}




}