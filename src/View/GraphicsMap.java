package View;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;

class GraphicsMap {
	
	private static final GraphicsMap graphicsMap = new GraphicsMap();
	
	private HashMap<String,BufferedImage> graphics;
	
	public GraphicsMap() {
		graphics = new HashMap<String,BufferedImage>();
		
		try{
		//interfaces
		graphics.put("bg", ImageIO.read(new File("art/bg.png")));
		graphics.put("hud", ImageIO.read(new File("art/hud.png")));
		graphics.put("overview", ImageIO.read(new File("art/overview.png")));
		graphics.put("selection", ImageIO.read(new File("art/selection.png")));
		graphics.put("selectbox", ImageIO.read(new File("art/selectbox.png")));
		graphics.put("resourcedisplay", ImageIO.read(new File("art/resourcedisplaynew.png")));

		//terrain
		graphics.put("GRASS", ImageIO.read(new File("art/grass.png")));
		graphics.put("DESERT", ImageIO.read(new File("art/desert.png")));
		graphics.put("ROCKY", ImageIO.read(new File("art/rocky.png")));
		graphics.put("BORDER", ImageIO.read(new File("art/rocky.png")));
		graphics.put("RIVER", ImageIO.read(new File("art/river.png")));
		
		//items
		graphics.put("SWORD", ImageIO.read(new File("art/sword.png")));
		graphics.put("HELMET", ImageIO.read(new File("art/helmet.png")));
		graphics.put("BANGLE", ImageIO.read(new File("art/bangle.png")));
		graphics.put("SHIELD", ImageIO.read(new File("art/shield.png")));

		//resources
		graphics.put("food", ImageIO.read(new File("art/food.png")));
		graphics.put("ore", ImageIO.read(new File("art/ore.png")));
		graphics.put("energy", ImageIO.read(new File("art/energy.png")));
		
		//aoe
		graphics.put("HEALEFFECT", ImageIO.read(new File("art/redcross.png")));
		graphics.put("FULLRECOVEREFFECT", ImageIO.read(new File("art/redcross.png")));
		graphics.put("DAMAGEEFFECT", ImageIO.read(new File("art/skull.png")));
		graphics.put("INSTANTDEATHEFFECT", ImageIO.read(new File("art/skull.png")));
		
		//obstacles
		graphics.put("MOUNTAIN", ImageIO.read(new File("art/mountain.png")));
		graphics.put("TREE", ImageIO.read(new File("art/mountain.png")));
		
		//units
		graphics.put("MELEE", ImageIO.read(new File("art/melee.png")));
		graphics.put("RANGE", ImageIO.read(new File("art/ranged.png")));
		
		graphics.put("EXPLORER", ImageIO.read(new File("art/colonist.png")));
		graphics.put("COLONIST", ImageIO.read(new File("art/colonist.png")));
		
		graphics.put("MELEEPORT", ImageIO.read(new File("art/meleeport.png")));
		graphics.put("RANGEPORT", ImageIO.read(new File("art/rangedport.png")));
		graphics.put("COLONISTPORT", ImageIO.read(new File("art/colonistport.png")));
		graphics.put("EXPLORERPORT", ImageIO.read(new File("art/colonistport.png")));
		

		}
		
        catch(Exception e){
            System.out.println("Image Load Error");
        }
	}
	
	public BufferedImage getGraphic(String s) {
		return graphics.get(s);
	}
	
	public static GraphicsMap getGraphicsMap() {
		return graphicsMap;
	}
}