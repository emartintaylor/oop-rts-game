package View;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.media.opengl.GL;

import Model.DisplayablePlayer;
import Model.Displayable.interfaces.Displayable;

class StructureVPLayer extends Layer {
	DisplayablePlayer dp;
	BufferedImage tiles[][];
	Graphics2D drawer;

	
	public StructureVPLayer(DisplayablePlayer dp, BufferedImage tiles[][], int pri) {
		super(pri);
		this.tiles = tiles;
		this.dp = dp;
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		ArrayList<Displayable> displayables = dp.getStructures();
		
		for(Displayable current : displayables) {
			System.out.println("placing unit");
			int tileX = current.getLocation().getX();
			int tileY = current.getLocation().getY();
			
			Graphics2D drawer = tiles[tileX][tileY].createGraphics();
		       drawer.drawImage(graphicsMap.getGraphic("MELEE"), 80, 70, null);

		}
	}

}