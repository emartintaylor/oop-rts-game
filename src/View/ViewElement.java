package View;

import javax.media.opengl.GL;

public interface ViewElement {
	public void render(GL gl);
	public void update();
}