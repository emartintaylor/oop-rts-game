package View;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import javax.media.opengl.GL;

import Model.MapToView;
import Model.ViewItem;

class ItemVPLayer extends Layer {
	
	MapToView mapToView;
	BufferedImage tiles[][];
	
	
  	public ItemVPLayer(MapToView mapToView, BufferedImage tiles[][], int pri) {
  		super(pri);
		this.tiles = tiles;
		this.mapToView = mapToView;
		update();
	}
	
	public void render(GL gl) {
		
		
	}
	
	public void update() {
		
		LinkedList<ViewItem> items = mapToView.getItems();
		
		for(ViewItem current : items) {
			int tileX = current.getX();
			int tileY = current.getY();
			
			Graphics2D drawer = tiles[tileX][tileY].createGraphics();
		       drawer.drawImage(graphicsMap.getGraphic(current.getRenderItem()), 50, 70, null);

		}
	}

}