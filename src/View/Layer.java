package View;

abstract class Layer extends BaseImage implements ViewElement, Comparable<Layer> {
	private int priority;
	public Layer(int pri) {priority = pri;}
	public int getPriority() {return priority;}
	public int compareTo(Layer l) {
		if(priority > l.getPriority())
			return 1;
		else if(priority == l.getPriority())
			return 0;
		else 
			return -1;
	}
}