package LocationStuff;

public interface Locateable {
	public Location getLocation();
}
