package LocationStuff;

public class ChLocation implements Location{

	int x,y;
	public ChLocation(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setLocation(Location local) {
		this.x = local.getX();
		this.y = local.getY();
	}
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}
	
	public boolean equals(Object o){
		if(o instanceof Location){
			return (((Location) o).getX() == this.x && ((Location) o).getY() == this.y);
		}
		else return false;
	}
	public String toString()
	{
		return "("+x+","+y+")";
	}
}
