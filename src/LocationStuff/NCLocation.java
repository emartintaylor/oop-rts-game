package LocationStuff;

public class NCLocation implements Location {

	private int x;
	private int y;
	public NCLocation(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public boolean equals(Object o){
		if(o instanceof Location){
			return (((Location) o).getX() == this.x && ((Location) o).getY() == this.y);
		}
		else return false;
	}

}
