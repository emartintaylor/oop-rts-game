package LocationStuff;

	public interface Location {
		public int getX();
		public int getY();
		public boolean equals(Object o);
	}

