package decorators;

import java.util.ArrayList;

import Model.Army;
import Model.CommandBuilder;
import Model.FactoryWarehouse;
import Model.PlayerCursor;
import Model.Commands.Command;
import Model.Displayable.Rally;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Renderable;
import Util.CircularList;

public class RallyPointDecorator extends GameDecorator
{
	private CircularList<Rally>  rallyPoints;
	private CircularList<Army>  armies;
	private Rally currentInstance;
	
	private CircularList<Command> supportedCommands;
	private CircularList<Command> nullCommands = new CircularList<Command>();
	//private Command currentCommand;
	
	public RallyPointDecorator() {}
	
	public RallyPointDecorator(PlayerCursor curs, FactoryWarehouse currentFactoryWarehouse, CommandBuilder commandBuilder)
	{
		this.currentFactoryWarehouse = currentFactoryWarehouse;
		this.commandBuilder = commandBuilder;
		this.curs = curs;
		rallyPoints = currentFactoryWarehouse.getRallyList();
		//armies = currentFactoryWarehouse.getArmyList();
	}
	
	public void nextType() {}
	public void prevType() {}
	
	public void nextOrder()
	{	
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandForward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void prevOrder()
	{	
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getPrevious();
			commandBuilder.cycleCommandBackward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void nextInstance()
	{
		currentInstance = rallyPoints.getNext();
		curs.setCurrentControllable(rallyPoints.getCurrent());
		System.out.println(currentInstance);
		System.out.println(currentInstance.getName() + " " + currentInstance.getID());
		supportedCommands = currentInstance.getCommands();
		commandBuilder.resetCommandList(supportedCommands);
	}
	public void prevInstance()
	{
		currentInstance = rallyPoints.getPrevious();
		curs.setCurrentControllable(rallyPoints.getCurrent());
		System.out.println(currentInstance);
		System.out.println(currentInstance.getName() + " " + currentInstance.getID());
		supportedCommands = currentInstance.getCommands();
		commandBuilder.resetCommandList(supportedCommands);
	}	

	public ArrayList<String> getSelectionHierarchy()
	{
		ArrayList<String> selHierarchy = new ArrayList<String>();
		selHierarchy.add("Rally Points");
		if(currentInstance == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentInstance.getName() + " " + currentInstance.getID());
		}
		System.out.println("===============================\n" + commandBuilder.getCommandName());
		selHierarchy.add(commandBuilder.getCommandName());
		
		return selHierarchy;
	}
	
	public Renderable getCurrentRenderable()
	{
		if(currentInstance != null)
			return currentInstance;
		else
			return null;
	}
	
	public String toString()
	{
		return "Rally Point Decorator";
	}
}
