package decorators;

import java.util.ArrayList;
import Model.Displayable.interfaces.Renderable;

public interface SelectablesToView {
	ArrayList<String> getSelectionHierarchy();	//Only Main Screen -- Unit, Melee, Melee1, Move
	Renderable getCurrentRenderable();			//All Screens -- (Main Screen - Instance) (Overview - Selection)
	ArrayList<Renderable> getRenderableList();	//Only Overviews -- Directory
	String getHeader();							//Only Overviews -- Directory Title 
	ArrayList<Renderable> getUnassigned();		//Only Overviews -- Unassigned Units
	int getNumAvailableUniv();					//Only Overviews -- Number of Available Universities
}
