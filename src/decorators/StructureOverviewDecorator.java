package decorators;

import java.util.ArrayList;

import Model.FactoryWarehouse;
import Model.Displayable.Structure;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Renderable;
import Util.CircularList;

public class StructureOverviewDecorator extends OverviewDecorator{
	
	private CircularList<CircularList<Structure>> structures;
	
	private CircularList<Structure> currentType;
	private Structure currentInstance;
	
	public StructureOverviewDecorator() {}
	
	public StructureOverviewDecorator(FactoryWarehouse currentFactoryWarehouse)
	{
		this.currentFactoryWarehouse = currentFactoryWarehouse;
	}

	@Override
	public ArrayList<String> getSelectionHierarchy()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Renderable getCurrentRenderable()
	{
		if(currentInstance != null)
		{
			currentType = structures.getCurrent();
			return currentInstance = currentType.getCurrent();
		}
		else
			return null;
	}

	@Override
	public void MenuDown()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void MenuUp()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeDirectoryBack()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeDirectoryInto()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fireCommand()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextMenu()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prevMenu()
	{
		// TODO Auto-generated method stub
		
	}
}
