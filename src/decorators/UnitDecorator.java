package decorators;

import java.util.ArrayList;

import Model.CommandBuilder;
import Model.FactoryWarehouse;
import Model.PlayerCursor;
import Model.Commands.Command;
import Model.Displayable.Moveable;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Renderable;
import Util.CircularList;

public class UnitDecorator extends GameDecorator
{
	private CircularList<CircularList<Unit>> units;

	private CircularList<Unit> currentType;
	private Moveable currentInstance;
	
	private CircularList<Command> supportedCommands; 
	private CircularList<Command> nullCommands = new CircularList<Command>();
	private PlayerCursor curs;
	
	public UnitDecorator() {}
	
	public UnitDecorator(PlayerCursor curs, FactoryWarehouse currentFactoryWarehouse, CommandBuilder commandBuilder)
	{
		this.currentFactoryWarehouse = currentFactoryWarehouse;
		this.commandBuilder = commandBuilder;
		this.curs = curs;
		units = currentFactoryWarehouse.getUnitsList();
	}
	
	public void nextType()
	{
		CircularList<Unit> current = units.getNext();
		int size = units.size();
		for(int i = 0; i < size; i++)
		{
			System.out.println("Size current: " + current.size());
			if( current.isEmpty() )
			{
				current = units.getNext();
			}
			else
			{
				currentType = current;
				currentInstance = currentType.get(0);
				curs.setCurrentControllable(currentInstance);
				System.out.println(currentInstance.getName() + " " + currentInstance.getID());
				commandBuilder.resetCommandList(currentInstance.getCommands());
				return;
			}
		}	
			
	}
	public void prevType()
	{
		CircularList<Unit> current = units.getPrevious();
		int size = units.size();
		for(int i = 0; i < size; i++)
		{
			if( current.isEmpty() )
			{
				current = units.getPrevious();
			}
			else
			{
				currentType = current;
				currentInstance = currentType.get(0);
				curs.setCurrentControllable(currentType.getCurrent());
				supportedCommands = currentInstance.getCommands();
				System.out.println(currentInstance.getName() + " " + currentInstance.getID());
				commandBuilder.resetCommandList(currentInstance.getCommands());
				return;
			}
		}	
	}
	public void nextOrder()
	{
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandForward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void prevOrder()
	{
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandBackward();
			//.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void nextInstance()
	{
		if(currentInstance != null)
		{
			currentInstance = currentType.getNext();
			curs.setCurrentControllable(currentType.getCurrent());
			System.out.println(currentInstance.getName() + " " + currentInstance.getID());
			supportedCommands = currentInstance.getCommands();
			commandBuilder.resetCommandList(supportedCommands);
		}
	}
	public void prevInstance()
	{
		if(currentInstance != null)
		{
			currentInstance = currentType.getPrevious();
			curs.setCurrentControllable(currentType.getCurrent());
			System.out.println(currentInstance.getName() + " " + currentInstance.getID());
			supportedCommands = currentInstance.getCommands();
			commandBuilder.resetCommandList(supportedCommands);
		}
	}	
	
	public ArrayList<String> getSelectionHierarchy()
	{
		ArrayList<String> selHierarchy = new ArrayList<String>();
		selHierarchy.add("Unit");
		if(currentType == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentInstance.getName());
		}
		if(currentInstance == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentInstance.getName() + " " + currentInstance.getID());
		}
		selHierarchy.add(commandBuilder.getCommandName());
			
		return selHierarchy;
	}

	public Renderable getCurrentRenderable()
	{
		if(currentInstance != null)
			return currentInstance;
		else
			return null;
	}
	
	public String toString()
	{
		return "Unit Decorator";
	}
}
