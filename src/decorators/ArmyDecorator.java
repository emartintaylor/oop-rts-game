package decorators;

import java.util.ArrayList;

import Model.Army;
import Model.CommandBuilder;
import Model.FactoryWarehouse;
import Model.PlayerCursor;
import Model.Commands.Command;
import Model.Displayable.Moveable;
import Model.Displayable.Rally;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Renderable;
import Util.CircularList;

public class ArmyDecorator extends GameDecorator
{
	private CircularList<Rally>  rallies;
	private CircularList<CircularList<Moveable>> armies;
	private CircularList<String> armyNames;

	private CircularList<Unit> currentType;
	private Unit currentInstance;
	private String currentName;
	
	private CircularList<Command> supportedCommands;
	private CircularList<Command> nullCommands = new CircularList<Command>();
	//private Command currentCommand;
	
	public ArmyDecorator() {}
	
	public ArmyDecorator(PlayerCursor curs, FactoryWarehouse currentFactoryWarehouse, CommandBuilder commandBuilder)
	{
		this.currentFactoryWarehouse = currentFactoryWarehouse;
		this.commandBuilder = commandBuilder;
		this.curs = curs;
		rallies = currentFactoryWarehouse.getRallyList();
	}
	
	public void initArmies()
	{
		//armies = rallies.getArmyList();
		//armyNames = rallies.getArmyNameList();
	}
	
	public void nextType()
	{
		//CircularList<Unit> current = armies.getNext();
		currentName = armyNames.getNext();
		int size = armies.size();
		for(int i = 0; i < size; i++)
		{
			//System.out.println("Size current: " + current.size());
			//if( current.isEmpty() )
			{
				//current = armies.getNext();
				currentName = armyNames.getNext();
			}
			//else
			{
//				currentType = current;
				currentInstance = currentType.get(0);
				curs.setCurrentControllable(currentType.getCurrent());
				supportedCommands = currentInstance.getCommands();
				System.out.println(currentInstance.getName() + " " + currentInstance.getID());
				commandBuilder.resetCommandList(currentInstance.getCommands());
				return;
			}
		}
	}
	public void prevType()
	{
	///	CircularList<Unit> current = armies.getPrevious();
		currentName = armyNames.getPrevious();
		int size = armies.size();
		for(int i = 0; i < size; i++)
		{
		//	if( current.isEmpty() )
			{
			//	current = armies.getPrevious();
				currentName = armyNames.getPrevious();
			}
	//		else
			{
		//		currentType = current;
				currentInstance = currentType.get(0);
				curs.setCurrentControllable(currentType.getCurrent());
				supportedCommands = currentInstance.getCommands();
				System.out.println(currentInstance.getName() + " " + currentInstance.getID());
				commandBuilder.resetCommandList(currentInstance.getCommands());
				return;
			}
		}	
	}
	public void nextOrder()
	{
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandForward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void prevOrder()
	{
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandBackward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void nextInstance()
	{
		if(currentInstance != null)
		{
			currentInstance = currentType.getNext();
			curs.setCurrentControllable(currentType.getCurrent());
			System.out.println(currentInstance.getName() + " " + currentInstance.getID());
			supportedCommands = currentInstance.getCommands();
			commandBuilder.resetCommandList(supportedCommands);
		}
	}
	public void prevInstance()
	{
		if(currentInstance != null)
		{
			currentInstance = currentType.getPrevious();
			curs.setCurrentControllable(currentType.getCurrent());
			System.out.println(currentInstance.getName() + " " + currentInstance.getID());
			supportedCommands = currentInstance.getCommands();
			commandBuilder.resetCommandList(supportedCommands);
		}
	}	

	public ArrayList<String> getSelectionHierarchy()
	{
		ArrayList<String> selHierarchy = new ArrayList<String>();
		selHierarchy.add("RP" + rallies.getCurrent().getID() + " Army");
		if(currentName == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentName);
		}
		if(currentInstance == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentInstance.getName() + " " + currentInstance.getID());
		}
		selHierarchy.add(commandBuilder.getCommandName());
		
		return selHierarchy;
	}

	public Renderable getCurrentRenderable()
	{
		if(currentInstance != null)
			return currentInstance;
		else
			return null;
	}
	
	public String toString()
	{
		return "Army Decorator";
	}
}
