package decorators;

import java.util.ArrayList;

import Model.FactoryWarehouse;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Renderable;
import Util.CircularList;

public class UnitOverviewDecorator extends OverviewDecorator{
	
	private CircularList<CircularList<Unit>> units;
	
	private CircularList<Unit> currentType;
	private Unit currentInstance;
	
	public UnitOverviewDecorator() {}
	
	public UnitOverviewDecorator(FactoryWarehouse currentFactoryWarehouse)
	{
		this.currentFactoryWarehouse = currentFactoryWarehouse;
	}

	public ArrayList<String> getSelectionHierarchy()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public Renderable getCurrentRenderable()
	{
		if(currentInstance != null)
		{
			currentType = units.getCurrent();
			return currentInstance = currentType.getCurrent();
		}
		else
			return null;
	}

	public void MenuDown() {
		
	}

	public void MenuUp() {
		
	}

	public void changeDirectoryBack() {
		
	}

	public void changeDirectoryInto() {
		
	}

	public void fireCommand() {
		
	}

	public void nextMenu() {
		
	}

	public void prevMenu() {
		
	}
}
