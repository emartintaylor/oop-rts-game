package decorators;

import Model.FactoryWarehouse;

public abstract class OverviewDecorator implements IDecorator{

	FactoryWarehouse currentFactoryWarehouse;
	
	public void ctrlRIGHT()
	{
		nextMenu();
	}
	public void ctrlLEFT()
	{
		prevMenu();
	}
	public void keyUP()
	{
		MenuUp();
	}
	public void keyDOWN()
	{
		MenuDown();
	}
	public void keyRIGHT()
	{
		changeDirectoryInto();
	}
	public void keyLEFT()
	{
		changeDirectoryBack();
	}
	public void keyENTER()
	{
		fireCommand();
	}
	
	public abstract void MenuUp();
	public abstract void MenuDown();
	public abstract void nextMenu();
	public abstract void prevMenu();
	public abstract void changeDirectoryInto();
	public abstract void changeDirectoryBack();
	public abstract void fireCommand();
}
