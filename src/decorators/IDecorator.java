package decorators;

import java.util.ArrayList;

import Model.Displayable.interfaces.Renderable;

public interface IDecorator {

	/**
	 * Interface for handling the interfaces
	 */
	public void ctrlRIGHT();
	public void ctrlLEFT();
	public void keyUP();
	public void keyDOWN();
	public void keyRIGHT();
	public void keyLEFT();
	public ArrayList<String> getSelectionHierarchy();
	public Renderable getCurrentRenderable();
}
