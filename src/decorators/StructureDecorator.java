package decorators;

import java.util.ArrayList;

import Model.CommandBuilder;
import Model.FactoryWarehouse;
import Model.PlayerCursor;
import Model.Commands.Command;
import Model.Displayable.Structure;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Renderable;
import Util.CircularList;
import Util.OilException;

public class StructureDecorator extends GameDecorator
{
	private CircularList<CircularList<Structure>> structures;

	private CircularList<Structure> currentType;
	private Structure currentInstance;
	
	private CircularList<Command> supportedCommands; 
	private CircularList<Command> nullCommands = new CircularList<Command>();
	//private Command currentCommand;
	
	public StructureDecorator() {}
	
	public StructureDecorator(PlayerCursor curs, FactoryWarehouse currentFactoryWarehouse, CommandBuilder commandBuilder)
	{
		this.currentFactoryWarehouse = currentFactoryWarehouse;
		this.commandBuilder = commandBuilder;
		this.curs = curs;
		structures = currentFactoryWarehouse.getStructuresList();
	}
	
	public void nextType()
	{
		CircularList<Structure> current = structures.getNext();
		int size = structures.size();
		for(int i = 0; i < size; i++)
		{
			if( current.isEmpty() )
				current = structures.getNext();
			else
			{
				currentType = current;
				currentInstance = currentType.get(0);
				curs.setCurrentControllable(currentType.getCurrent());
				supportedCommands = currentInstance.getCommands();
				System.out.println(currentInstance.getName() + " " + currentInstance.getID());
				commandBuilder.resetCommandList(currentInstance.getCommands());
				return;
			}
		}	
	}
	public void prevType()
	{
		CircularList<Structure> current = structures.getPrevious();
		int size = structures.size();
		for(int i = 0; i < size; i++)
		{
			if( current.isEmpty() )
				current = structures.getPrevious();
			else
			{
				currentType = current;
				currentInstance = currentType.get(0);
				curs.setCurrentControllable(currentType.getCurrent());
				supportedCommands = currentInstance.getCommands();
				System.out.println(currentInstance.getName() + " " + currentInstance.getID());
				commandBuilder.resetCommandList(currentInstance.getCommands());
				return;
			}
		}
	}
	public void nextOrder()
	{
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandForward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void prevOrder()
	{
		if(supportedCommands != null)
		{
			//currentCommand = supportedCommands.getNext();
			commandBuilder.cycleCommandBackward();
			//System.out.println(currentCommand.getName());
		}
		else
		{
			commandBuilder.resetCommandList(nullCommands);
		}
	}
	public void nextInstance()
	{
		if(currentInstance != null)
		{
			currentInstance = currentType.getNext();
			curs.setCurrentControllable(currentType.getCurrent());
			System.out.println(currentInstance.getName() + " " + currentInstance.getID());
			supportedCommands = currentInstance.getCommands();
			commandBuilder.resetCommandList(supportedCommands);
		}
	}
	public void prevInstance()
	{
		if(currentInstance != null)
		{
			currentInstance = currentType.getPrevious();
			curs.setCurrentControllable(currentType.getCurrent());
			System.out.println(currentInstance.getName() + " " + currentInstance.getID());
			supportedCommands = currentInstance.getCommands();
			commandBuilder.resetCommandList(supportedCommands);
		}
	}	
	
	public ArrayList<String> getSelectionHierarchy()
	{
		ArrayList<String> selHierarchy = new ArrayList<String>();
		selHierarchy.add("Structure");
		if(currentType == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentInstance.getName());
		}
		if(currentInstance == null)
		{
			selHierarchy.add(null);
		}
		else
		{
			selHierarchy.add(currentInstance.getName() + " " + currentInstance.getID());
		}
		selHierarchy.add(commandBuilder.getCommandName());
		
		return selHierarchy;
	}
	
	public Renderable getCurrentRenderable()
	{
		if(currentInstance != null)
			return currentInstance;
		else
			return null;
	}
	
	public String toString()
	{
		return "Structure Decorator";
	}

}
