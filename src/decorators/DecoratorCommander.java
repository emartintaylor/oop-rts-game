package decorators;

import java.util.ArrayList;

import Util.CircularList;
import Util.OilException;

import Model.CommandBuilder;
import Model.ControllableModel;
import Model.FactoryWarehouse;
import Model.PlayerCursor;
import Model.Displayable.interfaces.Renderable;

public class DecoratorCommander implements ControllableModel, SelectablesToView{
	/**
	 * Handles all the changing of current Decorators
	 * Has reference to Player 
	 */
	private FactoryWarehouse currentFactoryWarehouse;
	private CircularList<IDecorator> declist = new CircularList<IDecorator>();
	private int gameOverviewIndex = 0;
	private IDecorator currentDecorator;
	private CommandBuilder commandBuilder;
	private PlayerCursor curs;
	
	private boolean overviewFlag = false;  
	
	public static final int RALLY_POINT_DECORATOR = 0;
	public static final int ARMY_DECORATOR = 3;
	public static final int UNIT_OVERVIEW_DECORATOR = 4;
	public static final int STRUCTURE_OVERVIEW_DECORATOR = 5;
	public static final int TECH_TREE_OVERVIEW_DECORATOR = 6;
	
	/**
	 * default constructor -- do not use
	 */
	public DecoratorCommander() {}
	
	public DecoratorCommander(FactoryWarehouse currentFactoryWarehouse, PlayerCursor curs)
	{
		if(currentFactoryWarehouse == null)
			System.out.println("currentFactoryWarehouse null.");
		this.currentFactoryWarehouse = currentFactoryWarehouse;
		this.commandBuilder = new CommandBuilder(currentFactoryWarehouse.getMyPlayer());
		this.curs = curs;
		initDecorators();
		init();
	}
	
	/**
	 * Initializes the current Decorators of choice
	 * Indexes
	 * 0 RallyPointDecorator
	 * 1 StructureDecorator
	 * 2 UnitDecorator
	 * 3 ArmyDecorator
	 * 4 UnitOverviewDecorator
	 * 5 StructureOverviewDecorator
	 * 6 RallyPointOverviewDecorator
	 */
	public void initDecorators()
	{
		declist.add(new RallyPointDecorator(curs,currentFactoryWarehouse,commandBuilder));
		declist.add(new StructureDecorator(curs,currentFactoryWarehouse,commandBuilder));
		declist.add(new UnitDecorator(curs,currentFactoryWarehouse,commandBuilder));
		declist.add(new ArmyDecorator(curs,currentFactoryWarehouse,commandBuilder));
		declist.add(new UnitOverviewDecorator(currentFactoryWarehouse));
		declist.add(new StructureOverviewDecorator(currentFactoryWarehouse));
		declist.add(new RallyPointOverviewDecorator(currentFactoryWarehouse));
	}
	
	public void init()
	{
		currentDecorator = declist.get(0);
	}
	
	public void nextDecorator()
	{
		if(gameOverviewIndex ==(ARMY_DECORATOR))
		{
			currentDecorator = declist.get(RALLY_POINT_DECORATOR);
			gameOverviewIndex = RALLY_POINT_DECORATOR;
		}
		else
		{
			gameOverviewIndex++;
			currentDecorator = declist.get(gameOverviewIndex);

		}
	}
	
	public void prevDecorator()
	{
		if(gameOverviewIndex == RALLY_POINT_DECORATOR)
		{
			currentDecorator = declist.get(ARMY_DECORATOR);
			gameOverviewIndex = (ARMY_DECORATOR);
		}
		else
		{
			gameOverviewIndex--;
			currentDecorator = declist.get(gameOverviewIndex);
		}
	}
	
	public void cycleToGameOverviewDecorator()
	{
		currentDecorator = declist.get(gameOverviewIndex);
		overviewFlag = false;
	}
	
	public void cycleToUnitOverviewDecorator()
	{
		currentDecorator = declist.get(UNIT_OVERVIEW_DECORATOR);
		overviewFlag = true;
	}
	
	public void cycleToStructureOverviewDecorator()
	{
		currentDecorator = declist.get(STRUCTURE_OVERVIEW_DECORATOR);
		overviewFlag = true;
	}
	
	public void cycleToTechTreeOverviewDecorator()
	{
		currentDecorator = declist.get(TECH_TREE_OVERVIEW_DECORATOR);
		overviewFlag = true;
	}
	
	public void cycleCommandForward() {
		System.out.println("Key Pressed Up");
		System.out.println(currentDecorator.toString());
		if(currentDecorator != null)
		{
			currentDecorator.keyUP();
		}
	}
	
	public void cycleCommandBackward(){ 
		System.out.println("Key Pressed Down");
		System.out.println(currentDecorator.toString());
		if(currentDecorator != null)
		{
			currentDecorator.keyDOWN();
		}
		else
			System.out.println("currentDecorator is null");
	}
	public void cycleInstanceForward() {
		System.out.println("Key Pressed Right");
		System.out.println(currentDecorator.toString());
		if(currentDecorator != null)
		{
			currentDecorator.keyRIGHT();
		}
		else
			System.out.println("currentDecorator is null");
	}
	
	public void cycleInstanceBackward() {
		System.out.println("Key Pressed Left");
		System.out.println(currentDecorator.toString());
		if(currentDecorator != null)
		{
			currentDecorator.keyLEFT();
		}
		else
			System.out.println("currentDecorator is null");
	}
	public void cycleModeForward() {
		if(!overviewFlag)
		{
			System.out.println("Key Pressed Ctrl Up");
			nextDecorator();
			System.out.println(currentDecorator.toString());
		}
	}
	
	public void cycleModeBackward() {
		if(!overviewFlag)
		{
			System.out.println("Key Pressed Ctrl Down");
			prevDecorator();
			System.out.println(currentDecorator.toString());
		}
	}
	
	public void cycleTypeForward() {
		System.out.println("Key Pressed Ctrl Right");
		System.out.println(currentDecorator.toString());
		if(currentDecorator != null)
		{
			currentDecorator.ctrlRIGHT();
		}
		else
			System.out.println("currentDecorator is null");
	}
	
	public void cycleTypeBackward() {
		System.out.println("Key Pressed Ctrl Left");
		System.out.println(currentDecorator.toString());
		if(currentDecorator != null)
		{
			currentDecorator.ctrlLEFT();
		}
		else
			System.out.println("currentDecorator is null");
	}
	
	public ArrayList<String> getSelectionHierarchy()
	{
		return currentDecorator.getSelectionHierarchy();
	}
	
	public Renderable getCurrentRenderable()
	{
		return currentDecorator.getCurrentRenderable();
	}
	
	public ArrayList<Renderable> getRenderableList()
	{
		return null;
	}
	
	public String getHeader()
	{
		return null;
	}
	
	public ArrayList<Renderable> getUnassigned()
	{
		return null;
	}
	
	public int getNumAvailableUniv()
	{
		return -1;
	}
}
