package decorators;

import Model.CommandBuilder;
import Model.ControllableModel;
import Model.FactoryWarehouse;
import Model.PlayerCursor;

public abstract class GameDecorator implements IDecorator
{
	FactoryWarehouse currentFactoryWarehouse;
	CommandBuilder commandBuilder;
	PlayerCursor curs;
	
	public void ctrlRIGHT()
	{
		nextType();
	}
	public void ctrlLEFT()
	{
		prevType();
	}
	public void keyUP()
	{
		nextOrder();
	}
	public void keyDOWN()
	{
		prevOrder();
	}
	public void keyRIGHT()
	{
		nextInstance();
	}
	public void keyLEFT()
	{
		prevInstance();
	}
	
	public abstract void nextType();
	public abstract void prevType();
	public abstract void nextOrder();
	public abstract void prevOrder();
	public abstract void nextInstance();
	public abstract void prevInstance();
}
