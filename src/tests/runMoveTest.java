package tests;

import LocationStuff.Location;
import LocationStuff.NCLocation;
import Model.Map;
import Model.Player;
import Model.TableBasedRecycler;
import Util.IntRecycler;

/**
 * Setup:
 * This test case creates a Player, Map.
 * Player is initialized with FactoryWarehouse, with a List of Rallys.
 * 
 * Map is initialized
 * It ignores controller, thus artificially puts a Move command on 
 * the Rally's queue. Move is artificially given a parameter. 
 * 
 * Test:
 * call run() on the command. 
 * 
 * @author Chris
 *
 */
public class runMoveTest {

	public static void main(String[] args){
		
		String [] mapArray = {
				"999999",
				"900009",
				"900009",
				"900009",
				"999999"
		};
		Map map = new Map(mapArray,1);
		
		//Player p = new Player(new TableBasedRecycler(),map);
		//p.makeRally(new NCLocation(2,2));
		//command in rally.
		//in player:
		//add commands arguments.  (check)
		//add command to rally queue (check)
		//p.fireCommand();
		//p.onTick();
	}
}
