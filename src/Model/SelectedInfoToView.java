package Model;

import LocationStuff.Location;
import Model.Displayable.interfaces.Renderable;

public interface SelectedInfoToView
{
	Renderable getCurrentRenderable();
	Location getCurrentLocation();
}