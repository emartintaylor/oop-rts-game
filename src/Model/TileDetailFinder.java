package Model;

import java.util.LinkedList;

public class TileDetailFinder {
	private static LinkedList<TileDetails> tilesWithDetails;
	public TileDetailFinder(LinkedList<TileDetails> _tilesWithDetails)
	{
		tilesWithDetails = _tilesWithDetails;
	}
	private static boolean found = false;
	public static boolean found(){return found;}
	public TileDetails find(int x, int y)
	{
		for(TileDetails tileDetails: tilesWithDetails)
		{
			if(tileDetails.match(x, y))
			{
				found=true;
				return tileDetails;
			}
			else found = false;
		}
		return null;
	}

}
