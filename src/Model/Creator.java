package Model;

import LocationStuff.Location;

public interface Creator {

	public void createLightVehicle(Location l);
	public void createRally(Location l);
	public void createMelee(Location l);
	public void createRange(Location l);
	public void createExplorer(Location l);
	public void createFort(Location l);
	public void createHeavyVehicle(Location l);
	public void createBase(Location l);
	
}
