package Model;

public abstract class Obstacle implements ViewObstacle{
	private int x;
	private int y;
	private String render;
	private int level;
	public Obstacle(int _x, int _y, String _render, int _level)
	{
		x = _x;
		y = _y;
		render = _render;
		level = _level;
	}
	public int getX(){return x;}
	public int getY(){return y;}
	public String getRenderObstacle(){return render;}
	public int getLevel(){return level;}
	public String toString()
	{
		return "-"+render+"-" + " at ("+x+","+y+")"+" level:"+level;
	}
}
