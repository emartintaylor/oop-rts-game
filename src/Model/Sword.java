package Model;

import Model.Displayable.interfaces.ItemApplicant;

public class Sword extends Item{
	public Sword(int _x, int _y)
	{
		super(_x,_y,"SWORD");
	}
	public void applyItem(ItemApplicant itemApplicant)
	{
		itemApplicant.incrementOffensive();
	}
}
