package Model;

import Model.Displayable.interfaces.ItemApplicant;

public class Helmet extends Item{
	public Helmet(int _x, int _y)
	{
		super(_x,_y,"HELMET");
	}
	public void applyItem(ItemApplicant itemApplicant)
	{
		itemApplicant.incrementMaxHealth();
	}
}
