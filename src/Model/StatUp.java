package Model;

public enum StatUp {
	OFFENSIVE(10), DEFENSIVE(10), ARMOR(10), HEALTH(20);
	private int statIncrease;
	private StatUp(int statIncrease)
	{
		this.statIncrease = statIncrease;
	}
	public int getStatIncrease(){ return statIncrease;}
}
