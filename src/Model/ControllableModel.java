package Model;

//This is the guy that Controller communicates to and is implemented by DecoratorCommander

public interface ControllableModel {
	void cycleToGameOverviewDecorator();
	void cycleToUnitOverviewDecorator();
	void cycleToStructureOverviewDecorator();
	void cycleToTechTreeOverviewDecorator();
	void cycleModeForward();
	void cycleModeBackward();
	void cycleTypeForward();
	void cycleTypeBackward();
	void cycleInstanceForward();
	void cycleInstanceBackward();
	void cycleCommandForward();
	void cycleCommandBackward();
}
