package Model;

public interface ViewItem {
	public String getRenderItem();
	public int getX();
	public int getY();
}
