package Model;

import LocationStuff.Locateable;
import LocationStuff.Location;
import LocationStuff.NCLocation;


public class Tile implements Locateable{
	private Terrain terrain;
	private int level;
	private Visibility [] visibility;
	private int ownership;
	private int x;
	private int y;
	private boolean changed;
	public Tile( int _x, int _y, Terrain _terrain, int numOfPlayers)
	{
		terrain = _terrain;
		visibility = new Visibility[numOfPlayers];
		for(int i = 0; i < visibility.length; i++)
			visibility[i] = Visibility.NOTVISIBLE;
		ownership = -1;
		level = terrain.getLevel();
		x = _x;
		y = _y;
		changed = true;
	}
	public void setChanged()
	{
		changed = true;
	}
	public void clrChanged()
	{
		changed = false;
	}
	public boolean getChanged()
	{
		return changed;
	}
	public int getX(){return x;}
	public int getY(){return y;}
	public int getLevel(){return terrain.getLevel();}
	public String getTerrainType(){return terrain.getTerrainType();}
	public Visibility getVisibility(int playerID){return visibility[playerID];}
	public void setVisibility(int playerID)
	{
		setChanged();
		visibility[playerID] = Visibility.VISIBLE;
	}
	public void shroud(int playerID)
	{
		if(visibility[playerID]==Visibility.VISIBLE)
			visibility[playerID]=Visibility.SHROUDED;
	}
	public void setObstacle(Obstacle obstacle)
	{
		level += obstacle.getLevel();
	}
	public int getPassable(int playerID, int mobility)
	{
		int value = 0;
		if(mobility >= level && (ownership == -1 || ownership == playerID))
				value = 1;
		return value;
	}
	public void setOwnership(int playerID){ownership = playerID;}
	public boolean isSelected() {
		return false;
	}
	@Override
	public Location getLocation() {
		return new NCLocation(x,y);
	}
}
