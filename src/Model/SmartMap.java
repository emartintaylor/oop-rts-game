package Model;

import LocationStuff.Location;
import Model.Displayable.Moveable;

public interface SmartMap {
	public Location getNextInPath(Moveable toMove, Location destination);
	
	/**
	 * call notify after it has received a new location. 
	 * 
	 * @param imoved
	 */
	public void notifyTileDetailsManager(Moveable imoved);
}
