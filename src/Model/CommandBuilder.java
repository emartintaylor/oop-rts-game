package Model;
import Util.CircularList;
import LocationStuff.Location;
import Model.Commands.Command;

/**
 * The CommandBuilder facilitates user cycling of commands.  It tells commands when they should register with the
 * GameEventDispatcher so they can use notifiers to specify their own requirements for users to fire them.  When
 * commands are selected by the user prepare() is called on the command, allowing it to register for GameEvents.
 * Consequently when another command is selected the CommandBuilder should call release() on the previously
 * selected command.
 * 
 * In addition to readying commands on selection, the CommandBuilder also allows the Command to retrieve any
 * additional information it needs via functions provided by the CommandBuilder, which the unit can access
 * when the CommandBuilder is passed to it in the prepare() function.
 * @author JR
 * @version 1.0
 *
 */
public class CommandBuilder {
	Player player;
	CircularList<Command> cList = null;
	Command currentCommand;
	
	/**
	 * Instantiates a CommandBuilder, and gives it access to the player so that it can retrieve information
	 * on behalf of the commands.
	 * @param player -- The player associated with human input.
	 */
	public CommandBuilder(Player player)
	{
		this.player = player;
		currentCommand = null;
		cList = new CircularList<Command>();
		System.out.println("CommandBuilder made.");
	}
	
	/**
	 * Retrieves the SmartMap, an interface to the games map used for movement.
	 * @return -- a SmartMap for this games model.
	 */
	public SmartMap getSmartMap()
	{
		return player.getSmartMap();
	}
	
	/**
	 * Retrieves the currently selected tile location from the player.
	 * @return The currently selected tile's location.
	 */
	public Location getCurrentlySelectedLocation(){
		return player.getCurrentlySelectedLocation();
	}

	/**
	 * Returns the Creator, the building instantiator.
	 * @return the Creator for the human controlled player.
	 */
	public Creator getCreator()
	{
		return player.getCreator();
	}
	
	/**
	 * Sets the CommandBuilder's list of selectable commands to a new list.  After this is called, the previously selected
	 * command will be release()'ed, and a command from the new list of commands will be selected and prepared.
	 * @param cList -- the new list of commands.
	 */
	public void resetCommandList(CircularList<Command> comList)
	{
			cList = comList;
			releaseCommand();
			if(!cList.isEmpty())
				currentCommand = cList.getCurrent().getClone();
			else
				currentCommand = null;
			prepareCommand();
	}
	
	/**
	 * Selects the next command in the command list as the currently selected command.  The previously
	 * selected command will be released, if the new command is not null it will be set and prepared.
	 */
	public void cycleCommandForward()
	{
		releaseCommand();
		if( !cList.isEmpty())
			currentCommand = cList.getNext().getClone();
		prepareCommand();
	}
	
	/**
	 * Selects the previous command in the command list as the currently selected command.  The previously
	 * selected command will be released, if the new command is not null it will be set and prepared.
	 */
	public void cycleCommandBackward()
	{
		releaseCommand();
		if( !cList.isEmpty())
			currentCommand = cList.getPrevious().getClone();
		prepareCommand();
	}
	
	/**
	 * Returns the String representation of the currently selected command.  If the currently selected command
	 * is null it will return the phrase "No Commands".
	 * @return The String representation of the current command.
	 */
	public String getCommandName()
	{
		String reply;
		if( currentCommand != null )
			reply = currentCommand.getName();
		else
			reply = "No Commands";
		return reply;
	}
	
	/**
	 * Prepares the currently selected command for execution by calling the Command's prepare() method.  If the
	 * current Command is null this function will do nothing.
	 */
	void prepareCommand()
	{
		if( currentCommand != null )
			currentCommand.prepare(this);
	}
	
	/**
	 * Releases the currently selected command from executable state by calling the Command's release() method.
	 * If the current Command is null this function will do nothing.
	 */
	void releaseCommand()
	{
		if( currentCommand != null )
			currentCommand.release();
	}
}
