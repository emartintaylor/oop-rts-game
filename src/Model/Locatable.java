package Model;

public interface Locatable {
	public int getX();
	public int getY();
}
