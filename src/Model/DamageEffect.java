package Model;

import Model.Displayable.interfaces.AOEApplicant;

public class DamageEffect extends AreaEffect{

	public DamageEffect(int x, int y) {
		super(x, y, "DAMAGEEFFECT");
	}
	public void applyEffect(AOEApplicant aoeApplicant) {
		aoeApplicant.damageEffect();
	}	
}
