package Model;

import Model.Displayable.interfaces.ItemApplicant;

public abstract class Item implements ViewItem{
	private int x;
	private int y;
	private String render;
	public Item(int _x, int _y, String _render)
	{
		x = _x;
		y = _y;
		render = _render;
	}
	public int getX(){return x;}
	public int getY(){return y;}
	public String getRenderItem(){return render;}
	public abstract void applyItem(ItemApplicant itemApplicant);
	public String toString()
	{
		return "-"+render+"-" + " at ("+x+","+y+")";
	}
}