package Model;

import java.util.ArrayList;
import java.util.List;

import Model.Displayable.interfaces.Displayable;

public interface ControllablesToView {
	ArrayList<Displayable> getDisplayables();
}
