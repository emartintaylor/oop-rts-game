package Model;

public enum TerrainEnum {
	ROCKY(1), GRASS(0), DESERT(0), BORDER(9);
	private int level;
	private TerrainEnum(int level) {
		this.level = level;
	}
	public int getLevel()
	{
		return this.level;
	}
}
