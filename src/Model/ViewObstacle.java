package Model;

public interface ViewObstacle {
	public String getRenderObstacle();
	public int getX();
	public int getY();
}
