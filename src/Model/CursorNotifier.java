package Model;

import controller.EventNotifier;
import controller.GameEventType;

public class CursorNotifier implements EventNotifier
{
	private PlayerCursor pc;
	
	public CursorNotifier(PlayerCursor pc)
	{
		this.pc = pc;
	}
	@Override
	public void notify(GameEventType g)
	{
		switch(g)
		{
		case CENTERVIEW:
			pc.centerOnControllable();
			break;
		case DIR_N:
			pc.moveCursor(Direction.N);
			break;
		case DIR_NE:
			pc.moveCursor(Direction.NE);
			break;
		case DIR_SE:
			pc.moveCursor(Direction.SE);
			break;
		case DIR_S:
			pc.moveCursor(Direction.S);
			break;
		case DIR_SW:
			pc.moveCursor(Direction.SW);
			break;
		case DIR_NW:
			pc.moveCursor(Direction.NW);
			break;
		}
		
	}

}
