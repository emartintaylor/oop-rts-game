package Model;

import Model.Displayable.interfaces.AOEApplicant;

public class InstantDeathEffect extends AreaEffect{

	public InstantDeathEffect(int x, int y) {
		super(x, y, "INSTANTDEATHEFFECT");
	}
	public void applyEffect(AOEApplicant aoeApplicant) {
		aoeApplicant.instantDeathEffect();
	}	
}
