package Model;

public class Resource implements DisplayableResource{
	private int x;
	private int y;
	private int food;
	private int energy;
	private int ore;
	private boolean assigned;
	private int base;
	public Resource(int _food, int _energy, int _ore, int _x, int _y)
	{
		x = _x;
		y = _y;
		food = _food;
		energy = _energy;
		ore = _ore;
		assigned = false;
		base = 10;
	}
	static int index = 0;
	public int getX(){return x;}
	public int getY(){return y;}
	public int getFood(){return food;}
	public int getEnergy(){return energy;}
	public int getOre(){return ore;}
	public boolean getAssigned(){return assigned;}
	public int harvestFood(int factor)
	{		
		int value = 0;
		int temp = factor*base;
		if(!assigned)
		{
			if( food <= 0)
				value = 0;
			else if( food > 0 && food <= temp )
			{
				value = food;
				food = 0;
			}
			else
			{
				value = temp;
				food -= temp;
			}
			assigned = true;
		}
		return value;
	}
	public int harvestEnergy(int factor)
	{		
		int value = 0;
		int temp = factor*base;
		if(!assigned)
		{
			if( energy <= 0)
				value = 0;
			else if( energy > 0 && energy <= temp )
			{
				value = energy;
				energy = 0;
			}
			else
			{
				value = temp;
				energy -= temp;
			}
			assigned = true;
		}
		return value;
	}
	public int harvestOre(int factor)
	{
		int value = 0;
		int temp = factor*base;
		if(!assigned)
		{
			if( ore <= 0)
				value = 0;
			else if( ore > 0 && ore <= temp )
			{
				value = ore;
				ore = 0;
			}
			else
			{
				value = temp;
				ore -= temp;
			}
			assigned = true;
		}
		return value;
	}
	public void renewFood(int factor)
	{
		if(!assigned)
		{
			food += factor*base;
			
		}
	}
	public void unassign()
	{
		assigned = false;
	}
}
