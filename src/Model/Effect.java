package Model;

public enum Effect {
	INSTANTDEATH, FULLRECOVERY, HEAL, DAMAGE;
}
