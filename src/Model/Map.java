package Model;

import java.util.*;

import LocationStuff.ChLocation;
import LocationStuff.Location;
import Model.Displayable.Controllable;
import Model.Displayable.Moveable;

public class Map implements MapToView, SmartMap, RiverMap{
	private Tile [][] tile;
	private int width;
	private int height;
	private LinkedList<ViewItem> items;
	private LinkedList<ViewObstacle> obstacles;
	private LinkedList<ViewAOE> areaEffects;
	private LinkedList<DisplayableResource> resources;
	private TileDetailsManager tileDetailsManager;
	private RiverManager riverManager;
	
	public Map(String[] mapArray, int players)
	{
		width = mapArray[0].length();
		height = mapArray.length;
		tile = new Tile[width][height];
		items = new LinkedList<ViewItem>();
		obstacles = new LinkedList<ViewObstacle>();
		areaEffects = new LinkedList<ViewAOE>();
		resources = new LinkedList<DisplayableResource>();
		tileDetailsManager = new TileDetailsManager(this);
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j<height; j++)
			{
				tile[i][j] = new Tile(i, j, TerrainFactory.match(mapArray[height-j-1].charAt(i)),players);	
			}
		}
		riverManager = new RiverManager(this);
		riverManager.init(tile[2][3]);
	}
	//debug purposes
	public void printTerrainLevel()
	{
		System.out.println();
		for(int j = 0; j < height; j++)
		{
			for(int i = 0; i < width; i++)
			{
				System.out.print(tile[i][height-j-1].getLevel()+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	//debug purposes
	public void printPassability(int playerID, int mobility)
	{
		System.out.println();
		for(int j = 0; j < height; j++)
		{
			for(int i = 0; i < width; i++)
			{
				System.out.print(tile[i][height-j-1].getPassable(playerID, mobility)+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	//debug purposes
	public void printMapDetails()
	{
		System.out.println();
		System.out.println(items.toString());
		System.out.println(obstacles.toString());
		System.out.println(areaEffects.toString());
	}
	public void putRiverToMap(RiverTile rt, int x, int y)
	{
		tile[x][y]= rt;
	}
	public void onTick()
	{
		shroud();
	}
	public void commitChangesToMap(Controllable c)
	{
		setVisibilityByRadius(c.getVisionRadius(),c.getX(),c.getY());
		setOwnership(c.getPlayerID(), c.getX(), c.getY());		
	}
	private void setOwnership(int playerID, int x, int y)
	{
		tile[x][y].setOwnership(playerID);
	}
	private void shroud()
	{
		for(int j = 0; j < height; j++)
		{
			for(int i = 0; i < width; i++)
			{
				tile[i][height-j-1].shroud(0);
			}
		}
	}
	private void setVisibilityByRadius(int radius, int x, int y)
	{
		radius--;

		if(outOfBounds(x,y))
			return;
		tile[x][y].setVisibility(0);
		if(radius==0)
		{
			return;
		}
		else
		{
			if(isEven(x))
				setVisibilityByRadius(radius,x+1,y);
			else
				setVisibilityByRadius(radius,x+1,y+1);
			setVisibilityByRadius(radius, x,y+1);
			if(isEven(x))
				setVisibilityByRadius(radius, x-1,y);
			else
				setVisibilityByRadius(radius, x-1,y+1);
			if(isEven(x))				
				setVisibilityByRadius(radius, x-1,y-1);
			else
				setVisibilityByRadius(radius, x-1,y);
			setVisibilityByRadius(radius,x,y-1);
			if(isEven(x))
				setVisibilityByRadius(radius,x+1,y-1);
			else
				setVisibilityByRadius(radius,x+1,y);
		}
	}
	public Location getNextLocation(Direction d,Location current)
	{
		int x = current.getX();
		int y = current.getY();
		ChLocation loc = new ChLocation(current.getX(),current.getY());
		
		switch(d)
		{
		case NE:
			if(isEven(x))
				loc.setLocation(x+1,y);
			else
				loc.setLocation(x+1,y+1);
			break;
		case N: loc.setLocation(x,y+1);break;
		case NW:
			if(isEven(x))
				loc.setLocation(x-1, y);
			else
				loc.setLocation(x-1,y+1);
			break;
		case SW:
			if(isEven(x))
				loc.setLocation(x-1,y-1);
			else
				loc.setLocation(x-1,y);
			break;
		case S: loc.setLocation(x,y-1);break;
		case SE:
			if(isEven(x))
				loc.setLocation(x+1,y-1);
			else
				loc.setLocation(x+1,y);
			break;
		default: break;
		}
		
		if(outOfBounds(loc.getX(), loc.getY()))
			return current;
		return loc;
	}
	
	
	private boolean isEven(int n)
	{
		return n%2==0;
	}
	private boolean outOfBounds(int x, int y)
	{
		return (x<0||y<0 ||
				x==width||y==height);
	}
	public int addItem(Item item)
	{
		int x = item.getX();
		int y = item.getY();
		
		items.add(item);
		System.out.println("Added to map");
		tileDetailsManager.addItem(item,x,y);
		return items.size();
	}
	public int addObstacle(Obstacle obstacle)
	{
		int x = obstacle.getX();
		int y = obstacle.getY();
		obstacles.add(obstacle); 
		System.out.println("obstacle level: "+obstacle.getLevel()+" tile level: "+tile[x][y].getLevel());
		tile[x][y].setObstacle(obstacle);
		return obstacles.size();
	}
	public int addAreaEffect(AreaEffect areaEffect)
	{
		int x = areaEffect.getX();
		int y = areaEffect.getY();
		areaEffects.add(areaEffect);
		tileDetailsManager.addAreaEffect(areaEffect, x, y);
		return areaEffects.size();
	}
	public int addResource(Resource resource)
	{
		resources.add(resource); 
		return resources.size();
	}
	public int getWidth(){return width;}
	public int getHeight(){return height;}
	public String[][] getTerrain()
	{
		String[][] terrain = new String[height][width];
		for(int j = 0; j < height; j++)
		{
			for(int i = 0; i < width; i++)
			{
				terrain[j][i]=tile[i][height-j-1].getTerrainType();
			}
		}
		return terrain;
	}
	public Visibility[][] getVisibility(int playerID)
	{
		Visibility[][] visibility = new Visibility[height][width];
		for(int j = 0; j < height; j++)
		{
			for(int i = 0; i < width; i++)
			{
				visibility[j][i]=tile[i][height-j-1].getVisibility(playerID);
			}
		}
		return visibility;
	}

	public LinkedList<ViewItem> getItems(){return items;}
	public LinkedList<ViewObstacle> getObstacles(){return obstacles;}
	public LinkedList<ViewAOE> getAreaEffects(){return areaEffects;}
	public LinkedList<DisplayableResource> getResources(){return resources;}
	public Tile getTile(int x, int y){return tile[x][y];}
	public void notifyTileDetailsManager(Moveable imoved)
	{
		tileDetailsManager.useItem(imoved);
		tileDetailsManager.useAreaEffect(imoved);
	}
	public void removeItemFromMap(Item item)
	{
		items.remove(item);
	}
	public Location getNextInPath(Moveable toMove, Location destination)
	{
		System.out.println("Starting from location ("+toMove.getX()+","+toMove.getY()+") to location ("+destination.getX()+","+destination.getY()+")");
		if(tile[toMove.getX()][toMove.getY()].getTerrainType()=="RIVER")
			riverManager.offer(toMove);
		int x = toMove.getX();
		int y = toMove.getY();
		if(x==destination.getX() && y== destination.getY())
		{
			System.out.println("Destination reached!");
		}
		else
		{
			LinkedList<Direction> dir = new LinkedList<Direction>();
			dir = calculatePath(x, y, destination.getX(), destination.getY(), toMove.getPlayerID(), toMove.getMobility());
			System.out.println(dir.peek());
			if(symm(x)==0)
			{
				switch(dir.pop())
				{
				case NE: x +=  1; y +=  0; break;
				case N : x +=  0; y +=  1; break;
				case NW: x += -1; y +=  0; break;
				case SW: x += -1; y += -1; break;
				case S : x +=  0; y += -1; break;
				case SE: x +=  1; y += -1; break;
				}
			}
			else
			{
				System.out.println("odd");
				switch(dir.pop())
				{
				case NE: x +=  1; y +=  1; break;
				case N : x +=  0; y +=  1; break;
				case NW: x += -1; y +=  1; break;
				case SW: x += -1; y +=  0; break;
				case S : x +=  0; y +=  -1; break;
				case SE: x +=  1; y +=  0; break;
				}			
			}
		}
		return new ChLocation(x,y);
	}
	public LinkedList<Direction> calculatePath(int xFrom, int yFrom, int xTo, int yTo, int playerID, int mobility)
	{
		LinkedList<Direction> directionQueue = new LinkedList<Direction>();
		int w = width;
		int numOfElem = width*height;
		s = new int[numOfElem];
		g = new int[numOfElem];
		for(int i = 0; i < numOfElem; i++)
		{
			s[i] = -1;
			g[i] = -1;
			int passability = tile[i%w][i/w].getPassable(playerID, mobility);
			if(passability == 0) s[i] = -2;
			if(i%w == xFrom && i/w == yFrom) { ss = i; }
			if(i%w == xTo && i/w == yTo) { gg = i; }
		}
		a = gg;
		b = gg;
		int [] sorted = new int[7];
		while(a != ss)
		{	
			sorted = manhattanSort();
			sortedAdd(sorted);
			a=s[a];			
		}
		while(a != gg)
		{
			directionQueue.offer(sv(g[a]));
			a+=g[a];
		}		
		return directionQueue;
	}
	private int ss, gg, a, b;
	private int [] s, g;
	private int add(int p, int o)
	{
		int temp = s[p+o];
		if(temp == -1)
		{
			s[b] = p+o;
			b = p+o;
			g[b] = -o;
		}
		return temp;
	}
	private Direction sv(int o)
	{
		Direction temp = null;
		if(o==1)
		{
			if(symm(a)==1) //downright
				temp = Direction.SE;
			else temp = Direction.NE; //upright
		}
		if (o==width)//up
			temp = Direction.N;
		if (o == width-1) //topleft
			temp = Direction.NW;
		if (o == width+1) //upright
			temp = Direction.NE;
		if(o==-1)
		{
			if(symm(a)==1) //downleft
				temp = Direction.SW;
			else temp = Direction.NW;	//topleft			
		}
		if(o == -width)//down
			temp = Direction.S;
			
		if(o == -width-1) //downleft
			temp = Direction.SW;
		if(o == -width+1) //downright
			temp = Direction.SE;
		return temp;
	}
	//actually distance formula not Manhattan
	private double manhattanDistance(int a, int ss)
	{
		double temp = 0;
		int w = width;
		temp = Math.sqrt(Math.pow((a%w - ss%w),2)+ Math.pow((a/w - ss/w),2));
		return temp;
	}
	//actually distance formula not Manhattan
	private int [] manhattanSort()
	{
		double[] temp = new double[6];
		int [] index = new int[7];
		int w = width;
		if(symm(a%w)==0)
		{
			//System.out.println("even"); 
			temp[0] = manhattanDistance(a+1-w,ss);//se
			temp[1] = manhattanDistance(a-1-w,ss);//sw
			temp[2] = manhattanDistance(a+0-w,ss);//s
			temp[3] = manhattanDistance(a-1-0,ss);//w
			temp[4] = manhattanDistance(a+1-0,ss);//e
			temp[5] = manhattanDistance(a+0+w,ss);//n
			index[6] = 0;
		}
		else
		{
			temp[0] = manhattanDistance(a+1+w,ss);//ne
			temp[1] = manhattanDistance(a+0+w,ss);//n
			temp[2] = manhattanDistance(a-1+w,ss);//nw
			temp[3] = manhattanDistance(a-1-0,ss);//w
			temp[4] = manhattanDistance(a+0-w,ss);//s
			temp[5] = manhattanDistance(a+1-0,ss);//e
			index[6] = 1;
		}
		double[] copy = new double[6];
		copy = temp.clone();
		for(int i = 0; i < temp.length-1; i++)
		{
			for(int j = i+1; j < temp.length; j++)
			{
				double mid;
				if(temp[i]>temp[j])
				{
					mid = temp[j];
					temp [j] = temp [i];
					temp [i] = mid;
				}
			}
		}
		for(int i = 0; i < index.length-1; i++)
		{
			for(int j = 0; j < index.length-1; j++)
			{
				if(temp[i]==copy[j])
					index[i] = j+1;
			}
		}
		return index;
	}
	private void sortedAdd(int [] index)
	{
		int w = width;
		if(index[6]==0)
		{
			for(int i = 0; i < index.length-1; i++)
			{
				switch(index[i])
				{
				case 1:	add(a,1-w); break;
				case 2:	add(a,-1-w); break;
				case 3:	add(a,-w); break;	
				case 4:	add(a,-1); break;
				case 5:	add(a,1); break;
				case 6:	add(a,w); break;
				default: break;
				}
			}			
		}
		else
		{
			for(int i = 0; i < index.length-1; i++)
			{
				switch(index[i])
				{
				case 1:	add(a,1+w); break;
				case 2:	add(a,w); break;
				case 3:	add(a,-1+w); break;	
				case 4:	add(a,-1); break;
				case 5:	add(a,-w); break;
				case 6:	add(a,1); break;
				default: break;
				}
			}
		}
	}
	private int symm(int a)
	{
		int symm = a%width;
		symm %= 2;
		return symm;
	}
	public void clrChanged(int x, int y) {
		tile[x][y].clrChanged();
	}
	public boolean getChanged(int x, int y) {
		return tile[x][y].getChanged();
	}
}
