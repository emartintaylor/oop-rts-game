package Model;

import java.util.LinkedList;

import Model.Displayable.Moveable;
import Model.Displayable.interfaces.TickListener;

public class RiverManager implements TickListener{
	private RiverMap m;
	private LinkedList<RiverTile> rivers;
	private int currentSpeed;
	public RiverManager(RiverMap m)
	{
		this.m = m;
		rivers = new LinkedList<RiverTile>();
		currentSpeed = 1;
	}
	public void init(Tile tile)
	{
		RiverTile current=null;
		current = addRiver(3,3,0,tile);
		current = addRiver(3,2,0,current);
		current = addRiver(3,1,0,current);
	}
	private RiverTile addRiver(int x, int y, int numOfPlayers, Tile nextTile)
	{
		RiverTile rt = new RiverTile(x,y,numOfPlayers,nextTile);
		rivers.add(rt);
		m.putRiverToMap(rt, x, y);
		return rt;
	}
	public void offer(Moveable moveable)
	{
		for(RiverTile rt: rivers)
		{
			if(moveable.getX()==rt.getX() && moveable.getY()==rt.getY())
				rt.flow(moveable, timer());
		}
	}
	private int timer()
	{
		return currentSpeed;
	}
	@Override
	public void onTick() {
		// TODO Auto-generated method stub
		currentSpeed = 0;
	}
}
