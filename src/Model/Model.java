package Model;

import controller.GameEventDispatcher;
import controller.GameEventType;
import LocationStuff.ChLocation;
import LocationStuff.Location;
import LocationStuff.NCLocation;
import Model.Commands.SmartNotifier;
import Model.Displayable.Movable;
import Model.Displayable.interfaces.TickListener;
import decorators.DecoratorCommander;
import decorators.SelectablesToView;

public class Model implements TickListener
{
	Map map;
	DecoratorCommander stv;
	Player p;
	PlayerCursor pc;
	
	public Model(GameEventDispatcher g)
	{
		//mixed-role below because Model high-level class is dealing with map details.  
		
		SmartNotifier.setDispatcher(g);
		String [] mapArray = {
				"9999999999",
				"9900000099",
				"9900000099",
				"9900000099",
				"9999999999"
		};
		map = new Map(mapArray,1);
		//testing
		//map.getTile(1, 1).setVisibility(0);
		map.getTile(1, 2).setVisibility(0);
		map.getTile(1, 3).setVisibility(0);
		map.getTile(1, 2).shroud(0);
		
		//end testing
		
		
		
		Sword s = new Sword(2,1);
		Bangle b = new Bangle(3,1);
		Shield sh = new Shield(4,1);
		Helmet hel = new Helmet(2,2);
		HealEffect he = new HealEffect(2,1);
		DamageEffect de = new DamageEffect(3,1);
		FullRecoverEffect fre = new FullRecoverEffect(4,1);
		InstantDeathEffect ide = new InstantDeathEffect(2,2);
		
		map.addResource(new Resource(10,10,10,1,1));
		
		
		///Explorer m = new Explorer(2,1,0, new IntList(10));
		Movable m = new Movable(2,1,0);
		m.changeHealth(-15);
		
		pc = new PlayerCursor(map);
		CursorNotifier cn = new CursorNotifier(pc);
		registerCursor(g, cn);
		
		p = new Player(map, 0, pc);
		map.addAreaEffect(he);
		map.addAreaEffect(de);
		map.addAreaEffect(fre);
		map.addAreaEffect(ide);
		map.addItem(s);
		map.addItem(b);
		map.addItem(sh);
		map.addItem(hel);
		map.printMapDetails();
		map.notifyTileDetailsManager(m);
		
		ChLocation l = new ChLocation(1,1);
		//this is the only place where controllables are made to initialize game. 
		p.makeColonist(l);
		l.setLocation(1, 3);
		p.makeExplorer(l);
		l.setLocation(2,1);
		p.makeRange(l);
		l.setLocation(4,3);
		p.makeExplorer(l);
		
		stv = p.getDecoratorCommander();
	}
	
	private void registerCursor( GameEventDispatcher g, CursorNotifier cn)
	{
		g.registerDefaultListener(GameEventType.DIR_N, cn);
		g.registerDefaultListener(GameEventType.DIR_NE, cn);
		g.registerDefaultListener(GameEventType.DIR_SE, cn);
		g.registerDefaultListener(GameEventType.DIR_S, cn);
		g.registerDefaultListener(GameEventType.DIR_SW, cn);
		g.registerDefaultListener(GameEventType.DIR_NW, cn);
		g.registerDefaultListener(GameEventType.CENTERVIEW, cn);
	}
	
	public MapToView getMapToView()
	{
		return map;
	}
	
	public SelectablesToView getSelectablesToView()
	{
		return stv;
	}
	
	public ControllableModel getControllableModel()
	{
		return stv;
	}
	
	public DisplayablePlayer getControllablesToView()
	{
		return p;
	}
	
	public PlayerCursor getCursor()
	{
		return this.pc;
	}

	@Override
	public void onTick() {
		map.onTick();
		p.onTick();
		
	}
	
}
