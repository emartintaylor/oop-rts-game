package Model;

public class Decal {
	public int kind;
	public Decal(int _kind)
	{
		kind = _kind;
	}
	public int getKind(){ return kind;}
}

/* Kind id list
 * 0 = red cross
 * 1 = crossbones
 */
