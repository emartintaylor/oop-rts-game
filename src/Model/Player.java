package Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import LocationStuff.ChLocation;
import LocationStuff.Location;
import Model.Commands.Command;
import Model.Commands.MoveToCursor;
import Model.Displayable.Controllable;
import Model.Displayable.Rally;
import Model.Displayable.Structure;
import Model.Displayable.Unit;
import Model.Displayable.interfaces.Displayable;
import Model.Displayable.interfaces.TickListener;
import Util.IntRecycler;
import decorators.*;

/**
 * 
 * @author Chris
 *
 */
public class Player implements TickListener, DisplayablePlayer, HarvestingPlayer {
	FactoryWarehouse factoryWarehouse;
	Map gameMap;
	int playerID;
	int foodCount;
	int energyCount;
	int oreCount;
	
	private PlayerResourceManager prm;
	DecoratorCommander dc;
	PlayerCursor cursor;

	//Was an intRecycler here that dynamically assigned playerID, but it's gone. 
	public Player(Map m, int playerID, PlayerCursor curs){
		foodCount = 999999;
		energyCount = 999999;
		oreCount = 999999;
		this.playerID = playerID;
		System.out.println("PlayerID: "+playerID);
		factoryWarehouse = new FactoryWarehouse(this);
		gameMap = m;
		cursor = curs;
		//HARD CODE! for testing only! 

		dc = new DecoratorCommander(factoryWarehouse, curs);
		prm = new PlayerResourceManager(this);
		//LOD violation okay b/c testing.
	}
	int getID(){
		return playerID;
	}
	//These Resource-related methods create mixed-role anti-cohesion. We had a Resources class
	//that handled this in a nice OO way...but it's gone and I don't know why. I'll go with this because
	//it's Monday and it works.  
	//Update: Eric and I's design for Resources was consistet. However, I was gone yesterday
	//and Eric hasn't been around and Yan has had to try and understand our design for Resources
	//he didn't so he rewrote something that he understood and would work.  
	public int getFoodCount()
	{
		return this.foodCount;
	}
	public int getEnergyCount()
	{
		return this.energyCount;
	}
	public int getOreCount()
	{
		return this.oreCount;
	}
	public void decrementFood(int amount)
	{
		foodCount-=amount;
	}
	
	//this method does stuff it should because decorators arent implemented yet.
	public void makeRally(Location l){
		factoryWarehouse.createRally(l);
		//HARD CODE! for testing only! 
		//currentlySelectedInstance = factoryWarehouse.getTestCase(); //makes instance the rally.
		//this violates LoD, but okay because only for testing. 
		//currentlySelectedCommand = currentlySelectedInstance.getCommands().getCurrent(); //MoveToCursor
		//prepare currently selected command. 
		
	}
	public void makeColonist(Location l)
	{
		factoryWarehouse.createColonist(l);
	}
	public void makeMelee(Location l)
	{
		factoryWarehouse.createMelee(l);
	}
	public void makeRange(Location l)
	{
		factoryWarehouse.createRange(l);
	}
	public void makeExplorer(Location l)
	{
		factoryWarehouse.createExplorer(l);
	}
	public void makeLightVehicle(Location l)
	{
		factoryWarehouse.createLightVehicle(l);
	}
	public void makeHeavyVehicle(Location l)
	{
		factoryWarehouse.createHeavyVehicle(l);
	}
	public void makeFort(Location l)
	{
		factoryWarehouse.createFort(l);
	}
	
	public void onTick() {
		factoryWarehouse.onTick();
		prm.onTick();
		Collection<Controllable> visioners = factoryWarehouse.getAllControllables();
		for(Controllable c: visioners)
		{
			System.out.println(c.getName());
			gameMap.commitChangesToMap(c);
		}
	}
	
	SmartMap getSmartMap()
	{
		return gameMap;
	}
	
	public Creator getCreator()
	{
		if(factoryWarehouse == null)
			System.out.println("factoryWarehouse null");
		return factoryWarehouse;
	}
	
	Collection<Controllable> getControllables()
	{
		//System.out.println( "While getting: " + factoryWarehouse.getControllables().size() );
		return factoryWarehouse.getControllables();
	}
	
	Location getCurrentlySelectedLocation(){
		return cursor.getCurrentLocation();
	}
	
	public FactoryWarehouse getFactoryWarehouse()
	{
		return factoryWarehouse;
	}
	
	public DecoratorCommander getDecoratorCommander()
	{
		return dc;
	}

	public void receiveEnergy(int energy) {
		energyCount+=energy;
	}
	public void receiveFood(int food) {
		foodCount+=food;
	}
	public void receiveOre(int ore) {
		oreCount+=ore;
	}
	public String toString()
	{
		return "Food/Energy/Ore: "+foodCount+"/"+energyCount+"/"+oreCount+"\n";
	}
	public ArrayList<Displayable> getRallyPoints() {
		return factoryWarehouse.getRallyPoints();
	}
	public ArrayList<Displayable> getStructures() {
		return factoryWarehouse.getStructures();
	}
	public ArrayList<Displayable> getUnits() {
		return factoryWarehouse.getUnits();
	}
}
