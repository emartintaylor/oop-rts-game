package Model;

public interface DisplayableResource {
	public int getFood();
	public int getEnergy();
	public int getOre();
	public int getX();
	public int getY();
}
