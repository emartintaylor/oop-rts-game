package Model;

public class IDCollection {
	private int playerID;
	IDTable rallyID;
	IDTable meleeID;
	IDTable rangeID;
	IDTable explorerID;
	IDTable structureID;
	IDTable vehicleID;
	public IDCollection(int playerID)
	{
		this.playerID=playerID;
		rallyID = new IDTable(10);
		meleeID = new IDTable(10);
		rangeID = new IDTable(10);
		explorerID = new IDTable(10);
		structureID = new IDTable(10);
		vehicleID = new IDTable(10);
	}
	public int getID(String type)
	{
		if(type.toLowerCase()=="rally")
			return rallyID.getID();
		else if(type.toLowerCase()=="melee")
			return meleeID.getID();
		else if(type.toLowerCase()=="range")
			return rangeID.getID();
		else if(type.toLowerCase()=="explorer")
			return explorerID.getID();
		else if(type.toLowerCase()=="structure")
			return structureID.getID();
		else if(type.toLowerCase()=="vehicle")
			return vehicleID.getID();
		return 0;
	}
}
