package Model.Commands;

import Model.Displayable.Harvester;

public class AddWorkersToTile extends IntegerArgCommand {

	Harvester h;
	public AddWorkersToTile(Harvester h)
	{
		this.h = h;
	}
	public String getName(){
		return "Add workers to tile";
	}
}
