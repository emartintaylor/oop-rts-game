package Model.Commands;

import controller.EventNotifier;
import controller.GameEventDispatcher;
import controller.GameEventType;

/**
 * SmartNotifier is an abstract class representing a group of notifiers which have a reference to the
 * GameEventDispatcher they register with.  This means they support the abstract functions register and unregister.
 * Concrete implementations of SmartNotifier are meant to work for specific concrete implementations of certain
 * command types.  This is due to the fact that a concrete SmartNotifier handles registering with the
 * GameEventDispatcher on its own, which means that it needs the GameEventTypes it's registering for hard coded.
 * 
 * SmartNotifiers have a static class reference to the CommandEventDispatcher, this CommandEventDispatcher must be 
 * set via the setDispatcher() method before any concrete SmartNotifiers are instantiated.  If it is not
 * than a concrete SmartNotifier may call on the static reference to the GameEventDispatcher and would cause an
 * runtime exception.
 * 
 * @author JR
 * @version 1.0
 *
 */
public abstract class SmartNotifier implements EventNotifier
{
	private static GameEventDispatcher ged;
	
	/**
	 * This method sets the static GameEventDispatcher for all SmartNotifiers.  This method should be called
	 * and the GameEventDispatcher set before any concrete SmartNotifers are instantiated.
	 * @param g -- The GameEventDispatcher that all SmartNotifiers will use.
	 */
	public static void setDispatcher( GameEventDispatcher g)
	{
		SmartNotifier.ged = g;
	}
	
	/**
	 * A private getter which returns a reference to the static GameEventDispatcher.
	 * @return The GameEventDispatcher all SmartNotifiers use.
	 */
	private GameEventDispatcher getDispatcher()
	{
		return SmartNotifier.ged;
	}
	
	/**
	 * A protected utility method, it registers this SmartNotifier as a one time listener for the provided
	 * GameEventType.  If this command is unselected in the CommandBuilder this notifier must be unregistered
	 * for the events otherwise user input will still be sent to this notifier.
	 * @param g -- This SmartNotifer will be informed if this GameEventType occurs. 
	 */
	protected void register( GameEventType g )
	{
		getDispatcher().registerOneTimeListener(g,this);
	}
	
	/**
	 * A protected utility method, it unregisters this SmartNotifier as a one time listener for the provided
	 * GameEventType.  If the notifier were registered multiple times, registrations will be removed. Unregister
	 * must be called when this notifiers command is unselected in the CommandBuilder otherwise user input will
	 * still be sent to this notifier.
	 * @param g -- The GameEventType this SmartNotifier is unregistering from. 
	 */
	protected void unregister( GameEventType g )
	{
		getDispatcher().removeOneTimeListener(g, this);
	}
	
	/**
	 * Notify will be called by the GameEventDispatcher whenever any events which this SmartNotifier are registered
	 * for occur.
	 * @param g -- The GameEventType which caused this notifier to be notified.
	 */
	public abstract void notify(GameEventType g);
	
	/**
	 * This method will be called by the Command when it is ready to be fired by the user, unregister should be
	 * called by the command when the command is unselected.
	 */
	abstract void register();
	
	/**
	 * This method should be called by the Command when it unselected and therefor no longer interested in user
	 * input.
	 */
	abstract void unregister();
}
