package Model.Commands;

import Model.CommandBuilder;
import Model.Direction;

/**
 * These commands will register interest in keys 7,8,9,1,2,3 (NW,N,NE,SW,S,SE) respectively. 
 * 
 * @author Chris
 *
 */
public abstract class DirectionArgCommand extends Command {

	Direction direction;
	public void setDirection(Direction d){
		direction = d;
	}
	@Override
	public void commit() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void prepare(CommandBuilder cb) {
		// TODO Auto-generated method stub

	}

	@Override
	public void release() {
		// TODO Auto-generated method stub

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

}
