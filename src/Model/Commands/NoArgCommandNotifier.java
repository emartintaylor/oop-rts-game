package Model.Commands;

import controller.GameEventType;

/**
 * NoArgCommandNotifier is a concrete implementation of SmartNotifier that should be used for Commands which are
 * fired by the user hitting the select command button.  It registers for the GameEventType SELECTCOMMAND and
 * when this event occurs it calls commit() on its command.
 * 
 * NoArgCommandNotifier inherits from SmartNotifier and so should not be instantiated until the static method
 * SmartNotifier.setDispatcher() has been set.
 * @author JR
 * @version 1.0
 * 
 */
public class NoArgCommandNotifier extends SmartNotifier
{
	private NoArgCommand theCommand;
	
	/**
	 * Instantiates a new NoArgCommandNotifier for the NoArgCommand provided.
	 * @param c -- The NoArgCommand this notifier will commit.
	 */
	public NoArgCommandNotifier( NoArgCommand c)
	{
		this.theCommand = c;
	}
	
	/**
	 * Notify will be called by the GameEventDispatcher the SELECTCOMMAND GameEventType occurs, at this poit the
	 * GameEventDispatcher will also remove this notifier from its one-time notifiers list.  When notify() is called
	 * it means the user has fired his currently selected command and the command this notifier is associated with
	 * should be committed.
	 * @param g -- The GameEventType which caused this notifier to be notified.
	 */
	@Override
	public void notify(GameEventType g)
	{
		theCommand.commit();
	}
	
	/**
	 * This method will be called by the Command when it is ready to be fired by the user, unregister should be
	 * called by the command when the command is unselected.
	 */
	@Override
	void register()
	{
		register( GameEventType.SELECTCOMMAND );
	}
	
	/**
	 * This method should be called by the Command when it unselected and therefor no longer interested in user
	 * input.
	 */
	@Override
	void unregister()
	{
		unregister( GameEventType.SELECTCOMMAND );
	}

}