package Model.Commands;

import LocationStuff.Location;
import Model.CommandBuilder;
import Model.Creator;
import Model.Displayable.Colonist;

public class EstablishBase extends NoArgCommand{

	Creator creator;
	Colonist c;
	NoArgCommandNotifier n;
	CommandBuilder cb;
	
	public EstablishBase(Colonist c){
		this.c = c;
		System.out.println("EstablishBase made");
	}
	@Override
	public String getName() {
		return "Establish Base";
	}

	@Override
	public void prepare(CommandBuilder comb) {
		n = new NoArgCommandNotifier(this);
		this.cb = comb;
		creator = cb.getCreator();	
		n.register();
	}

	@Override
	public void release() {
		System.out.println("Release EstablishBase");
		n.unregister();
	}

	@Override
	public void run() {
		System.out.println("Run EstablishBase");
		Location l = c.getLocation();
		if(creator == null)
			System.out.println("ASLODE");
		creator.createBase(l);
	}
	@Override
	public void commit() {
		System.out.println("Commit EstablishBase");
		c.addCommand(this);
	}

}
