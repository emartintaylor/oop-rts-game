package Model.Commands;


import LocationStuff.Location;
import Model.CommandBuilder;
import Model.SmartMap;
import Model.Displayable.Moveable;
import Util.LocationPrinter;

/**
 * Collectors will use Move mostly.  
 * Units will use the same functionality but their movement is automatic 
 * (you can't tell an individual unit to move).
 * 
 * @author Chris
 *
 */

public class Move extends NoArgCommand{
	
	Moveable toMove;
	Location destination;
	SmartMap map;
	NoArgCommandNotifier n;
	CommandBuilder cb;
	int progress;
	
	public Move(Moveable m)
	{
		toMove = m;
	}
	
	public void run(){
		LocationPrinter.print(toMove.getLocation());
		if( ++progress == toMove.getSpeed()) //move one spot
		{
			progress = 0;
			toMove.move(map.getNextInPath(toMove, destination));
			map.notifyTileDetailsManager(toMove);
		}
		
		if(toMove.getX() == destination.getX() && toMove.getY() == destination.getY())
		{
			System.out.println("Arrived.");
			toMove.removeCommand();
			System.out.println("New Location: "+toMove.getLocation());
		}
	}
	
	public void prepare(CommandBuilder cb)
	{
		n = new NoArgCommandNotifier(this);
		this.cb = cb;
		n.register();
	}
	public void release()
	{
		n.unregister();
	}
	
	public String getName(){
		return "Move";
	}
	@Override
	public  void commit()
	{
		map = cb.getSmartMap();
		destination = cb.getCurrentlySelectedLocation();
		toMove.addCommand(this);
	}
}
