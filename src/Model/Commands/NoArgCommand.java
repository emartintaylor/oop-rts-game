package Model.Commands;

import Model.CommandBuilder;

public abstract class NoArgCommand extends Command
{
	public abstract void commit();
	public abstract String getName();
	public abstract void prepare(CommandBuilder cb);
	public abstract void release();
	public abstract void run();
}
