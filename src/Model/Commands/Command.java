package Model.Commands;

import Model.CommandBuilder;

/**
 * The root of the command inheritance. 
 * 
 * @author Chris
 *
 */
public abstract class Command implements Cloneable{
	/**
	 * called on the Controllable's onTick()
	 */
	public abstract void run();
	
	/**
	 * prepare is called by command builder when the user cycles to the command. 
	 * Here, it should register its interest in any GameEvents. 
	 * 
	 * uses CommandBuilder to get whatever arguments it needs from model and stores them. 
	 * 
	 * @param cb
	 */
	public abstract void prepare(CommandBuilder cb);
	/**
	 * Called when the user cycles away from the command. Here the command
	 * should unregister interest here.  
	 */
	public abstract void release();
	/**
	 * 
	 * @return a string representation of the command.
	 */
	public abstract String getName();
	/**
	 * This is where all the info from commandBuilder should be retrieved. 
	 * (i.e. set arguments gotten from Model. 
	 */
	public abstract void commit();
	public Command getClone(){
		try {
			return (Command) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
