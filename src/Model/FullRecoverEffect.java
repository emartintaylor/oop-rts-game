package Model;

import Model.Displayable.interfaces.AOEApplicant;

public class FullRecoverEffect extends AreaEffect{

	public FullRecoverEffect(int x, int y) {
		super(x, y, "FULLRECOVEREFFECT");
	}
	public void applyEffect(AOEApplicant aoeApplicant) {
		aoeApplicant.fullRecoverEffect();
	}	
}
