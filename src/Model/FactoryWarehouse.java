package Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import LocationStuff.ChLocation;
import LocationStuff.Location;
import LocationStuff.NCLocation;
import Model.Displayable.Base;
import Model.Displayable.Colonist;
import Model.Displayable.Controllable;
import Model.Displayable.Explorer;
import Model.Displayable.Farm;
import Model.Displayable.Fort;
import Model.Displayable.HeavyVehicle;
import Model.Displayable.LightVehicle;
import Model.Displayable.Melee;
import Model.Displayable.Mine;
import Model.Displayable.Moveable;
import Model.Displayable.Plant;
import Model.Displayable.Rally;
import Model.Displayable.Range;
import Model.Displayable.Structure;
import Model.Displayable.Tower;
import Model.Displayable.Unit;
import Model.Displayable.University;
import Model.Displayable.Vehicle;
import Model.Displayable.interfaces.Displayable;
import Model.Displayable.interfaces.TickListener;
import Util.CircularList;
import Util.OilException;

public class FactoryWarehouse implements TickListener, Creator{
	
	Player myPlayer;
	
	//Rally points
	CircularList<Rally>  rallyPoints;
	
	//Units
	CircularList<CircularList<Unit>> units;
	
	//used to be type specific, changed to Unit because something in controller broke. 
	CircularList<Unit> melees;
	CircularList<Unit> colonists;
	CircularList<Unit> explorers;
	CircularList<Unit> ranges;
	CircularList<Unit> vehicles;
	
	//Structures
	CircularList<CircularList<Structure>> structures;
	
	CircularList<Structure> forts;
	CircularList<Structure> farms;
	CircularList<Structure> plants;
	CircularList<Structure> bases;
	CircularList<Structure> towers;
	CircularList<Structure> universities;
	CircularList<Structure> mines;
	
	//Armies
	//CircularList<Army> armies;
	
	IDCollection idc;
	
	FactoryWarehouse(Player p){
		myPlayer = p;
		rallyPoints = new CircularList<Rally>();
		melees = new CircularList<Unit>();
		colonists = new CircularList<Unit>();
		vehicles = new CircularList<Unit>();
		explorers = new CircularList<Unit>();
		ranges = new CircularList<Unit>();
		farms = new CircularList<Structure>();
		bases = new CircularList<Structure>();
		forts = new CircularList<Structure>();
		mines = new CircularList<Structure>();
		plants = new CircularList<Structure>();
		universities = new CircularList<Structure>();
		towers = new CircularList<Structure>();
		//armies = new CircularList<Army>();
		units = new CircularList<CircularList<Unit>>();
		structures = new CircularList<CircularList<Structure>>();
		idc = new IDCollection(p.getID());
		initRallies(10, new ChLocation(0,0));
		testMethod();
		//System.out.println("Init of Factory Warehouse");
		initUnits();
		initStructures();
	}
	
	public void testMethod()
	{
		//hard coding. 
		melees.add(new Melee(4,4,0,0));
		melees.add(new Melee(4,4,0,1));
		colonists.add(new Colonist(2,2,0,1));
		ranges.add(new Range(2,2,0,0));
		ranges.add(new Range(2,2,0,1));
		explorers.add(new Explorer(2,2,0,0));
		forts.add(new Fort(1,1,0,0));
		forts.add(new Fort(1,1,0,1));
		plants.add(new Plant(1,2,0,0, myPlayer));
		universities.add(new University(2,2,0,0));
	}
	
	public void initUnits()
	{
		units.add(melees);
		units.add(colonists);
		units.add(explorers);
		units.add(vehicles);
		units.add(ranges);
	}
	
	public void initStructures()
	{
		structures.add(forts);
		structures.add(farms);
		structures.add(plants);
		structures.add(bases);
		structures.add(towers);
		structures.add(universities);
		structures.add(mines);
	}
	
	/*public void createBase(Location l){
		//bases.add(new Base(l.getX(), l.getY(), myPlayer.getID(), new TableBasedRecycler(), myPlayer));
		
	}*/
	public void createRally(Location l){
		if(rallyPoints.size()<10)
		rallyPoints.add(new Rally(l.getX(), l.getY(), myPlayer.getID(), idc.getID("rally")));
		else
			System.out.println("Rally Points Size: "+rallyPoints.size());
	}
	public void createColonist(Location l)
	{
		if(
				(melees.size()+explorers.size()+ranges.size()+colonists.size())<25)
		explorers.add(new Colonist(l.getX(), l.getY(), myPlayer.getID(), 0));
		else
			System.out.println("Explorers Size: "+explorers.size()+" Units Size: "+(melees.size()+explorers.size()+ranges.size()));
	}
	public void createExplorer(Location l)
	{
		if(explorers.size()<10 && 
				(melees.size()+explorers.size()+ranges.size())<25){
		Explorer e;
		explorers.add(e=new Explorer(l.getX(), l.getY(), myPlayer.getID(), idc.getID("explorer")));
		//for testing only.
		rallyPoints.get(1).addUnit(e);
		}
		else
			System.out.println("Explorers Size: "+explorers.size()+" Units Size: "+(melees.size()+explorers.size()+ranges.size()));
	}
	public void createMelee(Location l)
	{
		if(melees.size()<10 && (melees.size()+explorers.size()+ranges.size())<25)
		{
			Melee m;
			melees.add(m=new Melee(l.getX(), l.getY(), myPlayer.getID(), idc.getID("melee")));
			//for testing only.
			rallyPoints.get(0).addUnit(m);
		}
		else
			System.out.println("Melees Size: "+melees.size()+" Units Size: "+(melees.size()+explorers.size()+ranges.size()));
	}
	public void createRange(Location l)
	{
		Range r;
		if(ranges.size()<10 && (melees.size()+explorers.size()+ranges.size())<25){
			
		ranges.add(r=new Range(l.getX(), l.getY(), myPlayer.getID(), idc.getID("range")));
		rallyPoints.get(0).addUnit(r);
		}
		else
			System.out.println("Ranges Size: "+ranges.size()+" Units Size: "+(melees.size()+explorers.size()+ranges.size()));
	}
	public void createHeavyVehicle(Location l)
	{

		//if(vehicles.size()<10)
		//vehicles.add(new HeavyVehicle(l.getX(), l.getY(), myPlayer.getID(), idc.getID("vehicle")));
		//else
		//	System.out.println("Vehicles Size: "+vehicles.size());

		/*if(vehicles.size()<10)
		vehicles.add(new HeavyVehicle(l.getX(), l.getY(), myPlayer.getID(), idc.getID("vehicle")));
		else
			System.out.println("Vehicles Size: "+vehicles.size());*/

	}
	public void createLightVehicle(Location l)
	{

		//if(vehicles.size()<10)
		//vehicles.add(new LightVehicle(l.getX(), l.getY(), myPlayer.getID(), idc.getID("vehicle")));
		//else
		//	System.out.println("Vehicles Size: "+vehicles.size());

		/*if(vehicles.size()<10)
		vehicles.add(new LightVehicle(l.getX(), l.getY(), myPlayer.getID(), idc.getID("vehicle")));
		else
			System.out.println("Vehicles Size: "+vehicles.size());*/

	}
	public void createFort(Location l)
	{
		if(structures.size()<10)
		forts.add(new Fort(l.getX(), l.getY(), myPlayer.getID(), idc.getID("structure")));
		else
			System.out.println("Structures Size: "+structures.size());
	}
	
	public void initRallies(int numberOfRallies, Location l)
	{
		for(int k = 0; k < numberOfRallies; k++)
		{
			createRally(l);
		}
	}
	@Override
	public void onTick() {
		for(Rally rp : rallyPoints.getAll())
			rp.onTick();
		for(CircularList<Unit> us: units.getAll())
			for(Moveable u: us.getAll())
				u.onTick();
		for(CircularList<Structure> structs: structures.getAll())
			for(Structure struct: structs.getAll())
				struct.onTick();
	}

	public Controllable getTestCase()
	{
		return rallyPoints.getCurrent();
	}
	//code duplication because of difference in java generic types in lists.  
	public ArrayList<Controllable> getControllables(){
		ArrayList<Controllable> t = new ArrayList<Controllable>();		
		t.addAll(rallyPoints.getAll());
		for(CircularList<Unit> us: units.getAll())
			t.addAll(us.getAll());
		for(CircularList<Structure> structs: structures.getAll())
			t.addAll(structs.getAll());
		return t;
	}
	public Collection<Controllable> getAllControllables()
	{
		Collection<Controllable> t = new ArrayList<Controllable>();
		t.addAll(melees.getAll());
		t.addAll(explorers.getAll());
		t.addAll(ranges.getAll());		
		t.addAll(colonists.getAll());
		return t;
	}
	public ArrayList<Displayable> getDisplayables()
	{
		ArrayList<Displayable> t = new ArrayList<Displayable>();		
		t.addAll(rallyPoints.getAll());
		for(CircularList<Unit> us: units.getAll())
			t.addAll(us.getAll());
		for(CircularList<Structure> structs: structures.getAll())
			t.addAll(structs.getAll());
		return t;	
	}
	public ArrayList<Displayable> getRallyPoints()
	{
		ArrayList<Displayable> t = new ArrayList<Displayable>();
		t.addAll(rallyPoints.getAll());
		return t;
	}
	public ArrayList<Displayable> getStructures()
	{
		ArrayList<Displayable> t = new ArrayList<Displayable>();
		t.addAll(forts.getAll());
		t.addAll(mines.getAll());
		t.addAll(bases.getAll());
		t.addAll(farms.getAll());
		t.addAll(plants.getAll());
		t.addAll(towers.getAll());
		t.addAll(universities.getAll());
		return t;
	}
	public ArrayList<Displayable> getUnits()
	{
		ArrayList<Displayable> t = new ArrayList<Displayable>();
		t.addAll(melees.getAll());
		t.addAll(explorers.getAll());
		t.addAll(ranges.getAll());
		t.addAll(colonists.getAll());
		return t;
	}
	public CircularList<Rally> getRallyList(){
		return rallyPoints;
	}
	
	public CircularList<CircularList<Unit>> getUnitsList(){
		return units;
	}

	/*public CircularList<Army> getArmyList(){
		return armies;
	}*/
	
	public CircularList<CircularList<Structure>> getStructuresList(){
		return structures;
	}
	public Player getMyPlayer()
	{
		return myPlayer;
	}

	@Override
	public void createBase(Location l) {
		//there's only every one base. hard code id 1. 
		//hard coded id 0 because Yan redid the id system and don't want to mess up what already works. 
		bases.add(new Base(l.getX(), l.getY(), 0, 1, myPlayer));
		System.out.println("Base established.");
	}

}
