package Model;

import Model.Displayable.Moveable;
import Util.CircularList;

public interface Army {

	public CircularList<CircularList<Moveable>> getArmyList();
	public CircularList<CircularList<String>> getArmyName();
	
}
