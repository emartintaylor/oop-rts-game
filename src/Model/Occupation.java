package Model;

import Model.Displayable.Movable;

public class Occupation {
	private Tile tile;
	private Item item;
	private AreaEffect areaEffect;
	private boolean occupied;
	public Occupation(Tile _tile)
	{
		tile = _tile;
		item = null;
		areaEffect = null;
		occupied = false;
	}
	public void offerItem(Item _item)
	{
		item = _item;
	}
	public void offerAreaEffect(AreaEffect _areaEffect)
	{
		areaEffect = _areaEffect;
	}
	public void useItem(Movable movable)
	{
		
		item = null;
	}
}
