package Model;

import java.util.ArrayList;
import Model.Displayable.*;
import Model.Displayable.interfaces.Displayable;
public interface DisplayablePlayer {
	public int getFoodCount();
	public int getEnergyCount();
	public int getOreCount();
	public ArrayList<Displayable> getRallyPoints();
	public ArrayList<Displayable> getStructures();
	public ArrayList<Displayable> getUnits();
}
