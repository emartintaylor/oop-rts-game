package Model;

import LocationStuff.Location;
import LocationStuff.NCLocation;
import Model.Displayable.Controllable;
import Model.Displayable.interfaces.Renderable;

public class PlayerCursor implements SelectedInfoToView
{
	Location currentLocation;
	Controllable currentControllable; 
	Map theMap;
	
	PlayerCursor(Map m)
	{
		theMap = m;
		currentLocation = new NCLocation(1,1);
		currentControllable = null;
		System.out.println("current location: "+currentLocation.getX()+" , "+currentLocation.getY());
	}
	
	public void centerOnControllable()
	{
		if(currentControllable != null)
			setCurrentLocation( new NCLocation(currentControllable.getX(), currentControllable.getY()));
		System.out.println("current location: "+currentLocation.getX()+" , "+currentLocation.getY());

	}
	
	public void moveCursor(Direction d)
	{
		setCurrentLocation(theMap.getNextLocation( d, currentLocation));
		System.out.println("current location: "+currentLocation.getX()+" , "+currentLocation.getY());
		
	}
	
	public void setCurrentLocation( Location l)
	{
		this.currentLocation = l;
		System.out.println("current location: "+currentLocation.getX()+" , "+currentLocation.getY());
	}
	
	public Location getCurrentLocation()
	{
		return this.currentLocation;
	}
	
	public void setCurrentControllable(Controllable c)
	{
		this.currentControllable = c;
	}
	
	public Controllable getCurrentControllable()
	{
		return this.currentControllable;
	}

	@Override
	public Renderable getCurrentRenderable()
	{
		return getCurrentControllable();
	}

}
