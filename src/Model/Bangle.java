package Model;

import Model.Displayable.interfaces.ItemApplicant;

public class Bangle extends Item{
	public Bangle(int _x, int _y)
	{
		super(_x,_y,"BANGLE");
	}
	public void applyItem(ItemApplicant itemApplicant)
	{
		itemApplicant.incrementArmor();
	}
}
