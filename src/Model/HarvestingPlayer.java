package Model;

public interface HarvestingPlayer {
	public void receiveFood(int food);
	public void receiveEnergy(int energy);
	public void receiveOre(int ore);
}
