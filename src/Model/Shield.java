package Model;

import Model.Displayable.interfaces.ItemApplicant;

public class Shield extends Item{
	public Shield(int _x, int _y)
	{
		super(_x,_y,"SHIELD");
	}
	public void applyItem(ItemApplicant itemApplicant)
	{
		itemApplicant.incrementDefensive();
	}
}
