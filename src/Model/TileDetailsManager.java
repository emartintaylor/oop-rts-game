package Model;

import java.util.LinkedList;
import Model.Displayable.Moveable;

public class TileDetailsManager {
	private LinkedList<TileDetails> tilesWithDetails;
	private Map m;
	public TileDetailsManager(Map m)
	{
		this.m = m;
		tilesWithDetails = new LinkedList<TileDetails>();
	}
	public void useItem(Moveable moveable)
	{
		TileDetails tileDetails = findTile(moveable.getX(),moveable.getY());
		if(found())
		{
			tileDetails.useItem(moveable);
			m.removeItemFromMap(tileDetails.getItem());
			tileDetails.removeItem();
			if(tileDetails.noDetails())
			{
				tilesWithDetails.remove(tileDetails);
			}
		}
	}
	public void useAreaEffect(Moveable moveable)
	{
		TileDetails tileDetails = findTile(moveable.getX(),moveable.getY());
		if(found())
		{
			tileDetails.useAreaEffect(moveable);
		}
	}
	public void addItem(Item item, int x, int y)
	{
		TileDetails tileDetails = findTile(x,y);
		if(found())
		{
			System.out.println("Added: "+item.getRenderItem());
			tileDetails.putItem(item);
		}
		else
		{
			tilesWithDetails.add(new TileDetails(m.getTile(x, y),item));
		}
	}
	public void addAreaEffect(AreaEffect areaEffect, int x, int y)
	{
		TileDetails tileDetails = findTile(x,y);
		if(found())
		{
			System.out.println("Added: "+areaEffect.getRenderAOE());
			tileDetails.putAreaEffect(areaEffect);
		}
		else
		{
			tilesWithDetails.add(new TileDetails(m.getTile(x, y),areaEffect));
		}
	}
	private static boolean found = false;
	private TileDetails findTile(int x, int y)
	{
		for(TileDetails tileDetails: tilesWithDetails)
		{
			if(tileDetails.match(x, y))
			{
				found=true;
				return tileDetails;
			}
			else found = false;
		}
		return null;
	}
	private boolean found(){return found;}
	public int size()
	{
		return tilesWithDetails.size();
	}
}
