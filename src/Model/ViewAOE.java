package Model;

public interface ViewAOE {
	public String getRenderAOE();
	public int getX();
	public int getY();
}
