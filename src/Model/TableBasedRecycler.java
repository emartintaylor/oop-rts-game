package Model;

import java.util.Arrays;

import Util.IntRecycler;

/**
 * only 30 possible ids to assign. 
 * @author Chris
 *
 */
public class TableBasedRecycler{

	int nextInt;
	boolean[] used;
	
	public TableBasedRecycler(){
		nextInt = 1;
		used = new boolean[30];
		Arrays.fill(used, false);
	}
	public TableBasedRecycler(int maxUniqueIDs){
		nextInt = 1;
		used = new boolean[maxUniqueIDs];
		Arrays.fill(used, false);
	}
	public int getID()
	{
		int id = -1;
		for(int i = 0; i < used.length; i++)
		{
			if(used[i]==false)
				return i;
		}
		return id;
	}
	public void clrID(int i)
	{
		used[i]=false;
	}
}
