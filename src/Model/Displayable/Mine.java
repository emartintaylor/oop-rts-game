package Model.Displayable;

import Model.HarvestingPlayer;
import Model.InstanceType;
import Model.Resource;
import Model.Displayable.Harvester;

public class Mine extends Harvester{

	public Mine(int x, int y, int playerID, int id, HarvestingPlayer player, int workers) {
		super(x, y, playerID, id,player, workers);
		this.setUpkeep(4);

		//TODO: AddMetalTile command
	}

	public Mine(int x, int y, int playerID, int id, HarvestingPlayer player) {
		this(x, y, playerID, id, player, 0);
	}

	@Override
	public String getName() {
		return "Mine";
	}

	@Override
	public InstanceType getType() {
		return InstanceType.MINE;
	}
	
	public void harvest(ResourceManager manager) {
		player.receiveOre(manager.harvestOre());	
	}

}
