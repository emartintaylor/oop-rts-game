package Model.Displayable;

import Model.Commands.JoinRP;
import Model.Commands.LeaveRP;
import Util.IntRecycler;

public abstract class Vehicle extends Collectors{

	int limit = 0;
	public Vehicle(int x, int y, int playerID, int id, int limit) {
		this(x, y, playerID, id);
		this.limit = limit;
		
		this.addSupportedCommand(new JoinRP(this));
		this.addSupportedCommand(new LeaveRP(this));
	}
	
	public Vehicle(int x, int y, int playerID, int id){
		super(x, y, playerID, id);
	}
	
	public void addUnit(Unit unit){
		if(this.getSize() >=  limit)
			return;
		
		unit.setState(State.DEACTIVE);
		super.addUnit(unit);
	}
	
}
