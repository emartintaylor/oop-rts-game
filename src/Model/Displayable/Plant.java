package Model.Displayable;

import Model.HarvestingPlayer;
import Model.InstanceType;
import Model.PayablePlayer;
import Model.Resource;
import Util.IntRecycler;

public class Plant extends Harvester{

	public Plant(int x, int y, int playerID, int id, HarvestingPlayer player, int workers) {
		super(x, y, playerID, id,player, workers);
		this.setUpkeep(4);
		//TODO: AddEnergyTile command
		
	}

	public Plant(int x, int y, int playerID, int id, HarvestingPlayer player) {
		this(x, y, playerID, id, player, 0);
	}

	@Override
	public String getName() {
		return "Plant";
	}

	@Override
	public InstanceType getType() {
		return InstanceType.PLANT;
	}

	public void harvest(ResourceManager manager) {
		player.receiveEnergy(manager.harvestEnergy());	
	}
}
