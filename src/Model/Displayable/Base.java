package Model.Displayable;

import Model.HarvestingPlayer;
import Model.InstanceType;
import Model.Resource;
import Model.Commands.AddEnergyTile;
import Model.Commands.AddFoodTile;
import Model.Commands.AddMetalTile;
import Model.Commands.AddWorkersToTile;
import Model.Commands.BreedWorkers;
import Model.Commands.MakeExplorer;

public class Base extends Harvester{

	public Base(int x, int y, int playerID, int id, HarvestingPlayer player, int workers) {
		super(x, y, playerID, id,player, workers);
		this.setUpkeep(4);

		this.addSupportedCommand(new BreedWorkers(this));
		this.addSupportedCommand(new MakeExplorer(this));
		this.addSupportedCommand(new AddEnergyTile(this));
		this.addSupportedCommand(new AddFoodTile(this));
		this.addSupportedCommand(new AddMetalTile(this));
	}

	public Base(int x, int y, int playerID, int id, HarvestingPlayer player) {
		this(x, y, playerID, id, player, 0);
	}
	@Override
	public String getName() {
		return "Base"+this.getID();
	}

	@Override
	public InstanceType getType() {
		return InstanceType.BASE;
	}
	
	public void breedWorkers(){
		this.addWorkers(5);
	}

	@Override
	public void harvest(ResourceManager manager) {
		player.receiveFood(manager.harvestFood());
		player.receiveEnergy(manager.harvestEnergy());
		player.receiveOre(manager.harvestOre());	
	}
}
