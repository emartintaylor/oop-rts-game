package Model.Displayable;

import Model.InstanceType;
import Util.IntRecycler;

public class University extends Structure {

	public University(int x, int y, int playerID, int id, int workers) {
		super( x, y, playerID, id, workers);
		// TODO Auto-generated constructor stub
	}
	
	public University(int x, int y, int playerID, int id) {
		super( x, y, playerID, id, 0);
	}

	
	public String getName() {
		return "University";
	}

	public InstanceType getType() {
		return InstanceType.UNIVERSITY;
	}

}
