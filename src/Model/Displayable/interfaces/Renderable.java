package Model.Displayable.interfaces;

import Util.EntityInfo;

public interface Renderable {
	
	public EntityInfo getRenderInfo();
	public double hpPercent();
	public int getX();
	public int getY();
}