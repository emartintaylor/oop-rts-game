package Model.Displayable.interfaces;

public interface ItemApplicant {
	public void incrementOffensive();
	public void incrementDefensive();
	public void incrementArmor();
	public void incrementMaxHealth();
}
