package Model.Displayable.interfaces;

import LocationStuff.Locateable;
import Model.InstanceType;

public interface Displayable extends Locateable{

	public String getName();
}
