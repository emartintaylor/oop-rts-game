package Model.Displayable.interfaces;

public interface AOEApplicant {
	public void healEffect();
	public void damageEffect();
	public void fullRecoverEffect();
	public void instantDeathEffect();
}
