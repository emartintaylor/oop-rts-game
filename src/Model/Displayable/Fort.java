package Model.Displayable;


import Model.InstanceType;
import Model.Resource;
import Util.CircularList;
import Util.EntityInfo;

public class Fort extends Structure{

	CircularList<Fighter> fighters = null;
	CircularList<Fighter>coming = null;
	
	public Fort(int x, int y, int playerID, int id, int workers) {
		super(x, y, playerID, id, workers);
		this.setUpkeep(4);
		// TODO Add make Melee/Make Range units
		
		fighters = new CircularList<Fighter>();
		coming = new CircularList<Fighter>();
	}
	
	public Fort(int x, int y, int playerID, int id) {
		this(x, y, playerID, id, 0);
	}

	@Override
	public String getName() {
		return "Fort";
	}

	@Override
	public InstanceType getType() {
		return InstanceType.FORT;
	}
	
	public void addFighter(Fighter fighter){
		coming.add(fighter);
	}
	
	public int getFighterCount(){ return fighters.size();}
	
	public boolean removeFighter(Fighter fighter){return (fighters.remove(fighter) != null || coming.remove(fighter)!=null);}
	
	public void onTick(){
		Fighter fighter;
		for(int i = coming.size()-1; i > 0; i--)
			if(coming.get(i).sameLocation(location)){
				fighter = coming.remove(i);
				fighters.add(fighter);
				fighter.setState(this.getState());
			}
		
		super.onTick();
	}
	
	public EntityInfo getRenderInfo(){
		EntityInfo info = super.getRenderInfo();
		
		for(int i = 0; i< fighters.size(); i++)
			info.addChildren(fighters.get(i).getRenderInfo());
		
		for(int i = 0; i< coming.size(); i++)
			info.addChildren(coming.get(i).getRenderInfo());
		
		return info;
	}

}
