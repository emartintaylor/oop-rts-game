package Model.Displayable;

import Model.InstanceType;
import Model.Resource;
import Util.IntRecycler;

public class Range extends Fighter{
	public Range(int x, int y, int playerID, int id) {
		super(x, y, playerID, id);
		this.setStats(100, 15, 10, 5);
		this.setVision(1);
		this.setMovement(1, 2);
		this.setUpkeep(4);
	}

	public InstanceType getType() {
		return InstanceType.RANGE;
	}

	public String getName() {
		return "Range";
	}
}
