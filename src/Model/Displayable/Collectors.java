package Model.Displayable;

import java.util.Collection;
import java.util.LinkedList;

import LocationStuff.Location;
import Model.Commands.Attack;
import Model.Commands.Defend;
import Model.Commands.MoveToCursor;
import Util.CircularList;
import Util.EntityInfo;
import Util.IntRecycler;
import Util.Pair;

public abstract class Collectors extends Moveable{

	/**
	 * type reasoning is tricky here.
	 * A Collector should be a collection of unit.
	 * A RP is a collection of units. A vehicle is too.  
	 * 
	 * however, Since a vehicle can be in a rallyPoint, a Collector
	 * doesn't have a list of units, it has a list of moveables,
	 * in order to include vehicle. However, Collector
	 * is itself a moveable.  
	 * 
	 */
	
	private CircularList<Moveable> army = null;
	private CircularList<Moveable> reserves = null;
	
	public Collectors(int x, int y, int playerID, int id) {
		super(x, y, playerID, id);
		army = new CircularList<Moveable>();
		reserves = new CircularList<Moveable>();
		//All collectors can move to cursor. 
	}
	/**
	 * This method returns {army} + {reserve} in one collection. 
	 * @return
	 */
	public CircularList<Moveable> getMoveablesInside(){
		Collection<Moveable> c = new LinkedList<Moveable>();
		c.addAll(army.getAll());
		c.addAll(reserves.getAll());
		CircularList<Moveable> cl = new CircularList<Moveable>();
		cl.addAll(c);
		return cl;
	}
	public void addUnit(Unit unit){
		this.addMoveable(unit);
	}
	
	protected void addMoveable(Moveable moveable){
		reserves.add(moveable);
	}
	
	
	public void removeUnit(Moveable moveable){
		
		try {
			army.remove(moveable);
			moveable.setState(State.STANDBY);	
		} catch (IndexOutOfBoundsException e) {
		}
		try{
		reserves.remove(moveable);
		moveable.setState(State.STANDBY);	
		}catch(IndexOutOfBoundsException e){
			
		}	
	}
	
	public int getSize(){
		return army.size();
	}

	public Moveable peekUnit(int index){
		return army.get(index);
	}
	
	public void onTick(){
		super.onTick();
		
		for(Moveable moveable : reserves.getAll()){
			if(moveable.getLocation().equals(this.getLocation())){
				reserves.remove(moveable);
				army.add(moveable);
			}
		}
	}
	
	public EntityInfo getRenderInfo(){
		EntityInfo info = super.getRenderInfo();
		
		for(int i = 0; i< army.size(); i++)
			info.addChildren(army.get(i).getRenderInfo());
		
		for(int i = 0; i< reserves.size(); i++)
			info.addChildren(reserves.get(i).getRenderInfo());
		
		return info;
	}
	
	public void move(Location local){
		super.move(local);
		for(int i = 0; i < army.size(); i++)
			army.get(i).move(local);
	}
	
	public CircularList<CircularList<Moveable>> getArmyList(){
		CircularList<CircularList<Moveable>> everyone = new CircularList<CircularList<Moveable>>();
		CircularList<Moveable> list = new CircularList<Moveable>();
		
		everyone.add(army);
		everyone.add(reserves);
		
		for(int i = 0; i< army.size(); i++)
			list.add(army.get(i));
		
		for(int i = 0; i< reserves.size(); i++)
			list.add(reserves.get(i));
		
		everyone.add(list);
		
		return everyone;
	}
	
	public CircularList<CircularList<String>> getArmyName(){
		CircularList<CircularList<String>> everyone = new CircularList<CircularList<String>>();
		CircularList<String> temp1 = new CircularList<String>();
		CircularList<String> temp2 = new CircularList<String>();
		CircularList<String> temp3 = new CircularList<String>();
		
		for(int i = 0; i< army.size(); i++){
			temp1.add(army.get(i).getName());
			temp3.add(army.get(i).getName());
		}
			
		
		for(int i = 0; i< reserves.size(); i++){
			temp2.add(army.get(i).getName());
			temp3.add(army.get(i).getName());
		}
		
		everyone.add(temp1);
		everyone.add(temp2);
		everyone.add(temp3);		
		return everyone;
	}
	
	public void setState(State feeling){
		super.setState(feeling);
		for(int i = 0; i<army.size(); i++)
			army.get(i).setState(feeling);
	}
	
	public void die(){
		super.die();
		for(int i = 0; i<army.size(); i++)
			army.get(i).die();
	}
	
	public void changeHealth(int amount){
		super.changeHealth(amount);
		int disperse = amount / army.size();
		for(int i = 0; i<army.size(); i++)
			army.get(i).changeHealth(disperse);
	}
}