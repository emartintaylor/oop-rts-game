package Model.Displayable;

import Model.InstanceType;
import Model.Resource;
import Model.Commands.EstablishBase;
import Model.Commands.Move;
import Util.IntRecycler;

public class Colonist extends Unit{
	public Colonist(int x, int y, int playerID, int id) {
		super(x, y, playerID, id);
		this.setStats(100, 5, 15, 3);
		this.setVision(1);
		this.setMovement(1, 1);
		this.setUpkeep(4);
		
		addSupportedCommand(new EstablishBase(this));
	}

	public InstanceType getType() {
		return InstanceType.COLONIST;
	}

	public String getName() {
		return "Colonist";
	}
}
