package Model.Displayable;

import Model.InstanceType;
import Model.Resource;
import Util.IntRecycler;

public class HeavyVehicle extends Vehicle {

	public HeavyVehicle(int x, int y, int playerID, int id) {
		super(x, y, playerID, id, 3);
		this.setUpkeep(4);
	}

	public String getName() {
		return "Heavy Vehicle";
	}

	public InstanceType getType() {
		return InstanceType.HEAVYVEHICLE;
	}
}
