package Model.Displayable;

import Model.Resource;

public class ResourceManager {

	private final Resource resource;
	private int workers=0;
	private static int RATE=10;
	private final ResourceType type;
	
	ResourceManager(Resource resource, ResourceType resourceType){
		this.resource= resource;
		workers = 0;
		type = resourceType;
	}
	
	ResourceManager(Resource resource, ResourceType resourceType,  int workers){
		this.resource = resource;
		this.workers = workers;
		type = resourceType;
	}
	
	public void addWorkers(int amount){
		workers+= amount;
	}
	
	public int removeWorkers(int amount){
		amount = Math.min(workers, amount);
		workers -= amount;
		return amount;
	}
	
	public int harvestFood(){
		int temp = resource.harvestFood(workers * RATE);
		resource.renewFood(20);
		return temp;
	}

	public int harvestEnergy(){return resource.harvestEnergy(workers * RATE);}
	public int harvestOre(){return resource.harvestOre(workers * RATE);}
	
	public static void incRate(){ RATE++;}
	
	public boolean contains(Resource resource){
		return (this.resource == resource);
	}
	
	public ResourceType getType(){return type;}
}
