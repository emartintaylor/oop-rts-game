package Model.Displayable;



import Model.Army;
import Model.InstanceType;
import Model.Commands.Attack;
import Model.Commands.Defend;
import Model.Commands.MoveToCursor;
import Model.Displayable.interfaces.TickListener;

public class Rally extends Collectors implements TickListener, Army{
	
	int workers;
	
	public Rally(int x, int y, int playerID, int id){
		super(x, y, playerID, id);
		workers =0;
		
		//supported commands
		this.addSupportedCommand(new MoveToCursor(this));
		this.addSupportedCommand(new Attack(this));
		this.addSupportedCommand(new Defend(this));
	}

	public InstanceType getType() {
		return InstanceType.RALLY;
	}

	public String getName() {
		return "Rally";
	}
		
	public void pickUpWorkers(int amount){workers+= amount;}
	
	public int dropOffWorkers(int amount){
		amount = Math.min(amount, workers);
		workers -= amount;
		return amount;
	}
	
	
	public void update(){
		
		//shouldn't this go in the constructor? 
		int health=0, maxHealth=0, armor=0, defense = 0,offense = 0, visibility = 0;
		int speed=Integer.MAX_VALUE, mobility=Integer.MAX_VALUE;
		
		for(int i = 0; i< getSize(); i++){
			health += peekUnit(i).getHealth();
			maxHealth += peekUnit(i).getMaxHealth();
			armor += peekUnit(i).getArmor();
			defense += peekUnit(i).getDefense();
			offense += peekUnit(i).getOffence();
			visibility += peekUnit(i).getVisionRadius();
			
			mobility = Math.min(mobility, peekUnit(i).getMobility());
			speed = Math.min(speed, peekUnit(i).getMobility());
			
			peekUnit(i).setState(this.getState());
		}
		
		this.setStats(maxHealth, offense, defense, armor);
		this.setMovement(mobility, speed);
		this.setVision(visibility);
	}
	
	public void onTick(){
		update();
		super.onTick();
	}
	
	public void addVehicle(Vehicle vehicle){
		addMoveable(vehicle);
	}
}
