package Model.Displayable;

import LocationStuff.ChLocation;
import LocationStuff.Location;
import Model.Commands.JoinRP;
import Model.Commands.LeaveRP;
import Model.Displayable.interfaces.AOEApplicant;
import Model.Displayable.interfaces.ItemApplicant;
import Util.IntRecycler;

public abstract class Moveable extends Controllable implements AOEApplicant, ItemApplicant{
	
	private ChLocation location = null;
	
	/*
	 * Please take note of the system. Speed is the speed that a unit can move measured
	 * in TICK/TILE. This means how many ticks it takes for it to move 1 tile.
	 * 1 TICK/TILE is the fastest a unit can go.
	 * 
	 * Anyway, progress is the amount of ticks gone by to allow a move.
	 * 		if progress = mobility, time to move. 
	 * 		else increment progress 
	 */
	private int mobility, speed, progress;
	
	Moveable(int x, int y, int playerID, int id){
		super(playerID, id);
		location = new ChLocation(x, y);
		this.addSupportedCommand(new JoinRP(this));
		this.addSupportedCommand(new LeaveRP(this));
	}
	
	public int getMobility(){	return mobility;}
	public int getSpeed(){		return speed;}
	protected void setMovement(int mobility, int speed){
		this.mobility = mobility;
		this.speed = speed;
	}
	
	public int getX(){return location.getX();}
	public int getY(){ return location.getY();}
	public void setLocation(int x, int y){location.setLocation(x, y);}
	public Location getLocation(){	return location;}
	
	public void move(Location local)
	{
			location.setLocation(local);
	}

	public boolean sameLocation(Location location){
		return this.location.equals(location);
	}
}
