package Model.Displayable;

import LocationStuff.Location;
import Model.InstanceType;
import Model.TableBasedRecycler;
import Model.Displayable.interfaces.AOEApplicant;
import Model.Displayable.interfaces.ItemApplicant;
import Util.IntRecycler;


////////////////////////////////////////////
// VERY FUCKING IMPORTANT //
///////////////////////////////////////////
////////////////////////////////////////////
//VERY FUCKING IMPORTANT //
///////////////////////////////////////////
////////////////////////////////////////////
//VERY FUCKING IMPORTANT //
///////////////////////////////////////////
//PLEASE EDIT ME but make sure to still add the mobility, getx and gety and playerID
//since path finding needs them. 
//make it abstract if you want
//the things in here now are merely for testing
////////////////////////////////////////////
//VERY FUCKING IMPORTANT //
///////////////////////////////////////////
////////////////////////////////////////////
//VERY FUCKING IMPORTANT //
///////////////////////////////////////////

public class Movable extends Unit{
	public Movable(int x, int y, int playerID)
	{
		super(x, y, playerID, 10);
		this.setStats(100, 5, 15, 3);
		this.setVision(1);
		this.setMovement(1, 1);
	}
	public String toString()
	{
		return "\nI am at ("+getX()+","+getY()+").\n"+
				"Stats: "+getHealth()+" "+getOffence()+" "+getDefense()+" "+getArmor()+" \n"
				;
	}
	@Override
	public String getName() {
		return "Movable";
	}
	@Override
	public InstanceType getType() {
		return InstanceType.COLONIST;
	}
	@Override
	public boolean sameLocation(Location location) {
		// TODO Auto-generated method stub
		return false;
	}
}
