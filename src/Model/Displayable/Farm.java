package Model.Displayable;

import Model.HarvestingPlayer;
import Model.InstanceType;
import Model.Resource;

public class Farm extends Harvester{

	public Farm(int x, int y, int playerID, int id, HarvestingPlayer player, int workers) {
		super(x, y, playerID, id, player, workers);

		this.setUpkeep(4);
	}

	public Farm(int x, int y, int playerID, int id, HarvestingPlayer player) {
		this(x, y, playerID, id, player, 0);
	}

	@Override
	public String getName() {
		return "Farm";
	}

	@Override
	public InstanceType getType() {
		return InstanceType.FARM;
	}

	public void harvest(ResourceManager manager) {
		player.receiveFood(manager.harvestFood());
	}
}
