package Model.Displayable;

import java.util.LinkedList;

import Model.HarvestingPlayer;
import Model.Resource;
import Model.Commands.AddWorkersToTile;

public abstract class Harvester extends Structure{

	int unassignedWorkers = 0;
	LinkedList<ResourceManager> resources = null;
	HarvestingPlayer player;
	
	public Harvester(int x, int y, int playerID, int id, HarvestingPlayer player, int workers) {
		super(x, y, playerID, id, workers);
		unassignedWorkers = workers;
		resources = new LinkedList<ResourceManager>();
		this.player = player;
		this.addSupportedCommand(new AddWorkersToTile(this));
	}

	public abstract void harvest(ResourceManager manager);
	
	//TODO: change this
	public void onTick(){
		harvest();
		super.onTick();
	}
	
	public int removeWorkers(int amount){
		amount = Math.min(amount, unassignedWorkers);
		unassignedWorkers -= amount;
		return super.removeWorkers(amount);
	}
	
	public int addWorkers(int amount){
		unassignedWorkers += amount;
		return super.addWorkers(amount);
	}

	public boolean addWorkersToResource(Resource resource, int amount){
		
		if(!contains(resource))
			return false;
		
		for(ResourceManager manager : resources)
			if(manager.contains(resource)){
				amount = Math.min(amount, unassignedWorkers);
				manager.addWorkers(amount);
				unassignedWorkers -= amount;
				return true;
			}
		
		return false;
	}
	
	public void addWorkersToResource(Resource resource){
		addWorkersToResource(resource, unassignedWorkers);
	}

	public int removeWorkersFromResource(Resource resource, int amount){
		
		if(!contains(resource))
			return 0;
		
		for(ResourceManager manager : resources)
			if(manager.contains(resource)){
				amount = manager.removeWorkers(amount);
				unassignedWorkers += amount;
				return amount;
			}
		
		return 0;
	}
	

	public void addResource(Resource resource, ResourceType type){
		if(resource.getAssigned())
			resources.add(new ResourceManager(resource, type));
	}
	
	public void removeResource(Resource resource){
		if(contains(resource)){
			resources.remove(resource);
			resource.unassign();
		}
	}
	
	private boolean contains(Resource resource){
		if (!resource.getAssigned())
			return false;
		
		for(ResourceManager manager : resources)
			if(manager.contains(resource))
				return true;
		return false;
	}
	
	private void harvest(){
		for(ResourceManager manager: resources)
			harvest(manager);
	}
}
