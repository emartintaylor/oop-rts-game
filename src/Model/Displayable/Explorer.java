package Model.Displayable;

import Model.InstanceType;
import Model.Resource;
import Util.EntityInfo;
import Util.IntRecycler;
import Util.Pair;

public class Explorer extends Unit{

	private boolean prospecting = false;
	
	public Explorer(int x, int y, int playerID, int id) {
		super(x, y, playerID, id);
		this.setStats(100, 5, 15, 3);
		this.setVision(2);
		this.setMovement(1, 1);
		this.setUpkeep(4);		
	}

	public InstanceType getType() {
		return InstanceType.EXPLORER;
	}

	public String getName() {
		return "Explorer";
	}
	
	public int getSpeed(){
		int speed = super.getSpeed();
		if(prospecting)
			speed++;
		
		return speed;
	}
	
	public void stopProspecting(){ prospecting = false;}
	public void startProspecting(){	prospecting = true;}
	public boolean isProspecting(){ return prospecting;}
	
	public EntityInfo getRenderInfo(){
		EntityInfo info = super.getRenderInfo();
		
		info.addInfo(new Pair<String>("Prospecting",this.isProspecting()+""));
		
		return info;
	}
}
