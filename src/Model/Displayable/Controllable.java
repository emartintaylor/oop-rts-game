package Model.Displayable;

import java.util.LinkedList;

import LocationStuff.Location;
import Model.InstanceType;
import Model.Resource;
import Model.Commands.CancelOrders;
import Model.Commands.Command;
import Model.Commands.Decommission;
import Model.Commands.PowerDown;
import Model.Commands.PowerUp;
import Model.Displayable.interfaces.Displayable;
import Model.Displayable.interfaces.Renderable;
import Model.Displayable.interfaces.TickListener;
import Util.CircularList;
import Util.EntityInfo;
import Util.IntRecycler;
import Util.Pair;


public abstract class Controllable implements TickListener, Displayable, Renderable{

	
	private int playerID, ID;
	private int offensive;
	private int defensive;
	private int health;
	private int maxHealth;
	private int armor;
	
	private int vision;
	private int upkeep;
	
	private State state;
	private CircularList<Command> supportedCommands = null;
	private LinkedList<Command> commandQueue = null; 
	private IntRecycler ir = null;
	
	
	public abstract InstanceType getType();
	public abstract Location getLocation();
	public abstract String getName();
	
	public Controllable(int playerID, int id){
		this.ID = id;
		this.playerID = playerID;
		state = State.DEACTIVE;
		supportedCommands = new CircularList<Command>();
		commandQueue = new LinkedList<Command>();
		this.addSupportedCommand(new PowerUp(this));
		this.addSupportedCommand(new PowerDown(this));
		this.addSupportedCommand(new Decommission(this));
		this.addSupportedCommand(new CancelOrders(this));
	}
	
	protected void setStats(int health, int offence, int defensive, int armor){
		this.health = this.maxHealth = health;
		this.offensive = offence;
		this.defensive = defensive;
		this.armor = armor;
	}
	public int getMaxHealth(){	return maxHealth;}
	public int getArmor(){return armor;	}
	public int getDefense(){ return defensive;}
	public int getOffence(){ return offensive;}
	public int getUpkeep(){ return upkeep;}
	protected void setUpkeep(int value){
		upkeep = value;
	}
	
	public int getPlayerID(){	return playerID;}
	
	public CircularList<Command> getCommands(){return (CircularList<Command>) supportedCommands;}
	protected void addSupportedCommand(Command command){supportedCommands.add(command);	}
	public void removeCommand(){	commandQueue.remove();	}
	
	public void addCommand(Command command){	commandQueue.add(command);}
	
	
	public int getID(){	return ID;}
	
	
	public State getState(){	return this.state;}
	public void setState(State feeling){ state = feeling;}

	
	public void onTick(){
		
		if(state == State.DEAD)
			return;

		if(!commandQueue.isEmpty())
			commandQueue.peekFirst().run();
	}
		
	public EntityInfo getRenderInfo(){
		EntityInfo info = new EntityInfo(this.getName());
		
		for(Command i: commandQueue)
			info.addMission(i.getName());
		
		info.addInfo(new Pair<String>("Player", this.getPlayerID()+""));
		info.addInfo(new Pair<String>("ID", this.getID()+""));
		info.addInfo(new Pair<String>("Health", this.getHealth()+""));
		info.addInfo(new Pair<String>("Max Health", this.getMaxHealth()+""));
		info.addInfo(new Pair<String>("Offense", this.getOffence()+""));
		info.addInfo(new Pair<String>("Defense", this.getDefense()+""));
		info.addInfo(new Pair<String>("Armor", this.getArmor()+""));
		
		
		info.setType(this.getName());
	
		
		return info;
	}
	
	public boolean isDead(){	return (state == State.DEAD);}
	public void die(){
		health = 0;
		state = State.DEAD;
		health = 0;
		ir.recycleInt(ID);
	}
	
	
	public void changeHealth(int amount){
		health += amount;
		health = Math.min(health, maxHealth);
		
		
		if(health <= 0){
			health = 0;
			this.die();
		}
	}
	
	public double hpPercent() {
		if(maxHealth!=0)
			return (double)(health/maxHealth);
		else
			return 1;
	}
	
	public abstract boolean sameLocation(Location location);
	
	public void changeHealth(double amount){changeHealth((int)amount*maxHealth);}
	
	public void damage(int damage){
		if(damage > armor)
			changeHealth(-(damage - armor));
	}
	public void heal(int heal){changeHealth(heal);}
	public void damage(double damage){damage((int)(damage * maxHealth));}
	public int getHealth(){return health;}
	public int getHealthPercentage(){	return (int)((float)health/maxHealth * 100);}
	
	public int getVisionRadius(){ return vision;}
	protected void setVision(int vision){ this.vision = vision; }
	
	public void insufficientResources(){this.changeHealth(- 0.05);}

	public void incrementOffensive(){offensive += 5;}
	public void incrementDefensive(){defensive += 5;}
	public void incrementArmor(){ armor += 5;}
	public void incrementMaxHealth(){ maxHealth += 10;}
	public void healEffect(){ health +=5;}
	public void damageEffect(){health -=5;}
	public void fullRecoverEffect(){health = maxHealth;}
	public void instantDeathEffect(){die();}
	
	public String toString(){
		return this.getName()+": "+this.getID()+" "+"H:"+this.getHealth()+"/"+this.getMaxHealth()+" O:"+this.getOffence()+" D:"+this.getDefense()+" A:"+this.getArmor()+"\n";
	}
}
