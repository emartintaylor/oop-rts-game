package Model.Displayable;

import Model.InstanceType;
import Model.Resource;
import Util.IntRecycler;

public class Melee extends Fighter{
	public Melee(int x, int y, int playerID, int id) {
		super(x, y, playerID, id);
		this.setStats(100, 5, 15, 5);
		this.setVision(1);
		this.setMovement(1, 2);
		this.setUpkeep(4);		
	}

	public InstanceType getType() {
		return InstanceType.MELEE;
	}

	public String getName() {
		return "Melee";
	}
}
