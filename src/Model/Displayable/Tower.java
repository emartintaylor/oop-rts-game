package Model.Displayable;

import Model.InstanceType;
import Model.Resource;
import Util.IntRecycler;

public class Tower extends Structure{

	public Tower(int x, int y, int playerID, int id, int workers) {
		super(x, y, playerID, id, workers);
		
		this.setStats(100, 0, 15, 4);
		this.setVision(2);
		this.setUpkeep(4);
	}


	public String getName() {return "Tower";}
	public InstanceType getType() {	return InstanceType.TOWER;}
	
	public int addWorkers(int amount){
		if(this.isBuilt())
			return 0;
		
		return super.addWorkers(amount);
	}
	

}
