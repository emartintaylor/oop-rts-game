package Model.Displayable;

import LocationStuff.Location;
import LocationStuff.NCLocation;

public abstract class Structure extends Controllable{

	NCLocation location;
	private int totalWorkers, building;
	private int assignedWorkers;
	
	public Structure(int x, int y, int playerID, int id, int workers) {
		super(playerID, id);
		location = new NCLocation(x,y);
		building=20;
		totalWorkers = workers;
		assignedWorkers = totalWorkers/2;
	}
	// Yan
	// My methods start here
	public int getAssignedWorkers()
	{
		return assignedWorkers;
	}
	public int getX()
	{
		return location.getX();
	}
	public int getY()
	{
		return location.getY();
	}
	public void assignWorkers(int amount)
	{
		assignedWorkers+= Math.min(amount, totalWorkers-assignedWorkers);
	}
	public void assignWorkers()
	{
		assignWorkers(1);
	}
	public void unAssignWorkers(int amount)
	{
		assignedWorkers-= Math.min(amount, assignedWorkers);
	}
	public void unAssignWorkers()
	{
		unAssignWorkers(1);
	}
	
	
	//my methods end here
	public int addWorkers(int amount){
		totalWorkers += amount;
		return totalWorkers;
	}
	
	public int removeWorkers(int amount){
		amount = Math.max(totalWorkers, amount);
		
		totalWorkers -= amount;
		return totalWorkers;
	}
	
	public int removeWorkers(){
		return removeWorkers(totalWorkers);
	}
	
	public Location getLocation(){
		return location;
	}
	
	public void onTick(){
		if(!isBuilt())
			return ;
		super.onTick();
	}
	
	
	public boolean isBuilt(){
		if(building != 0){
			building -= totalWorkers;
			return false;
		}
		
		return true;
	}
	
	public boolean sameLocation(Location location){
		return this.location.equals(location);
	}
}
