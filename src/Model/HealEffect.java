package Model;

import Model.Displayable.interfaces.AOEApplicant;

public class HealEffect extends AreaEffect{

	public HealEffect(int x, int y) {
		super(x, y, "HEALEFFECT");
	}
	public void applyEffect(AOEApplicant aoeApplicant) {
		aoeApplicant.healEffect();
	}	
}
