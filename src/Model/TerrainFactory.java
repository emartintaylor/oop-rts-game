package Model;

public class TerrainFactory {
	public TerrainFactory()
	{
	}
	public static Terrain match(char value)
	{
		Terrain terrain = null;
		switch(value)
		{
		case '0': terrain =  new Grass(); break;
		case '1': terrain = new Desert(); break;
		case '2': terrain = new Rocky(); break;
		case '9': terrain = new Border(); break;
		default: terrain = new Grass(); break;
		}
		return terrain;
	}
}
