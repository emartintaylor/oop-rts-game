package Model;

import java.util.*;
public interface MapToView {
	public int getWidth();
	public int getHeight();
	public String[][] getTerrain();
	public Visibility[][] getVisibility(int playerID);
	public LinkedList<ViewItem> getItems();
	public LinkedList<ViewObstacle> getObstacles();
	public LinkedList<ViewAOE> getAreaEffects();
	public LinkedList<DisplayableResource> getResources();
	public void clrChanged(int x,int y);
	public boolean getChanged(int x, int y);
}
