package Model;

public class Border extends Terrain{
	private final static int INFINITY = 9;
	public Border()
	{
		super("BORDER", INFINITY);
	}
}
