package Model;

public class Terrain {
	private String terrainType;
	private int level;
	public Terrain(String terrainType, int level)
	{
		this.terrainType = terrainType;
		this.level = level;
	}
	public String getTerrainType()
	{
		return this.terrainType;
	}
	public int getLevel()
	{
		return this.level;
	}
}
