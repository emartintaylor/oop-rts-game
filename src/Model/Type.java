package Model;

public enum Type {
	ITEM, OBSTACLE, AREAEFFECT, RESOURCE, MOVABLE, STRUCTURE;
}
