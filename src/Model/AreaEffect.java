package Model;

import Model.Displayable.interfaces.AOEApplicant;

public abstract class AreaEffect implements ViewAOE{
	private int x;
	private int y;
	private String render;
	public AreaEffect(int _x, int _y, String _render)
	{
		x = _x;
		y = _y;
		render = _render;
	}
	public int getX(){return x;}
	public int getY(){return y;}
	public String getRenderAOE(){return render;}
	public abstract void applyEffect(AOEApplicant aoeApplicant);
	public String toString()
	{
		return "-"+render+"-" + " at ("+x+","+y+")";
	}
}