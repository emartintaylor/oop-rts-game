package Model;

import LocationStuff.ChLocation;
import Model.Displayable.Moveable;

public class RiverTile extends Tile{
	private Tile nextTile;
	public RiverTile(int x, int y, int numOfPlayers, Tile nextTile)
	{
		super(x,y,new River(), 1);
		this.nextTile = nextTile;
	}
	public void flow(Moveable moveable, int time)
	{
		if(carryeable(moveable)&&time==0)
			moveable.setLocation(nextTile.getX(), nextTile.getY());
	}
	private boolean carryeable(Moveable moveable)
	{
		return nextTile.getPassable(moveable.getPlayerID(), moveable.getMobility()) == 1;
	}
}
