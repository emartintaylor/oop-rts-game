package Model;

import java.util.Arrays;

/**
 * only 30 possible ids to assign. 
 * @author Chris
 *
 */
public class IDTable{

	int nextInt;
	boolean[] used;
	
	public IDTable(){
		nextInt = 1;
		used = new boolean[30];
		Arrays.fill(used, false);
	}
	public IDTable(int maxUniqueIDs){
		nextInt = 1;
		used = new boolean[maxUniqueIDs];
		Arrays.fill(used, false);
	}
	public int getID()
	{
		int id = -1;
		for(int i = 0; i < used.length; i++)
		{
			if(used[i]==false)
			{
				used[i]=true;
				return i;
			}
		}
		return id;
	}
	public void clrID(int i)
	{
		used[i]=false;
	}
}
