package Model;

import java.util.*;

import decorators.DecoratorCommander;
import decorators.SelectablesToView;


import LocationStuff.ChLocation;
import LocationStuff.Location;
import Model.Displayable.*;
import Util.IntRecycler;
import View.View;



public class Main {
	static Map map;
	public static void main(String[] args) {
		String [] mapArray = {
				"999999",
				"900009",
				"900009",
				"900009",
				"999999"
		};
		map = new Map(mapArray,1);
		Sword s = new Sword(2,1);
		Bangle b = new Bangle(3,1);
		Shield sh = new Shield(4,1);
		Helmet hel = new Helmet(2,2);
		HealEffect he = new HealEffect(2,1);
		DamageEffect de = new DamageEffect(3,1);
		FullRecoverEffect fre = new FullRecoverEffect(4,1);
		InstantDeathEffect ide = new InstantDeathEffect(2,2);
		
		map.addResource(new Resource(10,10,10,1,1));
		
		
		///Explorer m = new Explorer(2,1,0, new IntList(10));
		Movable m = new Movable(2,1,0);
		m.changeHealth(-15);
		out(m.toString());
		PlayerCursor pc = new PlayerCursor(map);
		Player p = new Player(map, 0, pc);
		map.addAreaEffect(he);
		map.addAreaEffect(de);
		map.addAreaEffect(fre);
		map.addAreaEffect(ide);
		map.addItem(s);
		map.addItem(b);
		map.addItem(sh);
		map.addItem(hel);
		map.printMapDetails();
		out(m.toString());
		map.notifyTileDetailsManager(m);
		out(m.toString());
/*
		out(p.toString());
		ChLocation l = new ChLocation(2,2);
		p.makeExplorer(l);
		p.makeMelee(l);
		p.makeRange(l);
		p.onTick();
		out(p.toString());
		map.printTerrainLevel();
		
		map.printMapDetails();
		*/
		//
		map.printMapDetails();
		Movable m2 = new Movable(3,1,0);
		map.notifyTileDetailsManager(m2);
		map.printMapDetails();
	/*	Visibility [][] visibility = map.getVisibility(0);
		outv(visibility);
		map.commitVisibility(2,1,1);
		visibility = map.getVisibility(0);
		out();
		outv(visibility);
		map.commitVisibility(2,3,3);
		visibility = map.getVisibility(0);
		out();
		outv(visibility);
		map.addObstacle(new TreeObs(4,1));
		map.addObstacle(new MountainObs(4,2));*/
		map.printTerrainLevel();
		map.printPassability(0, 0);
		ChLocation l = new ChLocation(1,1);
		p.makeMelee(l);
		l.setLocation(1, 3);
		p.makeExplorer(l);
		l.setLocation(2,1);
		p.makeRange(l);
		l.setLocation(4,3);
		p.makeExplorer(l);
		p.onTick();
		
		Visibility [][] visibility = map.getVisibility(0);
		outv(visibility);
	}
	private static void outv(Visibility[][] visibility)
	{
		for(int j = 0; j < map.getHeight(); j++)
		{
			for(int i = 0; i < map.getWidth(); i++)
			{
				String str = visibility[j][i]+"\t";
				out(str);
			}
			out();
		}
	}
	private static void out(Object o)
	{
		System.out.print(o);
	}
	private static void out()
	{
		System.out.println();
	}
}
/*
 * 
		Movable m = new Movable(3,2);
		Location l = new ChLocation(2,2);
		LinkedList<Direction> list = new LinkedList<Direction>();
		//list = map.calculatePath(m.getX(),m.getY(),l.getX(),l.getY(),0,0);		
		//out();
		//out(list.toString());
		 * 
		 * out("Print Terrain level");
		map.printTerrainLevel();
		out("Print Passability for player 0, mobility 0");
		map.printPassability(0,0);
		out("Printing details of map");
		out();
		String [][] terrain = map.getTerrain();
		Visibility [][] visibility = map.getVisibility(0);
		for(int j = 0; j < map.getHeight(); j++)
		{
			for(int i = 0; i < map.getWidth(); i++)
			{
				String s = visibility[j][i]+"\t";
				out(s);
			}
			out();
		}	
		for(int j = 0; j < map.getHeight(); j++)
		{
			for(int i = 0; i < map.getWidth(); i++)
			{
				String s = terrain[j][i]+"\t";
				out(s);
			}
			out();
		}
		out();
		DecoratorCommander dc = new DecoratorCommander();
		
		View test = new View(map);
	}
	private static void out(Object o)
	{
		System.out.print(o);
	}
	private static void out()
	{
		System.out.println();
	}
}
 */

