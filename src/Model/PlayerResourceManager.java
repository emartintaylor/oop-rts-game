package Model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import Model.Displayable.Controllable;
import Model.Displayable.interfaces.TickListener;

public class PlayerResourceManager implements TickListener{

	Player myPlayer;
	
	PlayerResourceManager(Player p){
		myPlayer = p;
	}

	@Override
	public void onTick() {
		myControllablesConsumeMyResources();
		
	}
	private void myControllablesConsumeMyResources(){
		Collection<Controllable> c = myPlayer.getControllables();
		
		Iterator<Controllable> i = c.iterator();
		int amount;
		System.out.println("Size: "+c.size());
		while(i.hasNext()){
			amount = i.next().getUpkeep();
			//System.out.println("here");
			myPlayer.decrementFood(amount);
			//Waiting on Resources to be written, so can reference in Player. 
		}
	}
	
}
