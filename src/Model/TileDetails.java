package Model;

import Model.Displayable.Moveable;

public class TileDetails {
	private Tile tile;
	private Item item;
	private AreaEffect areaEffect;
	private boolean hasItem;
	private boolean hasAreaEffect;
	public TileDetails(Tile tile, Item item)
	{
		this.tile = tile;
		this.item = item;
		this.areaEffect = null;
		hasItem = true;
		hasAreaEffect = false;
	}
	public TileDetails(Tile tile, AreaEffect areaEffect)
	{
		this.tile = tile;
		this.item = null;
		this.areaEffect = areaEffect;
		hasItem = false;
		hasAreaEffect = true;
	}
	private boolean hasItem(){return hasItem;}
	private boolean hasAreaEffect(){return hasAreaEffect;}
	public boolean noDetails(){return (hasItem()==false)&&(hasAreaEffect()==false);}
	public Item getItem(){
		return item;}
	public void putItem(Item _item)
	{
		this.item = _item;
		System.out.println("This Tile Added: "+item.getRenderItem());
		hasItem = true;
		tile.setChanged();
	}
	public void putAreaEffect(AreaEffect _areaEffect)
	{
		this.areaEffect = _areaEffect;
		System.out.println("This Tile Added: "+areaEffect.getRenderAOE());
		hasAreaEffect = true;
		tile.setChanged();
	}
	public void useAreaEffect(Moveable moveable)
	{
		if(hasAreaEffect())
		{
			System.out.println("Using: "+areaEffect.getRenderAOE());
		areaEffect.applyEffect(moveable);
		}
	}
	public void useItem(Moveable moveable)
	{		
		if(hasItem())
		{
			System.out.println("Using: "+item.getRenderItem());
		item.applyItem(moveable);
		tile.setChanged();
		}
	}
	public void removeItem()
	{
		item = null;
		hasItem = false;
	}
	public boolean match(int x, int y)
	{
		return ((tile.getX()==x)&&(tile.getY()==y));
	}
}
