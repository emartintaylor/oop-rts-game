import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import Model.Displayable.interfaces.TickListener;


public class AClock {
	
	static final int TICK_DELAY = 1000; //1 second.
	
	ArrayList<TickListener> clients;
	Timer timer;
	AClock(){
		timer = new Timer();
		clients = new ArrayList<TickListener>(); 
		timer.scheduleAtFixedRate(new task(), 0, TICK_DELAY);
	}
	public void register(TickListener t){
		clients.add(t);
	}
	private class task extends TimerTask{
		public void run(){
			for(TickListener t : clients)
				t.onTick();
			System.out.println("TICK");
		}
	}
	
}
